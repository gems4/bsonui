//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file EdgesWidget.cpp
/// Implementation of class EdgesWidget - Widget to work with edges -
/// internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QMessageBox>
#include <QKeyEvent>
#include "EdgesWidget.h"
#include "ui_EdgesWidget.h"
using namespace bsonio;

namespace bsonui {

//----------------------------------------------------------------------

void EdgesWidget::closeEvent(QCloseEvent* e)
{
    if( keysModel )
     keysModel->Close();
    if( inKeysModel )
     inKeysModel->Close();
    if( outKeysModel )
     outKeysModel->Close();

    if( !onCloseEvent(this) )
           e->ignore();
       else
           QWidget::closeEvent(e);
}


EdgesWidget::EdgesWidget( const string& schemaName, const string& queryString, QWidget *parent) :
    BSONUIBase(schemaName, parent),
    edgeNames(ioSettings().Schema()->getEdgesList()),
    isDefaultQuery(!queryString.empty()),  defaultQuery(queryString),
    ui(new Ui::EdgesWidget),    keysModel(0), keysTable(0),
    inKeysModel(0), inKeysTable(0), outKeysModel(0), outKeysTable(0)
{
    // set up default data
    if( curSchemaName.empty())
      curSchemaName = edgeNames[0];
    vertexNames = ioSettings().Schema()->getVertexesList();

   ui->setupUi(this);
   ui->keySplitter->setStretchFactor(0, 1);
   ui->mainSplitter->setStretchFactor(0, 0);
   ui->mainSplitter->setStretchFactor(1, 0);
   ui->mainSplitter->setStretchFactor(2, 1);

   setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
   QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
   setWindowTitle(title);

   //set up main parameters
   bson_init(&curRecord);
   bson_finish(&curRecord);

   //define schema checkbox
   for( uint ii=0; ii<edgeNames.size(); ii++ )
      ui->typeBox->addItem(edgeNames[ii].c_str());
   ui->typeBox->setCurrentText(curSchemaName.c_str());

   // define edit tree view
   aHeaderData << "key" << "value"  << "comment" ;
   fieldTable =  new TBsonView(  ui->bsonWidget );
   model_schema = new TSchemaNodeModel(  ioSettings().Schema(), &curRecord,
        curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
   deleg_schema = new TSchemaNodeDelegate();
   fieldTable->setModel(model_schema);
   fieldTable->setItemDelegate(deleg_schema);
   fieldTable->setColumnWidth( 0, 150 );
   fieldTable->expandToDepth(0);
   ui->gridLayout->addWidget(fieldTable, 1, 0, 1, 2);

   // define all keys tables
   keysTable = new TKeyTable( ui->keySplitter,
                  [=]( const QModelIndex& index ){openRecordKey( index );});
   ui->keySplitter->insertWidget(0, keysTable);
   ui->keySplitter->setStretchFactor(0, 4);
   connect( keysTable, SIGNAL( clicked(const QModelIndex& ) ), this, SLOT(openRecordKey( const QModelIndex& )));

   // define table of incoming keys
   inKeysTable = new TKeyTable(ui->inWidget,
                  []( const QModelIndex&  ){;} );
   ui->verticalLayout_2->addWidget(inKeysTable);
   connect( inKeysTable, SIGNAL( doubleClicked(const QModelIndex& ) ),
            this, SLOT(addIncoming( const QModelIndex& )));

   // define table of outgoing keys
   outKeysTable = new TKeyTable(ui->outWidget,
                  []( const QModelIndex&  ){;} );
   ui->verticalLayout->addWidget(outKeysTable);
   connect( outKeysTable, SIGNAL( doubleClicked(const QModelIndex& ) ),
            this, SLOT(addOutgoing( const QModelIndex& )));

   resetDBClient( curSchemaName );
   resetInOutDBClients();

   // define menu
   setActions();

   // reset in/out queries
   if(  keysTable->model()->rowCount() < 1 )
       // set up default data
       CmNew();
   else
     {  // read first line
       isResetQuery = isDefaultQuery;
       openRecordKey( keysTable->model()->index(0,0), !isDefaultQuery );
     }
}

EdgesWidget::~EdgesWidget()
{
    if(keysModel)
       delete keysModel;
    if( keysTable )
      delete keysTable;

    // free incoming table data
    if(inKeysModel)
       delete inKeysModel;
    if( inKeysTable )
      delete inKeysTable;

    // free outgoing table data
    if(outKeysModel)
       delete outKeysModel;
    if( outKeysTable )
      delete outKeysTable;

    delete ui;
    //cout << "~EdgesWidget" << endl;
}

//-----------------------------------------------------

void EdgesWidget::resetTypeBox( const QString & text )
{
    disconnect( ui->typeBox, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));
    ui->typeBox->setCurrentText(text);
    connect( ui->typeBox, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));
}

/// Change current Edge
void EdgesWidget::typeChanged(const QString & text)
{
  try {
        curSchemaName = text.toUtf8().data();

        bson_destroy( &curRecord );
        bson_init( &curRecord );
        bson_finish( &curRecord );
        model_schema->setupModelData( &curRecord, curSchemaName);
        fieldTable->expandToDepth(0);

       if( !isDefaultQuery )
       {
          resetDBClient(curSchemaName );
          // reset in/out Query Lists
          if(  keysTable->model()->rowCount() > 0 )
             openRecordKey( keysTable->model()->index(0,0), true );
       }
       else
        dbgraph->resetSchema( curSchemaName, false );
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void EdgesWidget::resetDBClient(const string& schemaName_ )
{
  try{

    TDBEdgeDocument* newClient = TDBEdgeDocument::newDBEdgeDocument(
                uiSettings().database(), schemaName_, defaultQuery );
     // no schema
     if( newClient == nullptr )
     {
      dbgraph.reset();
      ui->edgeQuery->setText("");
     } else
       {
        ui->edgeQuery->setText(newClient->GetLastQuery().c_str());
        dbgraph.reset( newClient );
       }

    if( !keysModel )
      keysModel = new DBKeysModel( dbgraph, keysTable, this );
    else
      keysModel->resetDBClient(dbgraph);

   }
   catch(std::exception& e)
     {
        cout << "Internal comment " << e.what() << endl;
        throw;
     }
}

void EdgesWidget::resetInOutDBClients()
{

  try{
        queryInStr = "";
        queryOutStr = "";
        string inVertex = "VertexElement", outVertex = "VertexDataSource";

        TDBVertexDocument* vertexdoc = TDBVertexDocument::newDBVertexDocument(
             uiSettings().database(), "VertexElement" );
        if( vertexdoc == nullptr )
          return;
        vertexdoc->resetMode(true);


        if( isDefaultQuery)  // set up fixed query
        {
           string _inV_val = extractStringField( "_inV", defaultQuery );
           if( !_inV_val.empty() )
           {
               queryInStr = vertexdoc->idQuery( _inV_val );
               string label = vertexdoc->extractLabelById( _inV_val );
               if( !label.empty() )
               {
                   inVertex = ioSettings().Schema()->getVertexName( label );
                   cout << " ########## "<< inVertex << endl;
               }
               ui->actionIncoming_Vertex_Query->setDisabled(true);
           }

           string _outV_val = extractStringField( "_outV", defaultQuery );
           if( !_outV_val.empty() )
           {
               queryOutStr = vertexdoc->idQuery( _outV_val );
               string label = vertexdoc->extractLabelById( _outV_val );
               if( !label.empty() )
                 {
                   outVertex = ioSettings().Schema()->getVertexName( label );
                   cout << " ########## "<< outVertex << endl;
                 }
               ui->actionOutgoing_Vertex_Query->setDisabled(true);
           }
        }

        vertexdoc->resetSchema( inVertex, true );
        if( !queryInStr.empty() )
        {
          vertexdoc->SetQueryJson(queryInStr);
          vertexdoc->runQuery();
        }
        inDB.reset( vertexdoc );
        //delete inKeysModel;
        if( !inKeysModel )
          inKeysModel = new DBKeysModel( inDB, inKeysTable, this );
        else
          inKeysModel->resetDBClient(inDB);

        TDBVertexDocument* outClient = TDBVertexDocument::newDBVertexDocument(
                    uiSettings().database(), outVertex, queryOutStr );
        outDB.reset( outClient );
        outDB->resetMode(true);
        if( !outKeysModel )
          outKeysModel = new DBKeysModel( outDB, outKeysTable, this );
        else
          outKeysModel->resetDBClient(outDB);
     }
    catch(std::exception& e)
      {
         cout << "Internal comment " << e.what() << endl;
         throw;
      }
}

bool EdgesWidget::resetBson(const string& schemaName )
{
   // test legal schema name
   if( !curSchemaName.empty() &&
       !schemaName.empty() && schemaName != curSchemaName )
   return false;
   fieldTable->updateModelData( &curRecord, curSchemaName );
   fieldTable->update();
   return true;
}

void EdgesWidget::openRecordKey(  const QModelIndex& rowindex , bool resetInOutQuery  )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    string key = keysTable->model()->data(index).toString().toUtf8().data();

    /*if( isDefaultQuery )
    { // reset schema from label
        index = rowindex.sibling( row, 1);
        string _label = pTable->model()->data(index).toString().toUtf8().data();
        string _schema = schema->getEdgeName(_label);
        if( _schema.empty() )
          return;
        if( _schema != curSchemaName )
        {
            ui->typeBox->setCurrentText(_schema.c_str());
            //typeChanged(_schema.c_str() );
        }
    }*/
    // Read Record
    openRecordKey(  key, resetInOutQuery  );
}


void EdgesWidget::openRecordKey(  const string& reckey, bool resetInOutQuery  )
{
   try{
        if( isDefaultQuery )
         dbgraph->resetMode(true);

        // Read Record
        dbgraph->GetRecord( reckey.c_str() );

        if( isDefaultQuery )
        {
           dbgraph->resetMode(false);
           string _schema = dbgraph->GetSchemaName();
           if( _schema != curSchemaName )
           {
               curSchemaName = _schema;
               resetTypeBox( curSchemaName.c_str() );
               //typeChanged(_schema.c_str() );
           }
        }

        bson_destroy( &curRecord );
        dbgraph->GetBson(&curRecord);
        resetBson( ""/*dbclient->getKeywd()*/ );
        contentsChanged = false;

        string _inV, _outV;
        model_schema->getValue("_inV",_inV);
        model_schema->getValue("_outV",_outV);

        if( isResetQuery )
        {
           // Reopen in/out lists
          if( queryInStr.empty() && !_inV.empty() )
            inKeysModel->changeQueryToId( _inV  );

          if( queryOutStr.empty() && !_outV.empty() )
            outKeysModel->changeQueryToId(_outV  );
        }

        if( resetInOutQuery )
        {
           inKeysModel->resetInOutQuery( _inV  );
           outKeysModel->resetInOutQuery( _outV  );
        }

       // select lines into lists
       if( !_inV.empty()  )
          inKeysModel->changeKeyList(_inV);
       if( !_outV.empty()  )
          outKeysModel->changeKeyList(_outV);
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }
}

//-------------------------------------------------------------

/// Change current View menu selections
void EdgesWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TSchemaNodeModel::showComments );
    ui->action_Display_enums->setChecked(TSchemaNodeModel::useEnumNames);
    ui->action_Edit_id->setChecked(TSchemaNodeModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TBsonView::expandedFields );
}

/// Change model ( show/hide comments)
void EdgesWidget::updtModel()
{
   model_schema->updateModelData();
   fieldTable->header()->resizeSection(0, 150);
   fieldTable->header()->resizeSection(1, 250);
   fieldTable->expandToDepth(0);
}

/// Change table (Show Enum Names Instead of Values)
void EdgesWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void EdgesWidget::updtDB()
{
    // set up new dbclient
    dbgraph->runQuery();
    keysModel->changeKeyList("");;
    outDB->runQuery();
    outKeysModel->changeKeyList("");
    inDB->runQuery();
    inKeysModel->changeKeyList("");;

    //resetDBClient( curSchemaName );
    //resetInOutDBClients();
    // ??? clear current record
    CmNew();
    // show new key list
    // ??? changeKeyList( pTable, tableModel, dbclient->getKeyFromBson( curRecord.data ) );
}

void EdgesWidget::addIncoming(  const QModelIndex& rowindex )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    string key = index.model()->data(index).toString().toUtf8().data();
    string::size_type pos1 = key.find(":");
    key = key.substr(0, pos1);

    string id_;
    // only to unsaved records
    if( !model_schema->getValue("_id",id_) || id_.empty()  )
    {    model_schema->setValue("_inV",key);
         fieldTable->hide();
         fieldTable->show();
    }
}

void EdgesWidget::addOutgoing(  const QModelIndex& rowindex )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    string key = index.model()->data(index).toString().toUtf8().data();
    string::size_type pos1 = key.find(":");
    key = key.substr(0, pos1);

    string id_;
    // only to unsaved records
    if( !model_schema->getValue("_id",id_) || id_.empty()  )
    {    model_schema->setValue("_outV",key);
         fieldTable->hide();
         fieldTable->show();
    }
}

void EdgesWidget::updateAllKeys()
{
  dbgraph->runQuery();
  changeKeyList();
  inKeysModel->changeKeyList("");
  outKeysModel->changeKeyList("");
}


} // namespace bsonui
