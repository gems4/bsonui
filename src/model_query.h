//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_query.h
/// Declaration of TQueryModel, TQueryDelegate and TQueryView
/// implements a tree view of query structure
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef _model_query_h
#define _model_query_h

#include <QAbstractItemModel>
#include <QTreeView>
#include <QItemDelegate>
#ifdef addBSONIO
#include "dbquerydef.h"
#else
#include "bsonio/dbquerydef.h"
#endif
using namespace bsonio;

namespace bsonui {


/// \class TQueryModel
/// class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to query object
class TQueryModel: public QAbstractItemModel
{
	Q_OBJECT

    QStringList hdData;
    QueryLine* rootNode;

 public:
	  
  TQueryModel( QObject* parent = 0 );
  ~TQueryModel();
	 
  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;     //ok
  int columnCount ( const QModelIndex& parent  ) const; // ok
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant& value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const; 
  Qt::ItemFlags flags ( const QModelIndex& index ) const;
	 
  bsonio::QueryLine* lineFromIndex(const QModelIndex& index) const;
  bool isAll( const QModelIndex& index ) const;
  bool isAdd( const QModelIndex& index ) const;
  bool isResize( const QModelIndex& index ) const;

  void resizeArray( QWidget* parent, const QModelIndex& index );
  void delObject( QWidget* parent, const QModelIndex& index );
  const QModelIndex addField( const string& _fldsName,
     const SchInternalData& _fldData, const QModelIndex& index );
  const QModelIndex addObject( const string& akey, const QModelIndex& index );

  ///?? queryType - EJDB query or SparQL or other
  void setQuery( int /*queryType*/, const string& /*oldQuery*/ )
  {
    //queryType; oldQuery;
  }

  /// Make query string from internal model data
  string getQuery( int queryType );

  void clearModelData()
  {
      beginResetModel();
      if( rootNode )
        delete rootNode;
      rootNode = new QueryLine(0, "$root", 0 );
      endResetModel();
  }

};

/// \class TQueryDelegate
/// individual items in views are rendered and edited using delegates
class TQueryDelegate: public QItemDelegate
{
	Q_OBJECT

public:
	
    TQueryDelegate( QObject * parent = 0 );
    QWidget *createEditor( QWidget *parent,
	                       const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};

///  \class TQueryView implements a tree view of query structure
/// that displays items from a TQueryModel model.
class TQueryView: public QTreeView
{
	Q_OBJECT
	
	void keyPressEvent(QKeyEvent* e);
    bool testCurrentIndex();

    void addObject( const string& akey  );
    void addTopObject( const string& akey  );
    QMenu* contextMenu(const QModelIndex &index);


 protected slots:
    void slotPopupContextMenu(const QPoint& pos);
    void changeCurrent( int section );

 public slots:
    void CmAddObjects( const vector<string>& _fieldsList,
                       const vector<SchInternalData>& _fieldsTypes );
    void CmAddField();
    void CmAddAnd()
    {  addTopObject( "$and" );  }
    void CmAddOr()
    {  addTopObject( "$or" );  }
    void CmAddNot()
    {  addObject( "$not" );  }
    void CmAddIn()
    {  addObject( "$in" );  }
    void CmAddNin()
    {  addObject( "$nin" );  }
    void CmAddICase()
    {  addObject( "$icase" );  }
    void CmAddExists()
    {  addObject( "$exists" );  }
    void CmAddBegin()
    {  addObject( "$begin" );  }
    void CmAddGt()
    {  addObject( "$gt" );  }
    void CmAddGte()
    {  addObject( "$gte" );  }
    void CmAddLt()
    {  addObject( "$lt" );  }
    void CmAddLte()
    {  addObject( "$lte" );  }
    void CmAddEq()
    {  addObject( "$eq" );  }

    void CmDelObject();
    void CmClearObject();
    void CmResizeList();
    void CmContextMenu();

 public:

    TQueryView( QWidget * parent = 0 );

     void ClearAll()
     {
         TQueryModel* model_ =  (TQueryModel*)(model());
         if( model_)
           model_->clearModelData();
     }
};

} // namespace bsonui

#endif   // _model_query_h
