//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file graph_data.cpp
/// Implementation of classes SeriesLineData and ChartData -
/// internal representation of graphic data
/// (Plotting system description)
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing 
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical 
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <cstdio>
#include <QJsonArray>
#include <QVector>
#include "graph_data.h"
#ifdef addBSONIO
#include "v_json.h"
#else
#include "bsonio/v_json.h"
#endif
using namespace std;
using namespace bsonio;

namespace bsonui {


QColor colorAt(const QColor &start, const QColor &end, qreal pos)
{
    Q_ASSERT(pos >= 0.0 && pos <= 1.0);
    qreal r = start.redF() + ((end.redF() - start.redF()) * pos);
    qreal g = start.greenF() + ((end.greenF() - start.greenF()) * pos);
    qreal b = start.blueF() + ((end.blueF() - start.blueF()) * pos);
    QColor c;
    c.setRgbF(r, g, b);
    return c;
}

//---------------------------------------------------------------------------
// SeriesLineData
//---------------------------------------------------------------------------

void SeriesLineData::toBsonObject( bson *obj ) const
{
   bson_append_int( obj, "gpt", markerShape );
   bson_append_int( obj, "gps", markerSize );
   bson_append_int( obj, "gls", penSize );
   bson_append_int( obj, "glt", penStyle );
   bson_append_int( obj, "gsp", spline );
   bson_append_int( obj, "gndx", xcolumn );
   bson_append_int( obj, "grd", red );
   bson_append_int( obj, "ggr",  green );
   bson_append_int( obj, "gbl",  blue );
   bson_append_string( obj, "gnm",  name.c_str() );
}

void SeriesLineData::fromBsonObject( const char *obj )
{
    if(!bson_find_value( obj, "gpt", markerShape ) )
        markerShape = 0;
    if(!bson_find_value( obj, "gps", markerSize ) )
        markerSize = 4;
    if(!bson_find_value( obj, "gls", penSize ) )
        penSize = 2;
    if(!bson_find_value( obj, "glt", penStyle ) )
        penStyle = 0;
    if(!bson_find_value( obj, "gsp", spline ) )
        spline = 0;
    if(!bson_find_value( obj, "gndx", xcolumn ) )
        xcolumn = -1;
    if(!bson_find_value( obj, "grd", red ) )
        red = 25;
    if(!bson_find_value( obj, "ggr", green ) )
        green = 0;
    if(!bson_find_value( obj, "gbl", blue ) )
        blue = 150;
    name = "";
    bson_find_string( obj, "gnm", name );
}



void SeriesLineData::toJsonObject(QJsonObject& json) const
{
    json["gpt"] = markerShape;
    json["gps"] = markerSize;
    json["gls"] = penSize;
    json["glt"] = penStyle;
    json["gsp"] = spline;
    json["gndx"] = xcolumn;
    json["grd"] = red;
    json["ggr"] = green;
    json["gbl"] =  blue;
    json["gnm"] =  name.c_str();
}

void SeriesLineData::fromJsonObject(const QJsonObject& json)
{
    markerShape = json[ "gpt" ].toInt( 0 );
    markerSize = json[ "gps" ].toInt( 4 );
    penSize = json[ "gls" ].toInt( 2 );
    penStyle = json[ "glt" ].toInt( 0 );
    spline = json[ "gsp" ].toInt( 0 );
    xcolumn = json[ "gndx" ].toInt( -1 );
    red = json[ "grd" ].toInt( 25 );
    green = json[ "ggr" ].toInt( 0 );
    blue = json[ "gbl" ].toInt( 150 );
    name = json["gnm"].toString("").toStdString();
}

//---------------------------------------------------------------------------
// ChartData
//---------------------------------------------------------------------------


void ChartData::setMinMaxRegion( double reg[4] )
{
    region[0] = reg[0];
    region[1] = reg[1];
    region[2] = reg[2];
    region[3] = reg[3];
    part[0] = reg[0]+(reg[1]-reg[0])/3;
    part[1] = reg[1]-(reg[1]-reg[0])/3;
    part[2] = reg[2]+(reg[3]-reg[2])/3;
    part[3] = reg[3]-(reg[3]-reg[2])/3;

}


void ChartData::toBsonObject( bson *obj ) const
{
    int ii;
    bson_append_string( obj, "title", title.c_str() );
    bson_append_int( obj, "graphType", graphType );

    // define grid of plot
    bson_append_int( obj, "axisTypeX", axisTypeX );
    bson_append_int( obj, "axisTypeY", axisTypeY );
    bson_append_string( obj, "axisFont", axisFont.toString().toUtf8().data());
    bson_append_string( obj, "xName", xName.c_str() );
    bson_append_string( obj, "yName", yName.c_str() );

    bson_append_start_array(obj, "region");
    for( ii=0; ii<4; ii++)
        bson_append_double( obj, to_string(ii).c_str(), region[ii] );
    bson_append_finish_array(obj);

    bson_append_start_array(obj, "part");
    for( ii=0; ii<4; ii++)
        bson_append_double( obj, to_string(ii).c_str(), part[ii] );
    bson_append_finish_array(obj);

    bson_append_start_array(obj, "b_color");
    for( ii=0; ii<3; ii++)
        bson_append_int( obj, to_string(ii).c_str(), b_color[ii] );
    bson_append_finish_array(obj);

     // define curves
    bson_append_start_array(obj, "lines");
    for(uint ii=0; ii<linesdata.size(); ii++)
     {
            bson_append_start_object( obj, to_string(ii).c_str());
            linesdata[ii].toBsonObject( obj );
            bson_append_finish_object( obj );
     }
    bson_append_finish_array(obj);

    bson_append_start_array(obj, "models");
    for(uint ii=0; ii<modelsdata.size(); ii++)
     {
             bson_append_start_object( obj, to_string(ii).c_str());
             modelsdata[ii]->toBsonObject( obj );
             bson_append_finish_object( obj );
     }
    bson_append_finish_array(obj);
}

void ChartData::fromBsonObject( const char *obj )
{
   uint ii;
   const char *arr;

   if( !bson_find_string( obj, "title", title ) )
        title = "title";
    if(!bson_find_value( obj, "graphType", graphType ) )
        graphType = LineChart;

    // define grid of plot
    if(!bson_find_value( obj, "axisTypeX", axisTypeX ) )
        axisTypeX = 5;
    if(!bson_find_value( obj, "axisTypeY", axisTypeY ) )
        axisTypeY = 5;
    string fntName;
    if( bson_find_string( obj, "axisFont", fntName ) )
        axisFont.fromString( fntName.c_str() );
    if( !bson_find_string( obj, "xName", xName ) )
        xName = "x";
    if( !bson_find_string( obj, "yName", yName ) )
        yName = "y";

    arr  = bson_find_array(  obj, "region" );
    for( ii=0; ii<4; ii++)
        if(!bson_find_value( arr,  to_string(ii).c_str(), region[ii] ) )
            region[ii] = 0;
    arr  = bson_find_array(  obj, "part" );
    for( ii=0; ii<4; ii++)
        if(!bson_find_value( arr,  to_string(ii).c_str(), part[ii] ) )
            part[ii] = 0;
    arr  = bson_find_array(  obj, "b_color" );
    for( ii=0; ii<3; ii++)
        if(!bson_find_value( arr,  to_string(ii).c_str(), b_color[ii] ) )
            b_color[ii] = 255;

    linesdata.clear();
    SeriesLineData linebuf;
    arr  = bson_find_array(  obj, "lines" );
    bson_iterator iter;
    bson_iterator_from_buffer(&iter, arr /*bson_iterator_value(it)*/);
    while (bson_iterator_next(&iter))
    {
        linebuf.fromBsonObject( bson_iterator_value(&iter) );
        linesdata.push_back(  linebuf );
    }

    arr  = bson_find_array(  obj, "models" );
    bson_iterator_from_buffer(&iter, arr /*bson_iterator_value(it)*/);
    ii=0;
    while (bson_iterator_next(&iter))
    {
        if( ii >= modelsdata.size())
           break;
        modelsdata[ii++]->fromBsonObject( bson_iterator_value(&iter) );
    }

}

void ChartData::toJsonObject(QJsonObject& json) const
{
    json[ "title" ] =  title.c_str();
    json[ "graphType" ] = graphType;
    json[ "axisTypeX" ] = axisTypeX;
    json[ "axisTypeY" ] = axisTypeY;
    json[ "axisFont" ] = axisFont.toString();
    json[ "xName" ] =  xName.c_str();
    json[ "yName" ] = yName.c_str();

    QJsonArray regArray;
    QJsonArray partArray;
    for(uint ii=0; ii<4; ii++) {
        regArray.append(region[ii]);
        partArray.append(part[ii]);
    }
    json["region"] = regArray;
    json["part"] = partArray;

    QJsonArray colorArray;
    for(uint ii=0; ii<3; ii++)
        colorArray.append(b_color[ii]);
    json["b_color"] = colorArray;

    QJsonArray linesArray;
    for(uint ii=0; ii<linesdata.size(); ii++)
    {
        QJsonObject lnObject;
        linesdata[ii].toJsonObject(lnObject);
        linesArray.append(lnObject);
    }
    json["lines"] = linesArray;

    QJsonArray modelArray;
    for(uint ii=0; ii<modelsdata.size(); ii++)
    {
        QJsonObject lnObject;
        modelsdata[ii]->toJsonObject(lnObject);
        modelArray.append(lnObject);
    }
    json["models"] = modelArray;
}

void ChartData::fromJsonObject(const QJsonObject& json)
{
    title = json[ "title" ].toString( "Graph title" ).toStdString();
    graphType = json[ "graphType" ].toInt( LineChart );
    axisTypeX = json[ "axisTypeX" ].toInt( 5 );
    axisTypeY = json[ "axisTypeY" ].toInt( 5 );
    QString fntname = json[ "axisFont" ].toString( "Sans Serif" );
    axisFont.fromString( fntname );
    xName = json[ "xName" ].toString( "x" ).toStdString();
    yName = json[ "yName" ].toString( "y" ).toStdString();

    QJsonArray regArray = json["region"].toArray();
    QJsonArray partArray = json["part"].toArray();
    if( regArray.size() > 3 && partArray.size() > 3 )
     for(uint ii=0; ii<4; ii++)
     {
        region[ii] = regArray[ii].toDouble();
        part[ii] = partArray[ii].toDouble();
     }

    QJsonArray colorArray = json["b_color"].toArray();
    if( colorArray.size() > 2 )
     for(uint ii=0; ii<3; ii++)
        b_color[ii] = colorArray[ii].toInt();

    linesdata.clear();
    SeriesLineData linebuf;
    QJsonArray linesArray = json["lines"].toArray();
    for(int ii=0; ii<linesArray.size(); ii++)
    {
        QJsonObject lnObject = linesArray[ii].toObject();
        linebuf.fromJsonObject(lnObject);
        linesdata.push_back(  linebuf );
    }

    linesArray = json["models"].toArray();
    for(int ii=0; ii<linesArray.size(); ii++)
    {
       if( ii >= (int)modelsdata.size() )
               break;
        QJsonObject lnObject = linesArray[ii].toObject();
        modelsdata[ii]->fromJsonObject(lnObject);
    }
}

void ChartData::updateXSelections()
{
    uint defined_lines = linesdata.size();
    uint nLines = 0;

    for( uint ii=0; ii<modelsdata.size(); ii++)
    {
        auto numxColms = modelsdata[ii]->getAbscissaNumber();
        uint nLinN =  modelsdata[ii]->getSeriesNumber();
        for( uint jj=0; jj<nLinN; jj++, nLines++ )
        {
            bsonioErrIf( nLines >= defined_lines, "updateXSelections", "Error into graph data.." );
            if( linesdata[nLines].getXColumn() >= numxColms )
              linesdata[nLines].setXColumn( -1 );
        }
     }
}

void ChartData::updateYSelections( bool updateNames )
{
    uint defined_lines = linesdata.size();
    uint nLines = 0;

    for( uint ii=0; ii<modelsdata.size(); ii++)
    {
        uint nLinN =  modelsdata[ii]->getSeriesNumber();
        for( uint jj=0; jj<nLinN; jj++, nLines++ )
        {
            if( nLines >= defined_lines )
                 linesdata.push_back( SeriesLineData( jj, nLinN, modelsdata[ii]->getName(nLines)  ) );
            else
                if( updateNames )
                    linesdata[nLines].setName( modelsdata[ii]->getName(nLines) );
         }
     }
    linesdata.resize(nLines);
}


} // namespace bsonui

//--------------------- End of graph_data.cpp ---------------------------
