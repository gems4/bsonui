//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file QueryWidget.cpp
/// Implementation of class QueryWidget  - widget to work with database queries
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include "QueryWidget.h"
#include "ui_QueryWidget.h"
#include "SchemaSelectDialog.h"
#include "SelectDialog.h"
#include "BSONUIBase.h"
#ifdef addBSONIO
#include "json2cfg.h"
#else
#include "bsonio/json2cfg.h"
#endif
using namespace bsonio;

namespace bsonui {

QueryWidget::QueryWidget(const char* title, const vector<string>& schemaNames,
                         const bsonio::DBQueryDef& query,
                         QWidget *resultW, QWidget *parent):
    QMainWindow(parent),
    _schemaName(schemaNames[0]),
    defaultQr( query),  queryDef( query ),
    ui(new Ui::QueryWidget), resultWidget(resultW)
{
    ui->setupUi(this);
    setWindowTitle(title);

    ui->splitterSelect->setStretchFactor(0, 1);
    ui->splitterSelect->setStretchFactor(1, 2);

   for( uint ii=0; ii<schemaNames.size(); ii++ )
      ui->qSchema->addItem(schemaNames[ii].c_str());
   ui->qSchema->setCurrentText(queryDef.getToSchema().c_str());

    // set up select model
    lstmodel = new TStringVectorModel( queryDef.getFieldsCollect(), this );
    ui->fieldsList->setModel(lstmodel);
    //ui->fieldsList->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->fieldsList->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->fieldsList->setEditTriggers( QAbstractItemView::NoEditTriggers );

    // set up query edit mode
    queryEdit =  new TQueryView( ui->frameTree  );
    TQueryModel*  model_ = new TQueryModel( this );
    TQueryDelegate*  deleg_ =  new TQueryDelegate( this );
    queryEdit->setModel(model_);
    queryEdit->setItemDelegate(deleg_);
    queryEdit->setColumnWidth( 0, 350 );
    //ui->gridLayout_2->addWidget(queryEdit, 1, 0, 1, 5);
    ui->gridLayout_2->addWidget(queryEdit, 1, 0, 1, 1);

    // set up where part
    ui->queryText->setText(queryDef.getEJDBQuery().c_str());

    // connect commands
    setActions();
    resetDBClient();
}

QueryWidget::~QueryWidget()
{
    delete queryEdit;
    delete ui;
}


void QueryWidget::Update( const bsonio::DBQueryDef& query )
{
    defaultQr = query;
    queryDef = query;
    ui->queryText->setText(queryDef.getEJDBQuery().c_str());
    lstmodel->setStringList( queryDef.getFieldsCollect() );
     ui->qSchema->setCurrentText(queryDef.getToSchema().c_str());
    // queryEdit->Clear();
}

//  Connect all actions
void QueryWidget::setActions()
{
    connect( ui->qSchema, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(schemaChanged(const QString&)));

    // File
    connect( ui->action_Save_Current_Query_to_File, SIGNAL(triggered()),this, SLOT(CmSaveQuery()));
    connect( ui->action_Load_Query_from_File, SIGNAL( triggered()), this, SLOT(CmLoadQuery()));
    connect( ui->action_Execute_Current_Query, SIGNAL( triggered()), this, SLOT(CmExecute()));
    connect( ui->action_Accept_curent_Query, SIGNAL( triggered()), this, SLOT(CmApplay()));
    connect( ui->action_Close_Cancel, SIGNAL( triggered()), this, SLOT(close()));

    // Select part
    connect( ui->action_Select_Fieldpaths, SIGNAL( triggered()), this, SLOT(CmSelectFields()));
    connect( ui->action_Add_Fieldpaths, SIGNAL( triggered()), this, SLOT(CmAddSelectFields()));
    connect( ui->action_restote_select, SIGNAL( triggered()), this, SLOT(CmSelectRestore()));

    // Where part
    connect(ui->action_Apply_from_Tree, SIGNAL(triggered()), this, SLOT(CmWhereApply()));
    connect(ui->action_Restore_Query, SIGNAL(triggered()), this, SLOT(CmWhereRestore()));
    connect(ui->action_Clear_Tree, SIGNAL(triggered()), this, SLOT(CmClearTree()));

    // Menu
    connect(ui->actionAdd_List_Fieldpaths, SIGNAL(triggered()), this, SLOT(CmAddWhereFields()));
    connect(ui->actionAdd_and_2, SIGNAL(triggered()), queryEdit, SLOT(CmAddAnd()));
    connect(ui->actionAdd_or_2, SIGNAL(triggered()), queryEdit, SLOT(CmAddOr()));
    connect(ui->actionAdd_Fieldpath, SIGNAL(triggered()), queryEdit, SLOT(CmAddField()));
    connect(ui->actionAdd_Exists, SIGNAL(triggered()), queryEdit, SLOT(CmAddExists()));
    connect(ui->actionAdd_not, SIGNAL(triggered()), queryEdit, SLOT(CmAddNot()));
    connect(ui->actionAdd_in, SIGNAL(triggered()), queryEdit, SLOT(CmAddIn()));
    connect(ui->actionAdd_nin, SIGNAL(triggered()), queryEdit, SLOT(CmAddNin()));
    connect(ui->actionAdd_begin, SIGNAL(triggered()), queryEdit, SLOT(CmAddBegin()));
    connect(ui->actionAdd_gt, SIGNAL(triggered()), queryEdit, SLOT(CmAddGt()));
    connect(ui->actionAdd_gte, SIGNAL(triggered()), queryEdit, SLOT(CmAddGte()));
    connect(ui->actionAdd_lt, SIGNAL(triggered()), queryEdit, SLOT(CmAddLt()));
    connect(ui->actionAdd_lte, SIGNAL(triggered()), queryEdit, SLOT(CmAddLte()));
    connect(ui->actionAdd_eq, SIGNAL(triggered()), queryEdit, SLOT(CmAddEq()));
    connect(ui->actionAdd_i_case, SIGNAL(triggered()), queryEdit, SLOT(CmAddICase()));
    connect(ui->actionResize_List, SIGNAL(triggered()), queryEdit, SLOT(CmResizeList()));
    connect(ui->actionDelete_Line, SIGNAL(triggered()),  queryEdit, SLOT(CmDelObject()));
    connect(ui->actionClear_Line, SIGNAL(triggered()),  queryEdit, SLOT(CmClearObject()));

    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));

    // Help
    connect(ui->actionHelp_About, SIGNAL(triggered()), this, SLOT(CmHelpAbout()));
    connect(ui->action_Help, SIGNAL(triggered()), this, SLOT(CmHelp()));

    qName = new QLineEdit( ui->nameToolBar );
    qName->setEnabled( true );
    qName->setFocusPolicy( Qt::StrongFocus );
    qName->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  "new_query";
    qName->setText(title);
    ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QLabel *aname = new QLabel( "Name",ui->nameToolBar );
    ui->nameToolBar->addWidget( aname );
    ui->nameToolBar->addWidget( qName ); // setStretchableWidget( pLine );
}


bool QueryWidget::selectLine( string& fldpath, SchInternalData& flddata )
{
    SchemaSelectDialog dlg( this,
              "Please, mark data fields for the query table", _schemaName );
    if( !dlg.exec() )
       return false;
    fldpath = dlg.selIndex(&flddata);
    return !fldpath.empty();
}

void QueryWidget::schemaChanged(const QString & text)
{
   _schemaName = text.toUtf8().data();
}

// menu ----------------------------------------------------------

void QueryWidget::CmSaveQuery()
{
    bson fbson;
    //bson whilebson;
    try {
          // set data to bson
          queryToBson( &fbson );

          // save query to file
          string fileName = "*query.json";
          if(  ChooseFileSave( this, fileName,
                      "Please, select a file to save current query", "*.json", fileName  ))
          {
              FJson file( fileName);
              file.SaveBson( fbson.data );
          }
     }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }
    bson_destroy( &fbson );
    //bson_destroy( &whilebson);
}


void QueryWidget::CmLoadQuery()
{
    try{
         // Sellect file name
         string fileName;
         if(  !ChooseFileOpen( this, fileName,
                        "Please, select a file with saved query to execute", "*.json"  ))
            return;

         bson fbson;
         // read data to bson
         FJson file( fileName);
         file.LoadBson( &fbson );

         // setup data from bson
         queryFromBson( fbson.data );
         bson_destroy( &fbson );
      }
     catch(bsonio_exeption& e)
     {
         QMessageBox::critical( this, e.title(), e.what() );
     }
     catch(std::exception& e)
      {
         QMessageBox::critical( this, "std::exception", e.what() );
      }

}

// Save current record to DB file as new
void QueryWidget::CmCreateInsert()
{
    try
    {
        if( dbquery.get() == 0 )
          return;

        bson obj;
        queryToBson(&obj);
        string key = dbquery->SetBson( &obj );
        bson_destroy(&obj);
        dbquery->InsertRecord();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void QueryWidget::CmRead()
{
    try
    {
        if( dbquery.get() == 0 )
          return;

        // Select keys to load
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        dbquery->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to read/view", aValList, 0 );
         if( !selDlg.exec() )
          return;

        string reckey = aKeyList[selDlg.selIndex()];
        // Read Record
        dbquery->GetRecord( reckey.c_str() );
        bson obj;
        dbquery->GetBson( &obj );
        queryFromBson( obj.data );
        bson_destroy(&obj);
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void QueryWidget::CmUpdate()
{
    try
    {
       if( dbquery.get() == 0 )
          return;

        bson obj;
        queryToBson(&obj);
        string key = dbquery->SetBson( &obj );
        bson_destroy(&obj);
        dbquery->SaveRecord( key.c_str() );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void QueryWidget::CmDelete()
{
    try
    {
        if( dbquery.get() == 0 )
          return;
        // get current key
        string key = qName->text().toUtf8().data();
        key+=":";
        dbquery->DelRecord( key.c_str() );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void QueryWidget::CmExecute()
{
  try
  {
    BSONUIBase* parentdlg = dynamic_cast<BSONUIBase*>(resultWidget);
    if(parentdlg)
      parentdlg->setQuery( this );
  }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void QueryWidget::CmSelectFields()
{
 try
  {
    SchemaSelectDialog dlg( this, "Please, mark data fields for the ouput table",
                            _schemaName, defaultQr.getFieldsCollect());
    if( !dlg.exec() )
       return;

    queryDef.setFieldsCollect( dlg.allSelected());
    lstmodel->setStringList( queryDef.getFieldsCollect() );
  }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void QueryWidget::CmAddSelectFields()
{
  try
  {
    vector<string> _flist;
    SchemaSelectDialog dlg(this, "Please, mark addition data fields for the ouput table",
                           _schemaName, _flist);
    if( !dlg.exec() )
       return;

    _flist = dlg.allSelected();
    queryDef.addFieldsCollect(_flist);
    lstmodel->setStringList( queryDef.getFieldsCollect() );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
   }
}

void QueryWidget::CmSelectRestore()
{
    queryDef.setFieldsCollect( defaultQr.getFieldsCollect() );
    lstmodel->setStringList( queryDef.getFieldsCollect() );
}

void QueryWidget::CmWhereApply()
{
  try
  {
    TQueryModel* _model =(TQueryModel*)(queryEdit->model());
    string qrstr = _model->getQuery(DBQueryDef::qEJDB );
    if( !qrstr.empty() )
      ui->queryText->setText(qrstr.c_str());
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmWhereRestore()
{
   ui->queryText->setText(defaultQr.getEJDBQuery().c_str());
}

void QueryWidget::CmAddWhereFields()
{
  try
  {
    vector<string> _flist;
    vector<SchInternalData> _ftype;
    SchemaSelectDialog dlg( this, "Please, mark data fields for the query table",
                          _schemaName, _flist);
    if( !dlg.exec() )
       return;

    _flist = dlg.allSelected();
    _ftype = dlg.allSelectedTypes();
    queryEdit->CmAddObjects( _flist, _ftype );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmClearTree()
{
   queryEdit->ClearAll();
}


/// Return query ( Where )
const DBQueryDef& QueryWidget::getQueryDef()
{
    queryDef.setKeyName( qName->text().toUtf8().data());
    queryDef.setComment( ui->qComment->text().toUtf8().data());
    queryDef.setToSchema( ui->qSchema->currentText().toUtf8().data());
    //if( ui->pSparQl->isChecked() )
    //  queryDef.setStyle( ui->pSparQl->isChecked());
    //else
      queryDef.setEJDBQuery(ui->queryText->toPlainText().toUtf8().data());
    return queryDef;
}

//-------------internal

// set data to bson
void QueryWidget::queryToBson( bson *fbson )
{
     DBQueryDef quer = getQueryDef();
     quer.toBson(fbson);
}


void QueryWidget::queryFromBson( const char *fbson )
{
   queryDef.fromBson(fbson);
   qName->setText(queryDef.getKeyName().c_str());
   ui->qComment->setText(queryDef.getComment().c_str());
   ui->qSchema->setCurrentText( queryDef.getToSchema().c_str());
   if( queryDef.getStyle() == 1 )
      ui->pSparQl->setChecked(true);
   else
      ui->pEJDB->setChecked(true);
   lstmodel->setStringList( queryDef.getFieldsCollect() );
   ui->queryText->setText(queryDef.getEJDBQuery().c_str());
}

void QueryWidget::resetDBClient()
{
 try{

      TDBSchemaDocument* newClient = TDBSchemaDocument::newDBSchemaDocument(
                  uiSettings().database(), "Query", "queries" );
      // no schema
      if( newClient == nullptr )
         dbquery.reset();
      else
         dbquery.reset( newClient );
    }
    catch(std::exception& e)
      {
         cout << "Internal comment " << e.what() << endl;
         throw;
      }
}

} // namespace bsonui
