//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file FormatImpexWidget.h
/// Declaration of FormatImpexWidget - work with Foreign Import/Export File Format
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef IMPEXWIDGET_H
#define IMPEXWIDGET_H

#include <QCloseEvent>
#include "BSONUIBase.h"
#include "model_node.h"
#ifdef addBSONIO
#include "thrift_import.h"
#include "dbschemadoc.h"
#else
#include "bsonio/thrift_import.h"
#include "bsonio/dbschemadoc.h"
#endif

namespace Ui {
class FormatImpexWidget;
}

namespace bsonui {

using  ExecuteFunction = std::function<void( bsonio::AbstractIEFile *inputIEFile,
                                             const string& dataFile, bool overwrite )>;

TDBSchemaDocument* newDBImpexClient(  const string& queryString  );
bsonio::StructDataIEFile* connectFormatStructDataFile( int amode,
                  const ThriftSchema* schema, bson *formatBson, const string& lua_lib_path );
bsonio::KeyValueIEFile* connectFormatKeyValueFile( int amode,
         const ThriftSchema* schema, bson *formatBson, const string& lua_lib_path );
bsonio::TableIEFile* connectFormatTableFile( int amode,
         const ThriftSchema* schema, bson *formatBson, const string& lua_lib_path );

bsonio::AbstractIEFile* connectFormatDataFile( int amode, const string& impexSchemaName, bson *formatBson );


/// \class FormatImpexWidget - Widget to work with Foreign Import/Export File Format
class FormatImpexWidget : public BSONUIBase
{
    Q_OBJECT

    // Internal data
    int _mode;
    //string curSchemaName = ""; ///< Current impex format
    const string curCollectionName = "impexdefs";
    std::shared_ptr<bsonio::TDBSchemaDocument> dbquery;
    bson curRecord;
    bool contentsChanged = false;
    string parentSchemaName;

    // Work functions
    ExecuteFunction _execF;

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson(const string& schemaName );

    void closeEvent(QCloseEvent* e);

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

    void resetDBClient();
    void impexToBson( bson *obj );
    void impexFromBson( const char *obj );
    bsonio::AbstractIEFile* connectFormatFile( bson *formatBson );

public slots:

    // internal slots
    void openRecordKey(  const string& , bool  = false  ) {}
    void openRecordKey(  const QModelIndex&  ) {}
    void changeKeyList() {}
    void objectChanged()
       { contentsChanged = true; }
    void schemaChanged(const QString & text);

    // File
    void CmNew();
    //void CmClone();
    void CmExportJSON();
    void CmImportJSON();
    void CmSelectDataFile();
    void CmRunScript();

    // Record
    void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();

public:

    enum MODE_ {
         editMode = 0, runModeImport = 1, runModeExport = 2
       };

    explicit FormatImpexWidget( int mode, const string& aschemaName = "", QWidget *parent = 0);
    ~FormatImpexWidget();

    /// Set function to execute script
    void setExecuteFunction(ExecuteFunction afunc )
    {
       _execF = afunc;
    }

    void setQuery( QueryWidget*  ) {}

private:

    Ui::FormatImpexWidget *ui;

    // tree view
    QStringList aHeaderData;
    TSchemaNodeModel* model_schema;
    QItemDelegate *deleg_schema;
    TBsonView* fieldTable;

};

} // namespace bsonui

#endif // IMPEXWIDGET_H
