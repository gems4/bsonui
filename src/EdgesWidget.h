//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file EdgesWidget.h
/// Declaration of class EdgesWidget - Widget to work with edges -
/// internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef EDGESWIDGET_H
#define EDGESWIDGET_H

#include "BSONUIBase.h"
#include "model_node.h"
#include "dbkeysmodel.h"

namespace Ui {
class EdgesWidget;
}

namespace bsonui {

/// \class EdgesWidget - window to insert/update  Edges (TDBGraph)
class EdgesWidget : public BSONUIBase
{
    Q_OBJECT

    // Internal data
    //string curSchemaName = "";
    const vector<string> edgeNames;
    vector<string> vertexNames;
    bool isDefaultQuery = false;
    bool isResetQuery = false;
    string defaultQuery;
    bson curRecord;
    bool resetInOutQuery = false;
    bool contentsChanged = false;

    // Work functions

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson(const string& schemaName );
    /// Reset current DB client
    void resetDBClient(const string& schemaName );
    /// Reset Incoming and Outgoing DB clients
    void resetInOutDBClients();
    void resetTypeBox( const QString& text );

    void closeEvent(QCloseEvent* e);

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

signals:

    /// Emit signal, when delete recod(s)
    void edgeDeleted();

protected slots:

    void typeChanged(const QString & text);

public slots:

    // internal slots
    void openRecordKey(  const string& key, bool resetInOutQuery = false  );
    void openRecordKey(  const QModelIndex& index, bool resetInOutQuery = false );
    void changeKeyList()
    {    keysModel->changeKeyList( &curRecord );  }

    void addIncoming(  const QModelIndex& index );
    void addOutgoing(  const QModelIndex& index );
    void objectChanged()
       { contentsChanged = true; }

    // View
    void CmFollowInVertex();
    void CmFollowOutVertex();
    /// Reload keys after delete vertex
    void updateAllKeys();

   // Record
    //void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();
    void CmDeleteSelect();
    void CmDisplaySearchResult();
    void CmSearchQuery();
    void CmBackupQueriedRecordstoFile();
    void CmRestoreMultipleRecordsfromFile();
    void CmBackupGraphtoFile();
    void CmRestoreGraphfromFile();

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();
    void CmIncomingQuery();
    void CmOutgoingQuery();

    void CmHelpContens();

public:
    explicit EdgesWidget( const string& schemaName, const string& queryString, QWidget *parent = 0);
    ~EdgesWidget();

    /// Update query
    void setQuery( QueryWidget* queryW  );

private:

    Ui::EdgesWidget *ui;

    // edit record view
    QStringList aHeaderData;
    TSchemaNodeModel* model_schema;
    QItemDelegate *deleg_schema;
    TBsonView* fieldTable;

    // keys list data
    std::shared_ptr<bsonio::TDBEdgeDocument> dbgraph;
    DBKeysModel *keysModel; ///< Keys list model
    TKeyTable *keysTable;  ///< Keys list table

    // incoming vertexes list data
    string queryInStr;
    std::shared_ptr<bsonio::TDBVertexDocument> inDB;
    DBKeysModel *inKeysModel; ///< incoming Keys list model
    TKeyTable *inKeysTable;  ///< incoming Keys list table

    // outgoing vertexes list data
    string queryOutStr;
    std::shared_ptr<bsonio::TDBVertexDocument> outDB;
    DBKeysModel *outKeysModel; ///< outgoing Keys list model
    TKeyTable *outKeysTable;  ///< outgoing Keys list table

};

} // namespace bsonui

#endif // EDGESWINDOW_H
