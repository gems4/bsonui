//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file VertexWidget.cpp
/// Implementation of class VertexWidget - Widget to work vertex
/// internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QMessageBox>
#include <QKeyEvent>
#include "VertexWidget.h"
#include "ui_VertexWidget.h"
using namespace bsonio;

namespace bsonui {

void VertexWidget::closeEvent(QCloseEvent* e)
{
    if( keysModel )
     keysModel->Close();

    if( !onCloseEvent(this) )
           e->ignore();
       else
           QWidget::closeEvent(e);
}


VertexWidget::VertexWidget( const string& aschemaName, QWidget *parent) :
    BSONUIBase( aschemaName, parent),
    /*curSchemaName(aschemaName),*/ ui(new Ui::VertexWidget),
    keysModel(0), keysTable(0)
{

   ui->setupUi(this);
   ui->keySplitter->setStretchFactor(0, 2);
   ui->keySplitter->setStretchFactor(1, 1);
   ui->mainSplitter->setStretchFactor(0, 1);
   ui->mainSplitter->setStretchFactor(1, 4);

   setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
   QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
   setWindowTitle(title);

   //set up main parameters
   bson_init(&curRecord);
   bson_finish(&curRecord);

   // define edit tree view
   aHeaderData << "key" << "value" << "comment" ;
   fieldTable =  new TBsonView(  ui->bsonWidget );
   model_schema = new TSchemaNodeModel(  ioSettings().Schema(), &curRecord,
   curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
   deleg_schema = new TSchemaNodeDelegate();
   fieldTable->setModel(model_schema);
   fieldTable->setItemDelegate(deleg_schema);
   fieldTable->setColumnWidth( 0, 250 );
   fieldTable->expandToDepth(0);
   ui->verticalLayout->addWidget(fieldTable);

   // define key table
   keysTable = new TKeyTable( ui->keylistWidget, [=]( const QModelIndex& index ){openRecordKey( index );});
   ui->horizontalLayout->addWidget(keysTable);
   //disconnect( keysTable, SIGNAL(customContextMenuRequested(QPoint)),
   //       keysTable, SLOT(slotPopupContextMenu(QPoint)));
   connect( keysTable, SIGNAL( clicked(const QModelIndex& ) ), this, SLOT(openRecordKey( const QModelIndex& )));
   resetDBClient( curSchemaName );

   // define menu
   setActions();

   // reset in/out queries
   if(  keysTable->model()->rowCount() < 1 )
       // set up default data
       CmNew();
   else
       // read first line
       openRecordKey( keysTable->model()->index(0,0) );

}

VertexWidget::~VertexWidget()
{
    if( keysModel )
      delete keysModel;
    if( keysTable )
      delete keysTable;
    delete ui;
    //cout << "~VertexWidget" << endl;
}

//-----------------------------------------------------

void VertexWidget::resetDBClient(const string& schemaName_ )
{
  try{
      TDBVertexDocument* newClient = TDBVertexDocument::newDBVertexDocument(
                  uiSettings().database(), schemaName_ );
      // no schema
      if( newClient == nullptr  )
      {
        dbgraph.reset();
        ui->queryEdit->setText("");
      }
      else
      {
       ui->queryEdit->setText(newClient->GetLastQuery().c_str());
       dbgraph.reset( newClient );
      }

      if( !keysModel )
        keysModel = new DBKeysModel( dbgraph, keysTable, this );
      else
        keysModel->resetDBClient(dbgraph);

   }
   catch(std::exception& e)
      {
         cout << "Internal comment " << e.what() << endl;
         throw;
      }
}


bool VertexWidget::resetBson(const string& schemaName )
{
   // test legal schema name
   if( !schemaName.empty() && schemaName != curSchemaName )
   return false;
   fieldTable->updateModelData( &curRecord, curSchemaName );
   fieldTable->update();
   return true;
}

void VertexWidget::openRecordKey(  const QModelIndex& rowindex    )
{
    // find key
    int row = rowindex.row();
    QModelIndex index = rowindex.sibling( row, 0);
    string key = keysTable->model()->data(index).toString().toUtf8().data();
    // Read Record
    openRecordKey(  key  );
}

void VertexWidget::openRecordKey(  const string& reckey, bool  )
{
  try{
        // Read Record
        dbgraph->GetRecord( reckey.c_str() );
        bson_destroy( &curRecord );
        dbgraph->GetBson(&curRecord);
        resetBson( ""/*dbclient->getKeywd()*/ );
        contentsChanged = false;
      }
    catch(bsonio_exeption& e)
    {
            QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
    {
            QMessageBox::critical( this, "std::exception", e.what() );
    }
}

//-------------------------------------------------------------

/// Change current View menu selections
void VertexWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TSchemaNodeModel::showComments );
    ui->action_Display_enums->setChecked(TSchemaNodeModel::useEnumNames);
    ui->action_Edit_id->setChecked(TSchemaNodeModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TBsonView::expandedFields );
}

/// Change model ( show/hide comments)
void VertexWidget::updtModel()
{
      model_schema->updateModelData();
      fieldTable->header()->resizeSection(0, 250);
      fieldTable->header()->resizeSection(1, 250);
      fieldTable->expandToDepth(0);
}

/// Change table (Show Enum Names Instead of Values)
void VertexWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void VertexWidget::updtDB()
{
    // set up new dbclient
    //resetDBClient( curSchemaName );
    dbgraph->runQuery();
    keysModel->changeKeyList("");
    // ??? clear current record
    CmNew();
    // show new key list
    // ??? changeKeyList();
}


} // namespace bsonui
