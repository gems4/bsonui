//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file EdgesWidget.cpp
/// Implementation of class EdgesWidget - Widget to work with edges
/// internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io)).
//

#include <QMessageBox>
#include <QLineEdit>
#include "EdgesWidget.h"
#include "ui_EdgesWidget.h"
#include "SelectDialog.h"
#ifdef addBSONIO
#include "json2cfg.h"
#include "traversal.h"
#else
#include "bsonio/json2cfg.h"
#include "bsonio/traversal.h"
#endif
using namespace bsonio;

void helpWin( const string& name, const string& item );

namespace bsonui {

//  Connect all actions
void EdgesWidget::setActions()
{
    connect( ui->typeBox, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));
    //if( isDefaultQuery )
    // ui->typeBox->setDisabled(true);
    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->action_Clone_Structured_Data_Object , SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
    connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
    connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));
    updateViewMenu();
    connect( ui->actionFollow_In_Vertex, SIGNAL( triggered()), this, SLOT(CmFollowInVertex()));
    connect( ui->actionFollow_Out_Vertex, SIGNAL( triggered()), this, SLOT(CmFollowOutVertex()));

    // Record
    connect( ui->actionNext_Record, SIGNAL( triggered()), keysTable, SLOT(CmNext()));
    connect( ui->actionPrevious_Record, SIGNAL( triggered()), keysTable, SLOT(CmPrevious()));
    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    //connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->actionDeleteMultiple, SIGNAL( triggered()), this, SLOT(CmDeleteSelect()));
    connect( ui->actionSearch_Results, SIGNAL( triggered()), this, SLOT(CmDisplaySearchResult()));
    if( isDefaultQuery )
    {
        ui->actionSearch->setDisabled(true);
        ui->action_Read->setDisabled(true);
    }
    else
    {    connect( ui->actionSearch, SIGNAL( triggered()), this, SLOT(CmSearchQuery()));
         connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));
    }
    connect( ui->actionBackup_Queried_Records_to_File, SIGNAL( triggered()),
               this,  SLOT(CmBackupQueriedRecordstoFile()));
    connect( ui->actionRestore_Multiple_Records_from_File, SIGNAL( triggered()),
               this, SLOT(CmRestoreMultipleRecordsfromFile()));
    connect( ui->actionBackup_Graph_to_File, SIGNAL( triggered()),
               this,  SLOT(CmBackupGraphtoFile()));
    connect( ui->actionRestore_Graph_from_File, SIGNAL( triggered()),
               this, SLOT(CmRestoreGraphfromFile()));
    connect( ui->actionIncoming_Vertex_Query, SIGNAL( triggered()), this, SLOT(CmIncomingQuery()));
    connect( ui->actionOutgoing_Vertex_Query, SIGNAL( triggered()), this, SLOT(CmOutgoingQuery()));

    QLineEdit* pLineTask = new QLineEdit( ui->nameToolBar );
    pLineTask->setEnabled( true );
    pLineTask->setFocusPolicy( Qt::ClickFocus );
    pLineTask->setReadOnly( true );
    pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  curSchemaName.c_str();
    pLineTask->setText(title);
    ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    ui->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );

}

// Menu commands -----------------------------------------------------------

void EdgesWidget::CmHelpContens()
{
  helpWin( "Edges", curSchemaName );
}

/// Set default bson record
void EdgesWidget::CmNew()
{
  try{
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          resetBson("");
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Clone bson record (clear _id)
void EdgesWidget::CmClone()
{
  try{
        string key = "";
        model_schema->setValue("_id",key);
        fieldTable->hide();
        fieldTable->show();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/* Save current record to DB file as new
void EdgesWidget::CmCreateInsert()
{
    try
    {
        if( dbclient.get() == 0 )
          return;
        string stroid = genOid();
        model_schema->setOid(stroid);
        string recBsonText;
        jsonFromBson( curRecord.data, recBsonText );
        dbclient->SetJsonYaml( recBsonText,  true );
        dbclient->InsertRecord( );
        contentsChanged = false;
        fieldTable->hide();
        fieldTable->show();
        changeKeyList( pTable, tableModel,dbclient->getKeyFromBson( curRecord.data ) );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}
*/

/// Read new record from DB
void EdgesWidget::CmRead()
{
    try
    {
        if( dbgraph.get() == 0 )
          return;

        // Select keys to load
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        dbgraph->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to read/view", aValList, 0 );
         if( !selDlg.exec() )
          return;

        string reckey = aKeyList[selDlg.selIndex()];
        // Read Record
        openRecordKey(  reckey  );
        contentsChanged = false;
        changeKeyList();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void EdgesWidget::CmUpdate()
{
    try
    {
       if( dbgraph.get() == 0 )
          return;

        //string recBsonText;
        // get current key
        string key = dbgraph->getKeyFromBson( curRecord.data );
        if( !dbgraph->Find( key.c_str() ))
        {
            string stroid = dbgraph->genOid();
            model_schema->setOid(stroid);
            //jsonFromBson( curRecord.data, recBsonText );
            //dbclient->SetJsonYaml( recBsonText,  true );
            dbgraph->SetBson(&curRecord);
            dbgraph->InsertRecord();
            fieldTable->hide();
            fieldTable->show();
        }
        else
        {
            //jsonFromBson( curRecord.data, recBsonText );
            //dbclient->SetJsonYaml( recBsonText,  true );
            dbgraph->SetBson(&curRecord);
            dbgraph->SaveRecord( key.c_str() );
        }
        contentsChanged = false;
        changeKeyList();
        // ??? might be error if changes into key
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void EdgesWidget::CmDelete()
{
    try
    {
        if( dbgraph.get() == 0 )
          return;

        // get current key
        string key = dbgraph->getKeyFromBson( curRecord.data );
        dbgraph->DelRecord( key.c_str() );
        contentsChanged = false;

        keysModel->changeKeyList(  key );
        emit edgeDeleted();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Delete records from List
void EdgesWidget::CmDeleteSelect()
{
    try
    {
        if( dbgraph.get() == 0 )
          return;

        // Select keys to delete
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        vector<int> selNdx;
        dbgraph->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to delete", aValList, selNdx );
         if( !selDlg.exec() )
             return;

        selNdx =  selDlg.allSelected();
        for( uint ii=0; ii<selNdx.size(); ii++ )
        { string key = aKeyList[selNdx[ii]];
          dbgraph->DelRecord( key.c_str() );
        }
        changeKeyList();
        emit edgeDeleted();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void EdgesWidget::CmImportJSON()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select Structured Data file", "*.json"  ))
       {
          FJson file( fileName);
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          if( !resetBson(schemafromName( fileName )) )
          {
            model_schema->saveToBson();
            bsonioErr( fileName , "Try to read another schema format file" );
          }
          contentsChanged = true;
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void EdgesWidget::CmExportJSON()
{
   try {
         string fileName = fileShemaExt(curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select Structured Data file to write the data", "*.json", fileName  ))
         {
            FJson file( fileName);
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void EdgesWidget::CmDisplaySearchResult()
{
  try
  {
     keysModel->showResult("Edges Query Result window");
  }
  catch(std::exception& e)
  {
        QMessageBox::critical( this, "CmDisplaySearchResult", e.what() );
  }
}

void EdgesWidget::CmSearchQuery()
{

  try
  {
      vector<string> schemalst;
      schemalst.push_back(curSchemaName);
      keysModel->showQuery( "Edges Query Widget", schemalst );
  }
  catch(std::exception& e)
  {
      QMessageBox::critical( this, "CmSearchQuery", e.what() );
  }
}

void EdgesWidget::setQuery( QueryWidget* queryW  )
{
  keysModel->setOneQuery( queryW );
  ui->edgeQuery->setText( dbgraph->GetLastQuery().c_str() );
  inKeysModel->setOneQuery( queryW );
  outKeysModel->setOneQuery( queryW );
}

/// Read multiple records from file fileName
void EdgesWidget::CmRestoreMultipleRecordsfromFile()
{
  dbgraph->resetMode(true);
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with json object", "*.json"  ))
       {
            bool overwrite = ioSettings().overwrite();
            FJsonArray file( fileName);
            file.Open( OpenModeTypes::ReadOnly );
            while( file.LoadNext( &curRecord ) )
               dbgraph->SaveBson( &curRecord, overwrite, false );
            file.Close();
        }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
   dbgraph->resetMode(false);
   string _schema = dbgraph->GetSchemaName();
   if( _schema != curSchemaName )
   {
       ui->typeBox->setCurrentText(_schema.c_str());
       //typeChanged(_schema.c_str() );
   }
   resetBson( "" );
   contentsChanged = false;
   changeKeyList( );
}

/// Write multiple records to file fileName
void EdgesWidget::CmBackupQueriedRecordstoFile()
{
    dbgraph->resetMode(true);
    try {
         string fileName = fileShemaExt(curSchemaName,"json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write (document) records", "*.json", fileName  ))
         {
            FJsonArray file( fileName);

            // Select keys to delete
            vector<string> aKeyList;
            vector<vector<string>> aValList;
            vector<int> selNdx;
            dbgraph->GetKeyValueList( aKeyList, aValList );
            if( aKeyList.empty() )
              return;

            SelectDialog selDlg( this, "Please, select a record to delete", aValList, selNdx );
             if( !selDlg.exec() )
                 return;

            selNdx =  selDlg.allSelected();
            file.Open( OpenModeTypes::WriteOnly );

            for( uint ii=0; ii<selNdx.size(); ii++ )
            {
                string key = aKeyList[selNdx[ii]];
                dbgraph->GetRecord( key.c_str() );
                //string valDB = dbclient->GetJson();
                bson obj;
                //jsonToBson( &obj, valDB );
                dbgraph->GetBson(&obj);
                file.SaveNext(&obj);
                bson_destroy(&obj);
            }
            file.Close();
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
    dbgraph->resetMode(false);
    string _schema = dbgraph->GetSchemaName();
    if( _schema != curSchemaName )
    {
        ui->typeBox->setCurrentText(_schema.c_str());
        //typeChanged(_schema.c_str() );
    }
}


/// Set up incoming query
void EdgesWidget::CmIncomingQuery()
{
    try
    {
        inKeysModel->showQuery( "Incoming Query Widget", vertexNames );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Incoming Query", e.what() );
    }
}

/// Set up incoming query
void EdgesWidget::CmOutgoingQuery()
{
    try
    {
        outKeysModel->showQuery( "Outgoing Query Widget", vertexNames );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Outgoing Query", e.what() );
    }
}

/// Show incoming vertex window
void EdgesWidget::CmFollowInVertex()
{
    try
    {
        string _inV;
        model_schema->getValue("_inV",_inV);
        if( _inV.empty()  )
          return;
        string schemaName = inDB->getSchemaFromId(  _inV  );
        if( !schemaName.empty() )
            showOtherWindow( true, schemaName, _inV+":", "" );

    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "follow incoming vertex", e.what() );
    }
}

/// Show outgoing vertex window
void EdgesWidget::CmFollowOutVertex()
{
    try
    {
        string _outV;
        model_schema->getValue("_outV",_outV);
        if( _outV.empty()  )
          return;
        string schemaName = outDB->getSchemaFromId(  _outV  );
        if( !schemaName.empty() )
            showOtherWindow( true, schemaName, _outV+":", "" );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow outgoing vertex", e.what() );
    }
}

/// Read multiple records from file fileName
void EdgesWidget::CmRestoreGraphfromFile()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with json object", "*.json"  ))
        {
            GraphTraversal travel( uiSettings().database());
            travel.RestoreGraphFromFile(fileName, ioSettings().overwrite() );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
   resetBson( "" );
   contentsChanged = false;
   updateDB();
}

/// Write multiple records to file fileName
void EdgesWidget::CmBackupGraphtoFile()
{
    if( dbgraph.get() == 0 )
       return;

    FJsonArray* file = 0;

    try {
         string fileName = fileShemaExt(curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write (document) records", "*.json", fileName  ))
         {
            file = new FJsonArray( fileName);

            // Select keys to delete
            vector<string> aKeyList;
            vector<vector<string>> aValList;
            vector<int> selNdx;
            dbgraph->GetKeyValueList( aKeyList, aValList );
            if( aKeyList.empty() )
              return;

            SelectDialog selDlg( this, "Please, select a record to backup", aValList, selNdx );
             if( !selDlg.exec() )
                 return;

            selNdx =  selDlg.allSelected();
            vector<string> idList;
            for( auto const &ent : selNdx)
            {
                string key = aKeyList[ent];
                strip_all( key, ":" );
                idList.push_back(key);
            }

            GraphTraversal travel( uiSettings().database() );

            file->Open( OpenModeTypes::WriteOnly );

            GraphElementFunction afunc =  [file]( bool , bson *data )
                    {
                       file->SaveNext(data);
                    };


            travel.Traversal( true, idList, afunc );
            file->Close();
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
    delete file;
}

} // namespace bsonui

// end of EdgesMenu.cpp
