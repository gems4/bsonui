//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file BSONUIWidget.h
/// BSONUIWidget - Widget to work with bson
/// or internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef BSONUIWIDGET_H
#define BSONUIWIDGET_H

#include <QCloseEvent>
#include "BSONUIBase.h"
#include "model_bson.h"
#ifdef addBSONIO
#include "dbschemadoc.h"
#else
#include "bsonio/dbschemadoc.h"
#endif

namespace Ui {
class BSONUIWidget;
}

namespace bsonui {

/// \class BSONUIWidget - window to work with bson or internal
/// DOM based on our JSON schemas data
class BSONUIWidget : public BSONUIBase
{
    Q_OBJECT

    // Internal data
    string curCollectionName = "";
    //::apache::thrift::stdcxx::shared_ptr<bsonio::TDBSchemaDocument> dbquery;
    std::shared_ptr<bsonio::TDBSchemaDocument> dbquery;
    bson curRecord;
    bool contentsChanged = false;

    // Work functions

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson(const string& schemaName );

    void closeEvent(QCloseEvent* e);

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

    void resetDBClient();

public slots:

    // internal slots
    void openRecordKey(  const string& , bool  = false  ) {}
    void openRecordKey(  const QModelIndex&  ) {}
    void changeKeyList() {}
    void objectChanged()
       { contentsChanged = true; }

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();
    void CmExportYAML();
    void CmImportYAML();
    void CmExportXML();
    void CmImportXML();

    // Record
    void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();

public:

    explicit BSONUIWidget( const string& aschemaName = "",
                           const string& collectName = "", QWidget *parent = 0);
    ~BSONUIWidget();

    void setQuery( QueryWidget*  ) {}

private:

    Ui::BSONUIWidget *ui;

    // tree view
    QStringList aHeaderData;
    TBsonAbstractModel* model_schema;
    QItemDelegate *deleg_schema;
    TBsonView* fieldTable;

};

} // namespace bsonui

#endif // TBSONUIWIDGET_H
