//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file BSONUIWidget.cpp
///Implementation of class  BSONUIWidget - Widget to work with bson
/// or internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <QKeyEvent>
#include <QLineEdit>
#include "BSONUIWidget.h"
#include "ui_BSONUIWidget.h"
#include "SelectDialog.h"
#include "model_node.h"
#ifdef addBSONIO
#include "json2cfg.h"
#else
#include "bsonio/json2cfg.h"
#endif
using namespace bsonio;

namespace bsonui {


void BSONUIWidget::closeEvent(QCloseEvent* e)
{
    if( !onCloseEvent(this) )
           e->ignore();
       else
           QWidget::closeEvent(e);
}


BSONUIWidget::BSONUIWidget( const string& aschemaName, const string& collectName, QWidget *parent) :
    BSONUIBase( aschemaName, parent),
    curCollectionName(collectName), ui(new Ui::BSONUIWidget)
{

   ui->setupUi(this);
   setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

   QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
   setWindowTitle(title);

   //set up main parameters
   bson_init(&curRecord);
   bson_finish(&curRecord);

   // define edit tree view
   aHeaderData << "key" << "value"  ;
   fieldTable =  new TBsonView(  ui->bsonWidget );
   if( curSchemaName.empty() )
   {
     model_schema = new TBsonModel(  &curRecord, aHeaderData, this/*ui->centralWidget*/ );
     deleg_schema =  new TBsonDelegate();
   }
   else
   {
     aHeaderData << "comment" ;
     model_schema = new TSchemaNodeModel(  ioSettings().Schema(), &curRecord,
        curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
     deleg_schema = new TSchemaNodeDelegate();
   }
   fieldTable->setModel(model_schema);
   fieldTable->setItemDelegate(deleg_schema);
   fieldTable->setColumnWidth( 0, 250 );
   fieldTable->expandToDepth(0);
   ui->verticalLayout->addWidget(fieldTable);

   // define menu
   setActions();
   resetDBClient();
   // set up default data
   CmNew();
}

BSONUIWidget::~BSONUIWidget()
{
    delete ui;
    //cout << "~BSONUIWidget" << endl;
}

bool BSONUIWidget::resetBson(const string& schemaName )
{
   // test legal schema name
   if( !curSchemaName.empty() &&
       !schemaName.empty() && schemaName != curSchemaName )
       return false;
   fieldTable->updateModelData( &curRecord, curSchemaName );
   fieldTable->update();
   return true;
}

//-------------------------------------------------------------


/// Change current View menu selections
void BSONUIWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TSchemaNodeModel::showComments );
    ui->action_Display_enums->setChecked(TSchemaNodeModel::useEnumNames);
    ui->action_Edit_id->setChecked(TSchemaNodeModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TBsonView::expandedFields );
}

/// Change model ( show/hide comments)
void BSONUIWidget::updtModel()
{
  if( !curSchemaName.empty() )
  {
      model_schema->updateModelData();
      fieldTable->header()->resizeSection(0, 250);
      fieldTable->header()->resizeSection(1, 250);
      fieldTable->expandToDepth(0);
  }
}

/// Change table (Show Enum Names Instead of Values)
void BSONUIWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void BSONUIWidget::updtDB()
{
  //resetDBClient();
  dbquery->runQuery();
}


//--------------------------------------------------------------------

//  Connect all actions
void BSONUIWidget::setActions()
{
    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->action_Clone_Structured_Data_Object , SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));
    connect( ui->action_Export_YAML_File, SIGNAL( triggered()), this, SLOT(CmExportYAML()));
    connect( ui->actionImport_YAML_File, SIGNAL( triggered()), this, SLOT(CmImportYAML()));
    connect( ui->actionExport_XML_File, SIGNAL( triggered()), this, SLOT(CmExportXML()));
    connect( ui->actionImport_XML_File, SIGNAL( triggered()), this, SLOT(CmImportXML()));

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    if( !curSchemaName.empty() )
    {   connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
        connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
        connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    }else
      {  ui->actionRemove_Alternatives_Union->setVisible(false);
         ui->action_Add_fields->setVisible(false);
         ui->actionReset_Data_to_Defaults->setVisible(false);
      }
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));
    updateViewMenu();

    if( !curCollectionName.empty())
    { connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
      connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
      connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
      connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));
    }
    else
      ui->menu_Database->menuAction()->setVisible(false);

   if( !curSchemaName.empty() )
   { QLineEdit* pLineTask = new QLineEdit( ui->nameToolBar );
     pLineTask->setEnabled( true );
     pLineTask->setFocusPolicy( Qt::ClickFocus );
     pLineTask->setReadOnly( true );
     pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
     QString title =  curSchemaName.c_str();
     pLineTask->setText(title);
     ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
     ui->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
   }
   else
   {
       ui->mainToolBar->hide();
   }
}

// Menu commands -----------------------------------------------------------


/// Set default bson record
void BSONUIWidget::CmNew()
{
  try{
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          resetBson("");
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Clone bson record (clear _id)
void BSONUIWidget::CmClone()
{
  try{
        TSchemaNodeModel*model = dynamic_cast<TSchemaNodeModel*>(model_schema);
        if( model)
        {  string key ="";
           model->setValue("_id",key);
           fieldTable->hide();
           fieldTable->show();
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Save current record to DB file as new
void BSONUIWidget::CmCreateInsert()
{
    try
    {
        if( dbquery.get() == 0 )
          return;
        string key = dbquery->SetBson( &curRecord );
        dbquery->InsertRecord();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void BSONUIWidget::CmRead()
{
    try
    {
        if( dbquery.get() == 0 )
          return;

        // Select keys to load
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        dbquery->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to read/view", aValList, 0 );
         if( !selDlg.exec() )
          return;

        string reckey = aKeyList[selDlg.selIndex()];
        // Read Record
        dbquery->GetRecord( reckey.c_str() );
        bson_destroy( &curRecord );
        dbquery->GetBson(&curRecord  );
        resetBson("");
        contentsChanged = true;

   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void BSONUIWidget::CmUpdate()
{
    try
    {
       if( dbquery.get() == 0 )
          return;
        string key = dbquery->SetBson( &curRecord );
        dbquery->SaveRecord( key.c_str() );
        contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void BSONUIWidget::CmDelete()
{
    try
    {
        if( dbquery.get() == 0 )
          return;
        // get current key
        string key = dbquery->getKeyFromBson( curRecord.data );
        dbquery->DelRecord( key.c_str() );
        contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void BSONUIWidget::CmImportJSON()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with JSON object", "*.json"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::Json_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          if( !resetBson(schemafromName( fileName )) )
          {
            model_schema->saveToBson();
            bsonioErr( fileName , "Try to read another schema format file" );
          }
          contentsChanged = true;
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void BSONUIWidget::CmExportJSON()
{
   try {
         string fileName = fileShemaExt( curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", "*.json", fileName  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::Json_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from yaml file fileName
void BSONUIWidget::CmImportYAML()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with YAML data object", "*.yaml"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::Yaml_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          if( !resetBson(schemafromName( fileName )) )
          {
            model_schema->saveToBson();
            bsonioErr( fileName , "Try to read another schema format file" );
          }
          contentsChanged = true;
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to YAML file fileName
void BSONUIWidget::CmExportYAML()
{
   try {
        string fileName = fileShemaExt(curSchemaName, "yaml");
        if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", "*.yaml", fileName  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::Yaml_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson record from xml file fileName
void BSONUIWidget::CmImportXML()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with xml object to read", "*.xml"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::XML_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          if( !resetBson(schemafromName( fileName )) )
          {
            model_schema->saveToBson();
            bsonioErr( fileName , "Try to read another schema format file" );
          }
          contentsChanged = true;
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to file fileName
void BSONUIWidget::CmExportXML()
{
   try {
        string fileName = fileShemaExt(curSchemaName, "xml");
        if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", "*.xml", fileName  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::XML_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void BSONUIWidget::resetDBClient()
{
  if( curCollectionName.empty())
       return;

  try{
      vector<KeyFldsData> keyFldsInf = { KeyFldsData( "name", "name", "Only name" )};
      bsonio::TDBSchemaDocument* newClient = TDBSchemaDocument::newDBSchemaDocument(
           uiSettings().database(), curSchemaName, curCollectionName, keyFldsInf,  "" );
      // no schema
      if( newClient == nullptr )
       dbquery.reset();
      else
       dbquery.reset( newClient );

    }
   catch(std::exception& e)
    {
       cout << "Internal comment " << e.what() << endl;
       throw;
    }
 }

} // namespace bsonui

// end of BSONUIWidget.cpp
