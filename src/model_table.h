//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_table.h
/// Declarations of classes TAbstractDataContainer, TMatrixModel,
/// TMatrixDelegate and TMatrixTable - implements flexible approach
/// provided by Qt's model/view architecture according TAbstractDataContainer
/// interface.
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef model_table_h
#define model_table_h

#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QTableView>
#ifdef addBSONIO
#include "nservice.h"
#else
#include "bsonio/nservice.h"
#endif
using namespace std;

namespace bsonui {


class Selection;

/// Item types into table
enum DATA_TYPES {
     ftString = 0,
     ftDouble = 1,
     ftNumeric = 2
};

/// Interface for table data container
class TAbstractDataContainer
{
 protected:
    QString tableName;

 public:

   TAbstractDataContainer( const char * aname ):
       tableName(aname){}
   virtual ~TAbstractDataContainer()
   {}

   // data to model
   virtual int rowCount() const = 0;
   virtual int columnCount() const = 0;
   virtual QVariant data( int line, int column ) const = 0;
   virtual bool setData( int line, int column, const QVariant & value ) = 0;
   virtual QString headerData ( int section ) const = 0;
   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return true; }
   virtual int getType( int /*line*/, int /*column*/ ) const
   { return ftString; }
   virtual QString getToolTip( int line, int column ) const
   { return QString("%1[%2,%3]").arg(tableName).arg(line).arg(column); }
   virtual void resetData() = 0;

};


enum NewRole{

   TypeRole = Qt::UserRole
};

///  \class TMatrixModel
/// class for represents the data set and is responsible for fetchin
/// the data is neaded for viewing and for writing back any changes.
/// Reading/writing data from/to inherited classes from TAbstractDataContainer interface
class TMatrixModel: public QAbstractTableModel
{
    friend class TMatrixDelegate;
    friend class TMatrixTable;

	Q_OBJECT
	
    TAbstractDataContainer *datacontainer;

public:
	  
     TMatrixModel( TAbstractDataContainer *adata, QObject * parent = 0 ):
         QAbstractTableModel(parent), datacontainer(adata)
     {
       if( parent )
         connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
                   parent,  SLOT(objectChanged()) );
     }
     ~TMatrixModel()
     { }

     int rowCount( const QModelIndex& /*parent*/ ) const
     {
       return datacontainer->rowCount();
     }
     int columnCount ( const QModelIndex& /*parent*/  ) const
     {
       return datacontainer->columnCount();
     }

     QVariant data ( const QModelIndex & index, int role ) const;
	 bool setData ( const QModelIndex & index, const QVariant & value, int role );
     QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
     Qt::ItemFlags flags( const QModelIndex & index ) const;

     void resetMatrixData();
};


///  \class TMatrixDelegate
/// individual items in views are rendered and edited using delegates
class TMatrixDelegate: public QItemDelegate
{
    Q_OBJECT

 public:

   TMatrixDelegate(    QObject * parent = 0 ):
       QItemDelegate( parent )
   { }
   QWidget *createEditor(QWidget *parent,
                         const QStyleOptionViewItem &option,
                         const QModelIndex &index) const;
};


///  \class TMatrixTable implements a table view
/// that displays items from a TMatrixModel model.
class TMatrixTable: public QTableView
{
        Q_OBJECT

     int _modeUsing;

     Selection getSelectionRange( bool paste_ = false );
     QString createString( Selection& sel, char _splitCol );
     QString createHeader(char _splitCol);
     void pasteIntoArea( Selection& sel, bool transpose, char _splitCol);
     void setFromString(char splitrow, const QString& str,
                Selection sel, bool transpose);

 protected:
    void keyPressEvent(QKeyEvent* e);

 protected slots:
    void slotPopupContextMenu(const QPoint& pos);

 public slots:
    void SelectRow();
    void SelectColumn();
    void SelectAll();
    void CopyData();
    void CopyDataHeader();
    // only if editable
    void CmCalc();
    void CutData();
    void ClearData();
    void PasteData();
    void PasteTransposedData();

 public:

    /// Types of Matrix table mode
    enum TABLE_TYPES {
           tbSort = 0x0010,  ///< Added sorting into colums
           tbGraph = 0x0004, ///< Connect 2d graphic for columns
           tbEdit = 0x0002,  ///< Enable editing
           tbNoMenu = 0x0001, ///< Disable context menu
           tbShow = 0         ///< Use only show mode
       };


    TMatrixTable( QWidget * parent = 0, int mode = tbEdit );

    /// Save selected data to csv format file
    void saveSelectedToCSV(const string& fileName);
    /// Save all table data to csv format file
    void saveAllToCSV(const string& fileName)
    {
      selectAll();
      saveSelectedToCSV( fileName );
      clearSelection();
    }
    /// Load new table from csv format file
    void loadAllFromCSV(const string& /*fileName*/)
    {}

    void makePopupContextMenu(QMenu *menu, QModelIndex index );
};

///  \class TMatrixUniqueSelection implements a table view
/// that displays items from a TMatrixModel model.
/// Can be selected only unique values into defined column.
class TMatrixUniqueSelection: public TMatrixTable
{
    Q_OBJECT

    int _columnUnique;

 protected slots:

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
    {
        TMatrixTable::selectionChanged( selected, deselected);
        slotSelectionChange(selected, deselected);

    }
    void slotSelectionChange(const QItemSelection &, const QItemSelection &);

 public:

    TMatrixUniqueSelection( int colUniq, QWidget * parent = 0, int mode = tbEdit );

    /// Return all selected rows
    set<int> allSelectedRows();
};


/// Class for double vector container
class TDoubleVectorContainer : public TAbstractDataContainer
{
    string  _key;
    vector<double>& _fields;

 public:

   TDoubleVectorContainer( const char * aname, const string& akey, vector<double>& afields ):
      TAbstractDataContainer(aname),
      _key(akey),_fields( afields )
   { }

   virtual ~TDoubleVectorContainer() {}

   int rowCount() const
   { return _fields.size();  }

   int columnCount() const
   { return 1; }

   QVariant data( int line, int /*column*/ ) const
   { return _fields[line];  }

   bool setData( int line, int /*column*/, const QVariant &value )
   {
       _fields[line] = value.toDouble();
       return true;
   }

   virtual QString headerData ( int /*section*/ ) const
   {  return _key.c_str();   }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   {  return true;  }

   virtual int getType( int /*line*/, int /*column*/ ) const
   {  return ftDouble;  }

   virtual QString getToolTip( int /*line*/, int /*column*/ ) const
   { return _key.c_str(); }

   void resetData()
   { }
};

/// Class for internal table data container
class StringTable : public TAbstractDataContainer
{
    vector< string > colHeads;
    bsonio::ValuesTable matrix;
    bool isEditable = false;

 public:

   /// Construct table from list of headers.
   StringTable( const char * aname, const vector<string>& heads ):
      TAbstractDataContainer(aname), colHeads (heads)
   {}

   /// Construct table from 2D array.
   StringTable( const char * aname, const vector<string>& heads,
                             const bsonio::ValuesTable& values ):
   TAbstractDataContainer(aname), colHeads(heads)
   {
     for( auto val : values )
       addRow( val );
   }

   virtual ~StringTable()
   {}

   void setEditable( bool editable )
   {
     isEditable = editable;
   }

   const bsonio::ValuesTable&  getValues() const
   {
       return matrix;
   }

   void updateValues( const bsonio::ValuesTable& table )
   {
       matrix = table;
   }

   void updateHeads( const vector<string>& heads )
   {
       colHeads = heads;
   }

   void addRow( const vector<string>& linevalues )
   {
       if( linevalues.size() >= (uint)columnCount())
          matrix.push_back(linevalues);
   }

   void addRow( initializer_list<string> list )
   {
       if( list.size() >= (uint)columnCount())
          matrix.push_back(list);
   }

   template<typename... T>
   void addRow(T&&... params)
   {
       vector<string> vec = {params...};
       addRow(vec);
   }

   vector<string> getColumn( uint column ) const
   {
      vector<string> keys;
       for( auto row: matrix)
         if( column < row.size())
             keys.push_back( "" );
          else
            keys.push_back( row[column]);
       return keys;
   }

   const vector<string>& getRow( int row ) const
   {
       if( row >= rowCount() )
          bsonio::bsonioErr( "getRow", "Illegal row");
       return matrix[row];
   }

   // data to model ---------------------------

   int rowCount() const
   { return matrix.size();  }

   int columnCount() const
   { return colHeads.size(); }

   QVariant data( int line, int column ) const
   {  return matrix[line][column].c_str(); }

   bool setData( int line, int column, const QVariant & value )
   {
       matrix[line][column] = value.toString().toStdString();
       return true;
   }

   virtual QString headerData ( int section ) const
   { return colHeads[section].c_str(); }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return isEditable; }

   virtual int getType( int /*line*/, int /*column*/ ) const
   { return ftString; }

   virtual QString getToolTip( int line, int /*column*/ ) const
   { return QString("%1").arg(line); }

   void resetData()
   { }

};

// -----------------------------------------------  List model


///  \class TStringVectorModel
/// class for represents the data set and is responsible for fetchin
/// the data is neaded for viewing and for writing back any changes.
/// Reading/writing data from/to string vector
class TStringVectorModel : public QAbstractListModel
{
    Q_OBJECT

    vector<string> lst;

public:

    explicit TStringVectorModel(const vector<string>& strs, QObject *parent = 0):
        QAbstractListModel(parent), lst(strs)
    {}

    int rowCount(const QModelIndex &parent = QModelIndex()) const
    {
        if (parent.isValid())
            return 0;
        return lst.size();
    }

    QVariant data(const QModelIndex &index, int role) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    vector<string> stringList() const
    {
      return lst;
    }
    void setStringList(const vector<string>& strs);

};


} // namespace bsonui

#endif   // model_table_h
