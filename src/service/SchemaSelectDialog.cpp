//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file SchemaSelectDialog.cpp
/// Implementation of class SchemaSelectDialog - dialog for single or multiple
/// selection from schema tree
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QPushButton>
#include "SchemaSelectDialog.h"
#include "ui_SchemaSelectDialog4.h"
#include "model_node.h"
#include "preferences.h"
using namespace bsonio;

namespace bsonui {

void SchemaSelectDialog::setupWindow(const char* title )
{
    ui->setupUi(this);
    setWindowTitle(title);

    // set up model
    QStringList aHeaderData;
    aHeaderData << "key" << "indexes" << "comment"  ;
    _model_schema = new TSelectSchemaModel(
                ioSettings().Schema(), _schemaName, aHeaderData, this );
    ui->schemaView->setModel(_model_schema);
    ui->schemaView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->schemaView->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    ui->schemaView->header()->resizeSection(0, 250);
    ui->schemaView->expandToDepth(0);

    if(_multi)
    {
        QPushButton * btn = ui->buttonBox->addButton(QDialogButtonBox::Reset);
        QObject::connect( btn, SIGNAL(clicked()), this, SLOT(CmReset()));
        btn = ui->buttonBox->addButton(QDialogButtonBox::RestoreDefaults);
        QObject::connect( btn, SIGNAL(clicked()), this, SLOT(CmDefault()));
    }
    else
    {
        ui->schemaView->setSelectionMode(QAbstractItemView::SingleSelection);
    }

    ui->schemaView->setFocus();
    QObject::connect( ui->buttonBox, SIGNAL(helpRequested()), this, SLOT(CmHelp()));
}


// Select constructor
SchemaSelectDialog::SchemaSelectDialog( QWidget* parent, const char* title,
                 const string& schemaName,
                 const vector<string>& adef_fieldsList ):
    QDialog(parent), _multi(true), _schemaName(schemaName), def_fieldsList(adef_fieldsList),
    ui(new Ui::SchemaSelectDialogData)
{
  setupWindow( title);
  defineMultiSelection( def_fieldsList );
}


SchemaSelectDialog::SchemaSelectDialog( QWidget* parent, const char* title,
                  const string& schemaName ):
    QDialog(parent), _multi(false), _schemaName(schemaName),
    ui(new Ui::SchemaSelectDialogData)
{
  setupWindow( title );
}


// Destructor
SchemaSelectDialog::~SchemaSelectDialog()
 {
    delete ui;
 }

// Select field by fieldpath
bool SchemaSelectDialog::selectField(  TSelectSchemaModel* _model,
   const QModelIndex& parIndex, queue<string> names )
{
   SelectSchemaLine* item =  _model->lineFromIndex(parIndex);
   for( int ii=0; ii<item->levels; ii++ )  //skip array and map names
     if( !names.empty() )
        names.pop();

    // select current line
    if( names.empty() )
    {    ui->schemaView->selectionModel()->select( parIndex,
            QItemSelectionModel::Rows|QItemSelectionModel::Select );
       return true;
    }

    // test children
    string fname = names.front();
    names.pop();

    QModelIndex _index;
    for(int rw = 0; rw < _model->rowCount( parIndex ); rw++ )
    {
      _index = _model->index( rw, 0, parIndex);
      string fldname = _model->data( _index, Qt::DisplayRole ).toString().toUtf8().data();
      if( fldname == fname )
       return selectField( _model, _index, names );
   }

   return false;  // not found
}


void SchemaSelectDialog::defineMultiSelection( const vector<string>& sel )
{
    ui->schemaView->setSelectionMode(QAbstractItemView::MultiSelection);
    TSelectSchemaModel* _model =dynamic_cast<TSelectSchemaModel*>( ui->schemaView->model());
    if( !_model )
        return;

    for( uint jj=0; jj< sel.size(); jj++ )
    {
      queue<string> names = split(sel[jj], ".");
      selectField( _model, ui->schemaView->rootIndex(), names );
    }
}


void SchemaSelectDialog::CmReset()
{
    ui->schemaView->clearSelection();
    //defineMultiSelection( _database->GetQueryFields() );
}


void SchemaSelectDialog::CmDefault()
{
    ui->schemaView->clearSelection();
    defineMultiSelection( def_fieldsList );
}


void SchemaSelectDialog::selectWithChildren(
        TSelectSchemaModel* _model,  const QModelIndex& parIndex,
    const QModelIndexList& selIndexes, vector<string>& arr, vector<SchInternalData>& aschdata )
{
  QModelIndex index;
  for(int rw = 0; rw < _model->rowCount( parIndex ); rw++ )
   {
      index = _model->index( rw, 0, parIndex);
      if( selIndexes.contains( index ) )
      {
        SelectSchemaLine* item =  _model->lineFromIndex(index);
        item->addLink(arr);
        aschdata.push_back( item->thiftdata );
      }
      selectWithChildren( _model, index, selIndexes, arr, aschdata );
   }
}

vector<string> SchemaSelectDialog::allSelected()
{
    vector<string> arr;
    if( !result() )
        return arr;
    QModelIndexList selection = ui->schemaView->selectionModel()->selectedRows();
    TSelectSchemaModel* _model =dynamic_cast<TSelectSchemaModel*>( ui->schemaView->model());
    if( !_model )
        return arr;

    // Multiple rows can be selected (order of selection)
    //for(int i=0; i< selection.count(); i++)
    // _model->lineFromIndex(selection.at(i))->addLink(arr);

    // Multiple rows can be selected (order of  structure)
    selectWithChildren( _model, ui->schemaView->rootIndex(), selection, arr, _arrsch );

    return arr;
}

string SchemaSelectDialog::selIndex( SchInternalData* flddata )
{
    QModelIndex index = ui->schemaView->currentIndex();

    if (index.isValid())
    {
       const TSelectSchemaModel* _model =
               dynamic_cast<const TSelectSchemaModel*>( index.model());
       vector<string> selected;
       if( _model )
       {
          SelectSchemaLine* item =  _model->lineFromIndex(index);
          item->addLink( selected );
          if( flddata )
            *flddata = item->thiftdata;
          return selected[0];
       }
   }
   return "";
}


void SchemaSelectDialog::CmHelp()
{ }


//------------------------------------------------------------------------

SelectSchemaLine::SelectSchemaLine( ThriftFieldDef*  afldDef, SelectSchemaLine* aparent ):
    ndx( 0 ),  type(0), levels(0), defValue(""), parent(aparent)
{
  if( parent )
  {
    ndx = parent->children.size();
    parent->children.push_back( unique_ptr<SelectSchemaLine>(this) );
  }

  if( afldDef )
  {  fldValue.append(afldDef->fName.c_str());
     fldValue.append("");
     //if( TBsonSchemaModel::showComments  )
     fldValue.append(afldDef->fDoc.c_str());
     thiftdata.thrifttype = afldDef->fTypeId.back();
     thiftdata.enumname = afldDef->className;
  } else
    {  fldValue.append("root");
       fldValue.append("");
       //if( TBsonSchemaModel::showComments  )
         fldValue.append("");
    }
}

SelectSchemaLine::~SelectSchemaLine()
{
    children.clear();
}

void SelectSchemaLine::addLink( vector<string>& fieldsList, const string& path )
{
  if( !parent )
  {
      fieldsList.push_back(path);
      return;
  }

  string new_path = fldValue[0].toUtf8().data();
  if( type > 0  && !fldValue[1].isEmpty() )
  {
     string fielndx =  fldValue[1].toUtf8().data();
     queue<string> names = split(fielndx, ";");
     string curname;
     while(  !names.empty() )
     {
       if( !names.front().empty() )
       {  curname = new_path + "." + names.front();
          if( !path.empty() )
             curname += "."+ path;
          parent->addLink( fieldsList, curname );
       }
       names.pop();
     }
  }
   else
   {
      if( !path.empty())
        new_path += "."+ path;
      parent->addLink( fieldsList, new_path );
   }
}

// Set up struct without bson data
void TSelectSchemaModel::struct2model( ThriftStructDef* strDef, SelectSchemaLine* parent )
{
   ThriftFieldDef*  fldDef;
   for(uint ii=0; ii<strDef->fields.size(); ii++ )
   {
       fldDef = &strDef->fields[ii];
       field2model( fldDef, parent );
   }
}

void TSelectSchemaModel::field2model( ThriftFieldDef*  fldDef, SelectSchemaLine* parent )
{
  // add line to tree view
  SelectSchemaLine *line = new SelectSchemaLine( fldDef, parent );
  // add levels for objects and arrays
  list2model( 0, fldDef, line );
}

// Read array
void TSelectSchemaModel::list2model( int level,
     ThriftFieldDef*  fldDef, SelectSchemaLine* line )
{
    // add levels for objects and arrays
    switch (fldDef->fTypeId[level])
    {
       // main constructions
       case T_STRUCT:
               {
                  // structure
                  if( fldDef->className.empty() )
                      bsonioErr("bson_to_list" , "Undefined struct name into schema" );
                  ThriftStructDef* strDef2 = schema->getStruct( fldDef->className);
                  if( strDef2 == nullptr )
                     bsonioErr( fldDef->className , "Undefined struct definition" );
                  struct2model( strDef2,  line );
                }
               break;
       case T_MAP:
               { // map
                 line->type = 2;
                 line->addValue("key");
                 list2model( level+2, fldDef, line );
               }
               break;
        case T_LIST:
        case T_SET:
             { // !! next level
                if(line->type == 0)
                  line->type = 1;
                line->addValue("0");
                list2model( level+1, fldDef, line );
             }
       default:     break;
    }
}


//--------------------------------------------------------------------------------------
// class TSelectSchemaModel
// class for represents the data set and is responsible for fetching the data
// is needed for viewing and for selecting.
// Selecting data from ThriftSchema object
//---------------------------------------------------------------------------------------

TSelectSchemaModel::TSelectSchemaModel( const ThriftSchema* aschema, const string& aschemaName,
                      const QStringList& aHeaderData,   QObject* parent ):
        QAbstractItemModel(parent), hdData( aHeaderData),
        schema(aschema), schemaName(aschemaName)
{
  strDef = nullptr;
  if( !schemaName.empty() )
     strDef =  schema->getStruct(schemaName);
  if( strDef == nullptr )
    bsonioErr( schemaName, "Undefined struct definition" );

  // set up model data
  rootNode = new SelectSchemaLine( 0,  0 );
  struct2model(strDef,  rootNode );

  //connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
  //          parent,  SLOT(objectChanged()) );
}

TSelectSchemaModel::~TSelectSchemaModel()
{
  if(rootNode )
     delete rootNode;
}

SelectSchemaLine *TSelectSchemaModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<SelectSchemaLine *>(index.internalPointer());
    } else {
        return rootNode;
    }
}

QModelIndex TSelectSchemaModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    SelectSchemaLine *parentItem = lineFromIndex( parent );
    return createIndex(row, column, parentItem->children[row].get());
}

QModelIndex TSelectSchemaModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    SelectSchemaLine *childItem = lineFromIndex(child);
    SelectSchemaLine *parentItem = childItem->parent;
    if (parentItem == rootNode )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}


int TSelectSchemaModel::rowCount( const QModelIndex& parent ) const
{
   if (!rootNode)
       return 0;
  if (parent.column() > 0)
      return 0;
  SelectSchemaLine *parentItem = lineFromIndex( parent );
  return parentItem->children.size();
}

int TSelectSchemaModel::columnCount( const QModelIndex& /*parent*/ ) const
{
  if( TSchemaNodeModel::showComments  )
     return 3;
   return 2;
}

Qt::ItemFlags TSelectSchemaModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  SelectSchemaLine *item = lineFromIndex( index );
  if( index.column() == 1 && (item->type > 0 ) )
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }
  else
     return (flags & ~Qt::ItemIsEditable);
}

QVariant TSelectSchemaModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
   if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<hdData.size())
        return hdData[section];
   return QVariant();
}

QString TSelectSchemaModel::getDescription( const QModelIndex& index ) const
{
    SelectSchemaLine *item = lineFromIndex( index );
    return item->fldValue[2];
}

QVariant TSelectSchemaModel::data( const QModelIndex& index, int role ) const
{
   if(!index.isValid())
     return QVariant();

   switch( role )
   {
     case Qt::DisplayRole:
          {   SelectSchemaLine *item = lineFromIndex( index );
              if( index.column() < item->fldValue.size()  )
                   return item->fldValue[index.column()];
          }
     case Qt::EditRole:
            {  SelectSchemaLine *item = lineFromIndex( index );
               if( index.column() < item->fldValue.size()  )
                   return item->fldValue[index.column()];
            }
      case Qt::ToolTipRole:
      case Qt::StatusTipRole:
              return  getDescription(index );
      case Qt::ForegroundRole:
          {
            if ( index.column() == 0   )
                 return QVariant( QColor( Qt::darkCyan ) );
          }
      default: break;
   }
   return QVariant();
}

bool TSelectSchemaModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
       SelectSchemaLine *line =lineFromIndex(index);
       if(  index.column() <  line->fldValue.size() )
          {
             line->fldValue[index.column()] = QVariant(value).toString();
          }
     return true;
    }
    return false;
}

} // namespace bsonui

//--------------------- End of SchemaSelectDialog.cpp ---------------------------

