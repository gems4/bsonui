//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file dbkeysmodel.cpp
/// Implementation of classes TKeyListTableNew, TKeyTable and DBKeysModel
/// implements data container, widget and model to work with data base record&keys.
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QKeyEvent>
#include <QMessageBox>
#include <QSortFilterProxyModel>
#include <QHeaderView>
#include "dbkeysmodel.h"
#include "QueryWidget.h"
#include "TableEditWindow.h"
#include "BSONUIBase.h"
using namespace bsonio;

namespace bsonui {


DBKeysModel::DBKeysModel( const std::shared_ptr<bsonio::TDBVertexDocument>& adbgraph,
                          TKeyTable *atable, BSONUIBase *aparent ):
  parent(aparent), table(atable),
  dataTable(0), tableModel(0), proxyModel(0), queryWindow(0), resultWindow(0)
{
   defKeysTable();
   resetDBClient( adbgraph );
}

DBKeysModel::~DBKeysModel()
{
    Close();

    if(queryWindow)
       delete queryWindow;
    if(resultWindow)
       delete resultWindow;

    if( dataTable )
      delete dataTable;

    if( proxyModel )
      delete proxyModel;

    //cout << "~DBKeysModel" << endl;
}


/// Close addition windows
void DBKeysModel::Close()
{
  if( queryWindow )
    queryWindow->close();
  if( resultWindow )
    resultWindow->close();
}

void DBKeysModel::resetDBClient( const std::shared_ptr<bsonio::TDBVertexDocument>& adbgraph )
{
   dbgraph = adbgraph;
   resetKeysTable();
   if(queryWindow)
        queryWindow->resetDBClient();
}

void DBKeysModel::defKeysTable()
{
    //table->horizontalHeader()->hide();
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->horizontalHeader()->setSectionResizeMode( QHeaderView::Interactive );
    table->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    table->setSortingEnabled(true);
    disconnect( table, SIGNAL(customContextMenuRequested(QPoint)),
           table, SLOT(slotPopupContextMenu(QPoint)));
}

void DBKeysModel::resetKeysTable()
{
    if( dataTable )
      delete dataTable;
    dataTable = new TKeyListTableNew("select", dbgraph.get());
    if( proxyModel )
      delete proxyModel;
    tableModel = new TMatrixModel( dataTable, parent );
    proxyModel = new QSortFilterProxyModel();
    proxyModel->setSourceModel( tableModel );
    table->setModel(proxyModel);
    table->hideColumn(0); // !!!! do not hide for Edges
}

void DBKeysModel::changeKeyList( const string& key )
{
    // reset model data
    tableModel->resetMatrixData();
    if( resultWindow )
     resultWindow->updateTable();

    // undefined current record
    if( key.empty() ||  key.find_first_of("*?") != std::string::npos )
      return;

    table->setColumnHidden(0, false);  // ??? not hide to edges
    // search `item` text in model
    QModelIndexList Items =  table->model()->match(  table->model()->index(0, 0),
       Qt::DisplayRole, QString(key.c_str()));
    if (!Items.isEmpty())
    {
        table->selectionModel()->select(Items.first(), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        table->scrollTo(Items.first(), QAbstractItemView::EnsureVisible);
    }
    table->setColumnHidden(0, true); // ??? not hide to edges

}

void DBKeysModel::showQuery( const char* title, const vector<string>& schemalst )
{
  if( dbgraph.get() == 0 )
       return;

  // define new dialog
  if(!queryWindow)
      {
          queryWindow = new QueryWidget( title, schemalst, dbgraph->getQuery(), parent, parent );
      }
  else
      {
         queryWindow->Update( dbgraph->getQuery());
         queryWindow->raise();
      }
  queryWindow->show();

}

void DBKeysModel::setOneQuery( QueryWidget* queryW )
{
    if( queryW !=queryWindow || dbgraph.get() == 0 )
         return;

    DBQueryDef oldquery = dbgraph->getQuery();
    try
    {
        // reset internal query data
        dbgraph->runQuery( queryW->getQueryDef() );
        // update search tables
        tableModel->resetMatrixData();
        if( resultWindow )
         resultWindow->updateTable();

    }
    catch(std::exception& e)
    {
      dbgraph->setQuery( oldquery );
      QMessageBox::critical( parent, "Set Query", e.what() );
   }
}

void DBKeysModel::showResult( const char* title )
{
    if( dbgraph.get() == 0 )
       return;

    // define new dialog
    if(!resultWindow)
    {
         resultWindow = new TableEditWidget( title, dataTable, TMatrixTable::tbShow|TMatrixTable::tbSort  );
    }
    else
    {
        resultWindow->updateTable();
        resultWindow->raise();
     }
     resultWindow->show();
}


void DBKeysModel::changeQueryToId( const string& id )
{
  string schemaName = dbgraph->getSchemaFromId( id  );
  if( !schemaName.empty() )
  {
      string qStr = dbgraph->idQuery( id );
      dbgraph->runQuery( DBQueryDef( schemaName,
      dbgraph->makeDefaultQueryFields(schemaName), qStr ));
      // update search tables
      tableModel->resetMatrixData();
      if( resultWindow )
       resultWindow->updateTable();
  }
}

void DBKeysModel::resetInOutQuery( const string& id )
{
    dbgraph->GetRecord( (id+":").c_str() );
    dbgraph->resetSchema(dbgraph->GetSchemaName(), true );
    dbgraph->runQuery();
    tableModel->resetMatrixData();
}


//--------------------------------------------------------------------------------


void TKeyTable::keyPressEvent(QKeyEvent* e)
{
    TMatrixTable::keyPressEvent(e);
    switch( e->key() )
    {
      case Qt::Key_Up:
      case Qt::Key_Down:
      case Qt::Key_PageUp:
      case Qt::Key_PageDown:
        { QModelIndex index = currentIndex();
          if (index.isValid())
           _openf( index ); // parentTask->openRecordKey( index );
          setCurrentIndex(index);
        }
        break;
    }
}

void TKeyTable::CmNext()
{
    QModelIndex index = currentIndex();
    if( !index.isValid() )
    {
      if( model()->rowCount() > 0)
        index = model()->index(0,0);
    }
    else
    { int row = index.row();
      row = min( row+1, model()->rowCount()-1);
      index = index.sibling(row, index.column());
    }
    if (index.isValid())
    {
      _openf( index ); //parentTask->openRecordKey( index );
      setCurrentIndex(index);
    }
}

void TKeyTable::CmPrevious()
{
    QModelIndex index = currentIndex();
    if (index.isValid())
    {
      int row = index.row();
      row = max( row-1, 0);
      index = index.sibling(row, index.column());
      _openf( index ); //parentTask->openRecordKey( index );
      setCurrentIndex(index);
    }
}


void TKeyListTable::resetData()
{
    uint ii;
    colHeads.clear();
    matrix.clear();

    // dbclient->runQuery();
    const vector<string>& fields_ = dbclient->GetQueryFields();
    const bsonio::TableOfValues& tabl_ = dbclient->GetQueryResult();

    // get header
    colHeads.push_back( "key" );
    for( ii=0; ii<fields_.size(); ii++ )
      colHeads.push_back( fields_[ii].c_str() );

    auto it = tabl_.begin();
    while( it != tabl_.end() )
    {
        QVector<QVariant> vec;
        matrix.push_back( vec );
        matrix.last().push_back( it->first.c_str() );
        for( ii=0; ii<it->second.size(); ii++ )
         matrix.last().push_back( it->second[ii].c_str() );
        it++;
    }
}

} // namespace bsonui

//--------------------------------------------------------------------------------
