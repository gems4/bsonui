//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file TBSONProtocol.cpp
/// Implementation of bson protocol for Thrift
/// Implements a protocol which uses bson as the wire-format
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include "TBsonProtocol.h"
#ifdef addBSONIO
#include "ar2base.h"
#else
#include "bsonio/ar2base.h"
#endif

namespace bsonui {

const char *keykwd = "kwd";
const char *typekwd = "type";
const char *sizekwd = "size";
const char *valuekwd = "val";

TBSONProtocol::TBSONProtocol(::apache::thrift::stdcxx::shared_ptr<TTransport> ptrans):
    TVirtualProtocol<TBSONProtocol>(ptrans),
    trans_(ptrans.get())
{
    bson_init(&bobj_tansport);
}

TBSONProtocol::~TBSONProtocol()
{
   bson_destroy(&bobj_tansport);
}

// Print bson object to binary file
uint32_t TBSONProtocol::print_bson_object_to_bin( const bson *b)
{
    uint32_t size = static_cast<uint32_t>(b->dataSize);
    this->trans_->write((uint8_t*)&size, 4);
    if (size > 0) {
      this->trans_->write((uint8_t*)b->data, size);
    }
    return size + 4;
}

// Load bson object from binary file
uint32_t TBSONProtocol::read_bson_object_from_bin( bson *b )
{
    union bytes {
      uint8_t b[4];
      int32_t all;
    } theBytes;
    this->trans_->readAll(theBytes.b, 4);
    int32_t size = theBytes.all;

    // Catch error cases
    if (size < 0)
      throw TProtocolException(TProtocolException::NEGATIVE_SIZE);

    // Try to borrow first
    /*const uint8_t* borrow_buf;
    uint32_t got = size;
    if ((borrow_buf = this->trans_->borrow(NULL, &got)))
    {
      //str.assign((const char*)borrow_buf, size);
      this->trans_->consume(size);
      return size;
    }*/

    char *data = new char[ size];
    this->trans_->readAll((uint8_t*)(data), size);

    bson_destroy(b); // free old data
    bson_init_finished_data(b, data);
    b->dataSize = size;

    return size + 4;
}

uint32_t TBSONProtocol::writeMessageBegin(const std::string& name,
                                          const TMessageType messageType,
                                          const int32_t seqid)
{
  uint32_t result = 0;
  tranWriteStart();

  contexts_.push( TBSONWriteContext( name.c_str(), T_LIST, 0, 4 ));
  bson_append_start_array(getBson(), name.c_str() );
  bson_append_int( getBson(), "0", 1 /*kThriftVersion1*/);
  bson_append_string( getBson(), "1", name.c_str() );
  bson_append_int( getBson(), "2", messageType );
  bson_append_int( getBson(), "3", seqid );
  return result;
}

uint32_t TBSONProtocol::writeMessageEnd()
{
  bson_append_finish_array(getBson());
  contexts_.pop();
  return transWriteEnd();
}

uint32_t TBSONProtocol::writeStructBegin(const char* name)
{
  (void)name;
  tranWriteStart();
  if( contexts_.size() > 0 )
      bson_append_start_object(getBson(), contexts_.top().getKey().c_str() );
  return 0;
}

uint32_t TBSONProtocol::writeStructEnd()
{
  if( contexts_.size() > 0 )
    bson_append_finish_object(getBson() );
  return transWriteEnd();
}

uint32_t TBSONProtocol::writeFieldBegin(const char* name,
                                        const TType fieldType,
                                        const int16_t fieldId)
{
  contexts_.push( TBSONWriteContext( valuekwd, fieldType, fieldId, 0 ));
  bson_append_start_object(getBson(), to_string(fieldId).c_str() );
  bson_append_string( getBson(), keykwd, name );
  bson_append_int( getBson(), typekwd, fieldType );
  return 0;
}

uint32_t TBSONProtocol::writeFieldEnd()
{
  bson_append_finish_object(getBson() );
  contexts_.pop();
  return 0;
}

uint32_t TBSONProtocol::writeFieldStop()
{
  return 0;
}

uint32_t TBSONProtocol::writeMapBegin(const TType /*keyType*/,
                                      const TType /*valType*/,
                                      const uint32_t size)
{
  bson_append_int( getBson(), sizekwd, size );
  bson_append_start_object(getBson(), contexts_.top().getName() );
  return 0;
}

uint32_t TBSONProtocol::writeMapEnd()
{
  bson_append_finish_object(getBson() );
  return 0;
}

uint32_t TBSONProtocol::writeListBegin(const TType elemType, const uint32_t size)
{
  bson_append_start_object(getBson(), contexts_.top().getName() );
  bson_append_int( getBson(), typekwd, elemType );
  bson_append_int( getBson(), sizekwd, size );
  bson_append_start_array(getBson(), valuekwd );
  return 0;
}

uint32_t TBSONProtocol::writeListEnd()
{
  bson_append_finish_array(getBson() );
  bson_append_finish_object(getBson());
  return 0;
}

uint32_t TBSONProtocol::writeSetBegin(const TType elemType, const uint32_t size)
{
    bson_append_start_object(getBson(), contexts_.top().getName() );
    bson_append_int( getBson(), typekwd, elemType );
    bson_append_int( getBson(), sizekwd, size );
    bson_append_start_array(getBson(), valuekwd );
   return 0;
}

uint32_t TBSONProtocol::writeSetEnd()
{
    bson_append_finish_array(getBson() );
    bson_append_finish_object(getBson());
    return 0;
}

uint32_t TBSONProtocol::writeBool(const bool value)
{
  string key = contexts_.top().getKey();
  if( key == ismapkey)
    contexts_.top().setMapKey( to_string(value) );
  else
    bson_append_bool( getBson(), key.c_str(), value );
  return 0;
}

uint32_t TBSONProtocol::writeByte(const int8_t byte)
{
  // writeByte() must be handled specially because boost::lexical cast sees
  // int8_t as a text type instead of an integer type
    string key = contexts_.top().getKey();
    if( key == ismapkey)
      contexts_.top().setMapKey( to_string(byte) );
    else
      bson_append_int( getBson(), key.c_str(), (int16_t)byte );
  return 0;
}

uint32_t TBSONProtocol::writeI16(const int16_t i16)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey)
      contexts_.top().setMapKey( to_string(i16) );
    else
      bson_append_int( getBson(), key.c_str(), i16 );
  return 0;
}

uint32_t TBSONProtocol::writeI32(const int32_t i32)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey)
      contexts_.top().setMapKey( to_string(i32) );
    else
       bson_append_int( getBson(), key.c_str(), i32 );
  return 0;
}

uint32_t TBSONProtocol::writeI64(const int64_t i64)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey)
      contexts_.top().setMapKey( to_string(i64) );
    else
      bson_append_long( getBson(), key.c_str(), i64 );
  return 0;
}

uint32_t TBSONProtocol::writeDouble(const double dub)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey)
      contexts_.top().setMapKey( to_string(dub) );
    else
      bson_append_double( getBson(), key.c_str(), dub );
  return 0;
}

uint32_t TBSONProtocol::writeString(const std::string& str)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey)
      contexts_.top().setMapKey( str );
    else
        if( key == JDBIDKEYNAME  )
        {  // oid
            if( !str.empty() && str != bsonio::emptiness )
            { bson_oid_t oid;
              bson_oid_from_string( &oid, str.c_str() );
              bson_append_oid( getBson(), JDBIDKEYNAME, &oid);
            }
        }
       else
         bson_append_string( getBson(), key.c_str(), str.c_str() );
  return 0;
}

uint32_t TBSONProtocol::writeBinary(const std::string& str)
{
    bson_append_binary( getBson(), contexts_.top().getKey().c_str(),
                        0, str.c_str(), str.length() );
    return 0;
}

//--------------- Reading functions --------------------------------------

uint32_t TBSONProtocol::readMessageBegin(std::string& name,
                                         TMessageType& messageType,
                                         int32_t& seqid)
{
    uint32_t returns = transReadStart();

    // get array data
    bson_iterator_next(&bitr.top());
    bson_iterator i = bitr.top();
    name = bson_iterator_key(&i);
    bson_type type = bson_iterator_type(&i);

    if ( type != BSON_ARRAY )
        throw TProtocolException(TProtocolException::INVALID_DATA,
              "Expected BSON array; field \"" + name + "\"");

   const char* arrdata = bson_iterator_value(&i);
   bson_iterator itval;
   bson_iterator_from_buffer(&itval, arrdata);
   readcntxt_.push( TBSONReadContext( name.c_str(), itval, true,  false ) );

   // read first  4 fields
   int kThriftVersion1 = readInt();
   if ( 1 != kThriftVersion1) {
     throw TProtocolException(TProtocolException::BAD_VERSION, "Message contained bad version.");
   }
   readString( name );
   messageType = static_cast<TMessageType>(readInt());
   seqid = readInt();
   return returns;
}

uint32_t TBSONProtocol::readMessageEnd()
{
  transReadEnd();
  return 0;
}

uint32_t TBSONProtocol::readStructBegin(std::string& /*name*/)
{
    uint32_t returns = transReadStart();
    return returns;
}

uint32_t TBSONProtocol::readStructEnd()
{
    transReadEnd();
    return 0;
}

uint32_t TBSONProtocol::readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId)
{
   if( bson_iterator_next(&bitr.top()))
   {
       bson_iterator i = bitr.top();
       string Itname = bson_iterator_key(&i);
       bson_type type = bson_iterator_type(&i);

       if ( type != BSON_OBJECT )
       {
         throw TProtocolException(TProtocolException::INVALID_DATA,
          "Expected BSON object; field \"" + name + "\"");
       }

       fieldId = stoi(Itname);

       const char* data = bson_iterator_value(&i);

       if(!bsonio::bson_find_string( data, keykwd, name ) )
           name=""; // skip field
       int ftype;
       if(!bsonio::bson_find_value( data, typekwd, ftype ) )
       {  ftype =T_VOID;  fieldId=-1; }// skip field
        fieldType = static_cast<TType>(ftype);

       bson_iterator itval;
       bson_find_from_buffer(&itval, data, valuekwd );
       string valkey = bson_iterator_key(&itval);
       if ( valkey != valuekwd )
       {
         throw TProtocolException(TProtocolException::INVALID_DATA,
          "Expected BSON object fval; field \"" + valkey + "\"");
       }
       readcntxt_.push( TBSONReadContext( name.c_str(), itval,
         (fieldType==T_SET || fieldType==T_LIST ),  fieldType==T_MAP ) );

       //  bdata = bson_iterator_value(&itval);

   } else // stop process
      {
        fieldType = ::apache::thrift::protocol::T_STOP;
      }
  return 0;
}

uint32_t TBSONProtocol::readFieldEnd()
{
  readcntxt_.pop();
  return 0;
}

uint32_t TBSONProtocol::readMapBegin(TType& /*keyType*/, TType& /*valType*/, uint32_t& size)
{
   // read size
   if(!bsonio::bson_find_value( bson_iterator_value(&bitr.top()), sizekwd, size ) )
        size=0; // skip field
   return 0;
}

uint32_t TBSONProtocol::readMapEnd()
{
  return 0;
}

uint32_t TBSONProtocol::readListBegin(TType& elemType, uint32_t& size)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    const char* data = bson_iterator_value(it);

    int ftype;
    if(!bsonio::bson_find_value( data, typekwd, ftype ) )
    {  elemType =T_VOID; }// skip field
     elemType = static_cast<TType>(ftype);

     // read size
     if(!bsonio::bson_find_value( data, sizekwd, size ) )
         size=0; // skip field

     bson_iterator itval;
     bson_find_from_buffer(&itval, data, valuekwd );
     string valkey = bson_iterator_key(&itval);
     if ( valkey != valuekwd )
     {
      throw TProtocolException(TProtocolException::INVALID_DATA,
       "Expected BSON object fval; field \"" + valkey + "\"");
     }

     readcntxt_.push( TBSONReadContext( valuekwd, itval, true,  false ) );
     readcntxt_.top().setArray( );
    return 0;
}

uint32_t TBSONProtocol::readListEnd()
{
  readcntxt_.pop();
  return 0;
}

uint32_t TBSONProtocol::readSetBegin(TType& elemType, uint32_t& size)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    const char* data = bson_iterator_value(it);

    int ftype;
    if(!bsonio::bson_find_value( data, typekwd, ftype ) )
    {  elemType =T_VOID; }// skip field
     elemType = static_cast<TType>(ftype);

     // read size
     if(!bsonio::bson_find_value( data, sizekwd, size ) )
         size=0; // skip field

     bson_iterator itval;
     bson_find_from_buffer(&itval, data, valuekwd );
     string valkey = bson_iterator_key(&itval);
     if ( valkey != valuekwd )
     {
      throw TProtocolException(TProtocolException::INVALID_DATA,
       "Expected BSON object fval; field \"" + valkey + "\"");
     }

     readcntxt_.push( TBSONReadContext( valuekwd, itval, true,  false ) );
     readcntxt_.top().setArray( );
    return 0;
}

uint32_t TBSONProtocol::readSetEnd()
{
  readcntxt_.pop();
  return 0;
}

uint32_t TBSONProtocol::readBool(bool& value)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     value = bson_iterator_bool(it);
    else
     value = std::stoi(mapkey);
    return 0;
}

int TBSONProtocol::readInt()
{
    int ii=0;
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     ii = bson_iterator_int(it);
    else
     ii = std::stoi(mapkey);
  return ii;
}


// readByte() must be handled properly because boost::lexical cast sees int8_t
// as a text type instead of an integer type
uint32_t TBSONProtocol::readByte(int8_t& byte)
{
  byte = readInt();
  return 0;
}

uint32_t TBSONProtocol::readI16(int16_t& i16)
{
  i16 = readInt();
  return 0;
}

uint32_t TBSONProtocol::readI32(int32_t& i32)
{
  i32 = readInt();
  return 0;
}

uint32_t TBSONProtocol::readI64(int64_t& i64)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     i64 = bson_iterator_long(it);
    else
     i64 = std::stol(mapkey);
  return 0;
}

uint32_t TBSONProtocol::readDouble(double& dub)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     dub = bson_iterator_double(it);
    else
     dub = std::stod(mapkey);
  return 0;
}

uint32_t TBSONProtocol::readString(std::string& str)
{
   std::string mapkey;
   bson_iterator *it = readcntxt_.top().getIterator( mapkey );
   if( mapkey.empty() )
   {
       if(  readcntxt_.top().getName() == JDBIDKEYNAME   )
       {  // oid
           char oidhex[25];
           bson_oid_to_string(bson_iterator_oid(it), oidhex);
           str = oidhex;
       }
      else
       // str = bson_iterator_string(it);
       {
               bson_type t = bson_iterator_type(it);
               if( !bsonio::bson_to_string( t, it, str ) )
                 str = "";
      }

   }
   else
    str = mapkey;
   return 0;
}

uint32_t TBSONProtocol::readBinary(std::string& str)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    str = bson_iterator_bin_data(it);
    return 0;
}

} // namespace bsonui
