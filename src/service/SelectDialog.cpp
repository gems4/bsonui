//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file SelectDialog.cpp
/// Implementation of class SelectDialog - dialog for single or multiple
/// selection from list/table; ChooseFileOpen, ChooseFileSave - service
/// functions
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QFileInfo>
#include <QFileDialog>
#include "SelectDialog.h"
#include "ui_SelectDialog4.h"
#include "preferences.h"
#include "TCSVPage.h"

namespace bsonui {

SelectTable::SelectTable( const char * aname, const vector<string>& list,
             const char  splitCol ):
    TAbstractDataContainer(aname)
{
    if(list.size() >0 )
    {
      uint ii;
      int jj;
      QString line;
      QStringList cells;

      // get values
      for( ii=0;  ii<list.size(); ii++ )
      {
        line =  list[ii].c_str();
        cells = line.split(splitCol, QString::KeepEmptyParts);
        QVector<QVariant> vec;
        matrix.push_back( vec );
        for( jj=0; jj<cells.count(); jj++ )
         matrix[ii].push_back( cells[jj] );
     }

     // get header
     for( jj=0; jj<cells.count(); jj++ )
       colHeads.push_back( QString("%1").arg(jj) );
  }
}

SelectTable::SelectTable( const char * aname, const vector< vector<string> >& list ):
    TAbstractDataContainer(aname)
{
    if(list.size() >0 )
    {
      uint ii, jj;

      // get header
      for( jj=0; jj< list[0].size(); jj++ )
       colHeads.push_back( QString("%1").arg(jj) );

      // get values
      for( ii=0;  ii<list.size(); ii++ )
      {
        QVector<QVariant> vec;
        matrix.push_back( vec );
        for( jj=0; jj<list[ii].size(); jj++ )
         matrix[ii].push_back( list[ii][jj].c_str() );
     }
    }
}

//----------------------------------------------------------------

// Single select constructor
SelectDialog::SelectDialog( QWidget* parent, const char* title,
              const vector<string>& list,
              uint sel, const char  splitCol, int mode):
    QDialog(parent),
    multi(false), tablemode(mode), data("select",list,splitCol ),
    ui(new Ui::SelectDialogData)
{
   ui->setupUi(this);
   defineSingleSelection(  title, sel );
}

// Single select constructor
SelectDialog::SelectDialog( QWidget* parent, const char* title,
              const vector< vector<string> >& list, uint sel, int mode):
    QDialog(parent),
    multi(false), tablemode(mode), data("select",list ),
    ui(new Ui::SelectDialogData)
{
   ui->setupUi(this);
   defineSingleSelection(  title, sel );
}

// Multiple select constructor
SelectDialog::SelectDialog( QWidget* parent, const char* title,
              const vector<string>& list,
              const vector<int>& sel, const char  splitCol, int mode):
    QDialog(parent),
    multi(true), tablemode(mode), data("select",list,splitCol ),
    ui(new Ui::SelectDialogData)
{
    ui->setupUi(this);
    defineMultiSelection(  title, sel );
}

// Multiple select constructor
SelectDialog::SelectDialog( QWidget* parent, const char* title,
              const vector< vector<string> >& list,
              const vector<int>& sel, int mode ):
    QDialog(parent),
    multi(true), tablemode(mode), data("select", list ),
    ui(new Ui::SelectDialogData)
{
   ui->setupUi(this);
   defineMultiSelection(  title, sel );
}

// Destructor
 SelectDialog::~SelectDialog()
 {
         delete pTable;
         delete ui;
 }


void SelectDialog::defSelectionTable()
{
    TMatrixModel* model = new TMatrixModel( &data, this );
    pTable = new TMatrixTable(this, tablemode/*TMatrixTable::tbNoMenu*/ );
    TMatrixDelegate* deleg = new TMatrixDelegate();
    pTable->setItemDelegate(deleg);

    if( tablemode & TMatrixTable::tbSort)
    { TSortFilterProxyModel *proxyModel = new TSortFilterProxyModel();
      proxyModel->setSourceModel( model );
     pTable->setModel(proxyModel/*modelFitResults*/);
    }
    else
    {
     pTable->setModel(model);
     pTable->horizontalHeader()->hide();
    }
    ui->gridLayout->addWidget(pTable, 1, 0, 1, 5);
    pTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    pTable->setEditTriggers( QAbstractItemView::AnyKeyPressed );
}


void SelectDialog::defineSingleSelection( const char* title, uint sel )
{
    setWindowTitle(title);

    defSelectionTable();
    pTable->setSelectionMode(QAbstractItemView::SingleSelection);
    if(  sel > (uint)pTable->model()->rowCount()  )
        sel = 0;

    TSortFilterProxyModel *proxyModel =0;
    auto tmodel = pTable->model();
    if( tablemode & TMatrixTable::tbSort)
    {
      proxyModel = dynamic_cast<TSortFilterProxyModel *>(tmodel);
      if( proxyModel )
       tmodel =  proxyModel->sourceModel();
    }

   if( proxyModel )
       pTable->setCurrentIndex(  proxyModel->mapFromSource( tmodel->index(sel,0) ) );
    else
       pTable->setCurrentIndex( tmodel->index(sel,0) );

    ui->pButton3->hide();
    ui->pButton2->hide();
    QObject::connect( ui->bHelp, SIGNAL( clicked() ), this, SLOT( CmHelp() ) );
    QObject::connect(pTable, SIGNAL(doubleClicked(const QModelIndex & )), this, SLOT(accept()));
    pTable->setFocus();
}


void SelectDialog::defineMultiSelection( const char* title, const vector<int>& sel_ )
{
    setWindowTitle(title);

    defSelectionTable();
    pTable->setSelectionMode(QAbstractItemView::MultiSelection);

    TSortFilterProxyModel *proxyModel =0;
    auto tmodel = pTable->model();
    if( tablemode & TMatrixTable::tbSort)
    {
      proxyModel = dynamic_cast<TSortFilterProxyModel *>(tmodel);
      if( proxyModel )
       tmodel =  proxyModel->sourceModel();
    }

    /* new */
    vector<int> sel = sel_;
    std::sort(sel.begin(), sel.end());

    QItemSelection selitems;
    int startRow=-1, endRow=-1;
    for( uint jj=0; jj< sel.size(); jj++ )
    {
        if( sel[jj] >= tmodel->rowCount() )
         break;

        if(startRow == -1 )
           startRow = endRow = sel[jj];
        else
          if( endRow+1 == sel[jj] )
             endRow++;
          else
          {
            selitems.merge(QItemSelection(tmodel->index(startRow, 0),
                                          tmodel->index(endRow, 0)),
                           QItemSelectionModel::Select);
            startRow = endRow = sel[jj];
          }
    }

    if(startRow != -1 )
        selitems.merge(QItemSelection(tmodel->index(startRow, 0),
                                      tmodel->index(endRow, 0)),
                       QItemSelectionModel::Select);

    if( (tablemode&TMatrixTable::tbSort) && proxyModel )
    {
       selitems =  proxyModel->mapSelectionFromSource(selitems);
    }

    pTable->selectionModel()->select(selitems,QItemSelectionModel::Rows|
                                     QItemSelectionModel::ClearAndSelect );
    pTable->setFocus();
    QObject::connect( ui->bHelp, SIGNAL( clicked() ), this, SLOT( CmHelp() ) );
}


void SelectDialog::CmSelectAll()
{
    if( multi == true )
      pTable->selectAll();
}


void SelectDialog::CmClearAll()
{
    pTable->clearSelection();
}

/// Returns single selection,
/// returns '-1' if nothing selected
int SelectDialog::selIndex()
{
   if( !( result() && (pTable->model()->rowCount() > 0) ) )
     return -1;
   if( tablemode & TMatrixTable::tbSort)
   {
     TSortFilterProxyModel *proxyModel = dynamic_cast<TSortFilterProxyModel *>(pTable->model());
     if( proxyModel )
      return proxyModel->mapToSource(pTable->currentIndex()).row();
    }
    return pTable->currentIndex().row();
}

vector<int> SelectDialog::allSelected()
{
    vector<int> arr;

    if( !result() )
        return arr;

    QItemSelection selitems = pTable->selectionModel()->selection();
    if( tablemode & TMatrixTable::tbSort)
    {
      TSortFilterProxyModel *proxyModel = dynamic_cast<TSortFilterProxyModel *>(pTable->model());
      if( proxyModel )
       selitems =  proxyModel->mapSelectionToSource(selitems);
     }
    //QModelIndexList selection = pTable->selectionModel()->selectedRows();
    QModelIndexList selection = selitems.indexes();

    // Multiple rows can be selected
    for(int i=0; i< selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        if(index.column() == 0 )
          arr.push_back( index.row() );
    }

    //QModelIndexList indexList = pTable->selectionModel()->selectedIndexes();
    //foreach (QModelIndex index, indexList) {
    //    arr.insert(index.row());
    //}

    return arr;
}

void SelectDialog::CmHelp()
{ }

// Service functions -----------------------------------

//QString UserDir="";

bool ChooseFileOpen(QWidget* par, string& path_,
   const char* title, const char *filter )
{
    QString path;
    if( path_.find('/') == string::npos )
           path  =  QString(bsonio::ioSettings().userDir().c_str())+"/"+path_.c_str();
         else
           path = path_.c_str();

    QString filt;
    if( filter )
        filt = QString("Text files (%1);;All files (*)").arg(filter);
    else
        filt = "All files (*)";

    QString fn = QFileDialog::getOpenFileName(  par, title,
          path, filt, 0, QFileDialog::DontConfirmOverwrite );
#ifdef buildWIN32
    std::replace( fn.begin(), fn.end(), '/', '\\');
#endif
   if ( !fn.isEmpty() )
    {
       QFileInfo flinfo(fn);
       bsonio::ioSettings().setUserDir( flinfo.dir().path().toStdString() );
       path_ = fn.toStdString();
       return true;
    }
    else
    {
        path_ = "";
        return false;
    }

}

bool ChooseFileSave(QWidget* par, string& path_,
       const char* title, const char *filter,  const string& schemafilter )
{
     QString path;
     if( path_.find('/') == string::npos )
       path  =  QString(bsonio::ioSettings().userDir().c_str())+"/"+path_.c_str();
     else
       path = path_.c_str();

    //replace(path.begin(), path.end(),'\\', '/');

    QStringList filt;
    if( !schemafilter.empty() )
        filt += QString("Schema (%1)").arg(schemafilter.c_str());
    if( filter )
        filt += QString("Files (%1)").arg(filter);
    else
        filt += "Text files (*.txt)";

    filt += "All files (*.*)";

    QString selectedFilter;
    QString fn = QFileDialog::getSaveFileName( par, title,
         path, filt.join( ";;" ), &selectedFilter,
         QFileDialog::DontConfirmOverwrite);

    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        bsonio::ioSettings().setUserDir(flinfo.dir().path().toStdString() );
        if( flinfo.suffix().isEmpty() ) // solwing for linux
        {
          int posb = selectedFilter.lastIndexOf(".");
          if( !schemafilter.empty() )
             posb = selectedFilter.indexOf(".");
          int pose = selectedFilter.indexOf(")", posb);
          if( posb > 0 && pose > posb )
          { QString ext = selectedFilter.mid(posb+1, pose-posb-1);
            fn += "."+ext;
          }
        }
        path_ = fn.toStdString();
        return true;
    }
    else
    {
        path_  = "";
        return false;
    }
}

} // namespace bsonui

//--------------------- End of SelectDialog.cpp ---------------------------

