//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file SelectDialog.h
/// Declaration of SelectDialog - dialog for single or multiple
/// selection from list/table; ChooseFileOpen, ChooseFileSave - service
/// functions
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef SelectDialog_included
#define SelectDialog_included

#include <string>
#include <iostream>
#include <set>
#include <QDialog>
#include "model_table.h"
using namespace std;

namespace Ui {
class SelectDialogData;
}

namespace bsonui {

//extern QString UserDir;

/// Function returns true and an existing file selected by the user into param path_.
/// If the user presses Cancel, it returns false
bool ChooseFileOpen(QWidget* par, string& path_,
   const char* title, const char *filter );

/// Function returns true and an existing file selected by the user into param path_.
/// If the user presses Cancel, it returns false
/// The file does not have to exist.
bool ChooseFileSave(QWidget* par, string& path_,
   const char* title, const char *filter,  const string& schemafilter = "" );


/// Class for internal table data container
class SelectTable : public TAbstractDataContainer
{
    QVector< QString > colHeads;
    QVector< QVector<QVariant> > matrix;


 public:

   /// Construct table from list of lines.
   /// Split each line into a list of columns by delimiter
   SelectTable( const char * aname, const vector<string>& list,
                const char  splitCol = '\t' );

   /// Construct table from 2D array.
   SelectTable( const char * aname, const vector< vector<string> >& list );

   virtual ~SelectTable()
   {}

   // data to model

   int rowCount() const
   { return matrix.size();  }

   int columnCount() const
   { return colHeads.size(); }

   QVariant data( int line, int column ) const
   {  return matrix.at( line ).at( column ); }

   bool setData( int /*line*/, int /*column*/, const QVariant & /*value*/ )
   {    //matrix[line].replace(column, value);
        return true;
   }

   virtual QString headerData ( int section ) const
   { return QString("%1").arg(section); }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return false; }

   virtual int getType( int /*line*/, int /*column*/ ) const
   { return ftString; }

   virtual QString getToolTip( int line, int /*column*/ ) const
   { return QString("%1").arg(line); }

   void resetData()
   { }

};

/// The SelectDialog class provides a dialog that allow users to select items from list
class SelectDialog : public QDialog
{
    Q_OBJECT

    bool multi;
    int tablemode;
    SelectTable data;
    TMatrixTable *pTable;

    void defSelectionTable();
    void defineSingleSelection( const char* title, uint sel );
    void defineMultiSelection( const char* title, const vector<int>& sel );

public slots:
    void objectChanged() { }

protected slots:
    void CmHelp();
    virtual void CmSelectAll();
    virtual void CmClearAll();

public:

    /// Single select constructor.
    /// Construct table from list of lines.
    /// Split each line into a list of columns by delimiter
    SelectDialog( QWidget* parent, const char* title,
                  const vector<string>& list,
                  uint sel=0 , const char  splitCol = '\t',
                  int mode = TMatrixTable::tbNoMenu );

    /// Single select constructor.
    /// Construct table from 2D array.
    SelectDialog( QWidget* parent, const char* title,
                  const vector< vector<string> >& list, uint sel=0,
                  int mode = TMatrixTable::tbNoMenu );

    /// Multiple select constructor
    /// Construct table from list of lines.
    /// Split each line into a list of columns by delimiter
    SelectDialog( QWidget* parent, const char* title,
                  const vector<string>& list,
                  const vector<int>& sel, const char  splitCol = '\t',
                  int mode = TMatrixTable::tbNoMenu );

    /// Multiple select constructor
    /// Construct table from 2D array.
    SelectDialog( QWidget* parent, const char* title,
                  const vector< vector<string> >& list,
                  const vector<int>& sel,
                  int mode = TMatrixTable::tbNoMenu );

    /// Destructor
    virtual ~SelectDialog();

    /// Returns single selection,
    /// returns '-1' if nothing selected
    int selIndex();

    /// Returns selection array/
    /// Array is empty if nothing is selected
    vector<int> allSelected();

private:
    Ui::SelectDialogData *ui;

};

} // namespace bsonui

#endif // SelectDialog_included
