//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file SchemaSelectDialog.h
/// Declaration of SchemaSelectDialog - dialog for single or multiple
/// selection fields from schema
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef SchemaSelectDialog_included
#define SchemaSelectDialog_included

#include <string>
#include <iostream>
#include <QDialog>
#include "model_query.h"
using namespace std;

namespace Ui {
class SchemaSelectDialogData;
}

namespace bsonui {

/// \class SelectSchemaLine represents an item in a tree view, and contains several columns of data.
/// Each items data based on thrift field definition
class SelectSchemaLine
{

public:

    /// The constructor is used to record the item's parent and the data associated with each column.
    SelectSchemaLine( ThriftFieldDef*  afldDef, SelectSchemaLine* parent );
    ~SelectSchemaLine();

    /// Set up default template of values for map or array
    void addValue( const char* next )
    {
      if( defValue.isEmpty() )
       defValue = next;
      else
       defValue += QString(".")+next;
       levels++;
       fldValue[1] = defValue;
    }

    /// Add path to list of fields
    void addLink( vector<string>& fieldsList, const string& path = "" );

    // internal data
    int ndx;                             ///< Internal ndx
    int type;                            ///< Type of field ( 1- array, 2- map, 0 - other)
    bsonio::SchInternalData thiftdata;   ///< Type of field ( from ThriftFieldDef )
    int levels;                          ///< Number of levels for array or map
    QString defValue;                    ///< Default value of field
    QVector<QString> fldValue;           ///< Values into columns (key, value, comment)

    SelectSchemaLine *parent;
    vector<unique_ptr<SelectSchemaLine>> children;
};


/// \class TSelectSchemaModel
/// class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for selecting.
/// Each items data based on thrift field definition for current thrift schema
class TSelectSchemaModel: public QAbstractItemModel
{
    Q_OBJECT

    QStringList hdData;
    const ThriftSchema* schema;
    string schemaName;
    ThriftStructDef* strDef;
    SelectSchemaLine* rootNode;

    QString getDescription( const QModelIndex& index ) const;

    // Read internal data from json schema
    void struct2model( ThriftStructDef* strDef, SelectSchemaLine* parent );
    void field2model( ThriftFieldDef* fldDef, SelectSchemaLine* parent );
    void list2model( int level, ThriftFieldDef* fldDef, SelectSchemaLine* parent );

 public:

  /// The constructor for model based on schemaName thrift schema
  TSelectSchemaModel( const ThriftSchema* aschema, const string& schemaName,
              const QStringList& aHeaderData,   QObject* parent = 0 );
  ~TSelectSchemaModel();

  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;     //ok
  int columnCount ( const QModelIndex& parent  ) const; // ok
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant & value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
  Qt::ItemFlags flags ( const QModelIndex& index ) const;

  /// Special function for access to internal item data
  SelectSchemaLine* lineFromIndex(const QModelIndex& index) const;

};

/// \class SchemaSelectDialog class provides a dialog that allow users to select
/// items from thrift schema fields list
class SchemaSelectDialog : public QDialog
{
    Q_OBJECT

    bool _multi;
    string _schemaName;
    vector<string> def_fieldsList;
    vector<bsonio::SchInternalData> _arrsch;

   void setupWindow(const char* title );
   void defineMultiSelection( const vector<string>& sel );
   void selectWithChildren( TSelectSchemaModel* _model, const QModelIndex& parIndex,
        const QModelIndexList& selIndexes, vector<string>& arr, vector<bsonio::SchInternalData>& SchData );
   bool selectField(  TSelectSchemaModel* _model,
       const QModelIndex& parIndex, queue<string> names );

public slots:
    void objectChanged() {}

protected slots:
    void CmHelp();
    virtual void CmReset();
    virtual void CmDefault();

public:

    /// Multiple select constructor
    /// Construct tree view of fields thrift schema
    /// \param schemaName.
    /// \param def_fieldsList - default selection
    SchemaSelectDialog( QWidget* parent, const char* title,
                  const string& schemaName,
                  const vector<string>& def_fieldsList );

    /// Single select constructor.
    /// Construct tree view of fields thrift schema schemaName.
    SchemaSelectDialog( QWidget* parent, const char* title,
                      const string& schemaName );

    /// Destructor
    virtual ~SchemaSelectDialog();

    /// Returns single selection
    ///    returns empty string if nothing selected
    string selIndex( bsonio::SchInternalData* flddata = 0 );

    /// Returns selection array
    ///    array is empty if nothing is selected
    vector<string> allSelected();

    /// Returns selection array types
    ///    array is empty if nothing is selected
    vector<bsonio::SchInternalData> allSelectedTypes()
    {
      return _arrsch;
    }

private:

    Ui::SchemaSelectDialogData *ui;
    TSelectSchemaModel *_model_schema;

};

} // namespace bsonui

#endif // SchemaSelectDialog_included
