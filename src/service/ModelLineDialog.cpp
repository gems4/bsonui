//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file ModelLineDialog.cpp
/// Implementation of class ModelLineDialog - service to set up field description
/// into model based on bson and our JSON schemas
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QSpinBox>
#include "ModelLineDialog.h"
#include "ui_ModelLineDialog.h"
#include "model_schema.h"
#include "model_node.h"

namespace bsonui {

QVariant TSizeModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
     return QVariant();

    switch( role )
    {  case Qt::DisplayRole:
       case Qt::EditRole:
         return data_[ index.row() ];
       default: break;
    }
    return QVariant();
}

bool TSizeModel::setData( const QModelIndex &index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
       data_[index.row()] = value.toInt();
       return true;
    }
    return false;
}

void TSizeModel::resetData( vector<int>&  sizes )
{
    beginResetModel();
    data_ = sizes;
    endResetModel();
}

QWidget *TSizeDelegate::createEditor(QWidget *parent,
                      const QStyleOptionViewItem&,
                      const QModelIndex&) const
{
  QSpinBox *spin_ = 	new QSpinBox(parent );
    spin_->setMinimum(0);
  return spin_;
}

//==================================================

ModelLineDialog::ModelLineDialog(const QModelIndex& index,
                                 const QModelIndex& parentIndex,
                                 const QString& fldName,
                                 QWidget *parentWidget) :
    QDialog(parentWidget), useSchemas(false), getArrayInf( !fldName.isEmpty() ),
    strDef(0), fldDef(0),
    ui(new Ui::ModelLineDialog)
{
    ui->setupUi(this);
    sizeModel = new TSizeModel( sizes, this);
    ui->sizeView->setModel(sizeModel);
    ui->sizeView->horizontalHeader()->hide();
    TSizeDelegate *sizedlg = new TSizeDelegate(this);
    ui->sizeView->setItemDelegate(sizedlg);

    const TSchemaNodeModel* schemamodel1;
    model = schemamodel1 = dynamic_cast<const TSchemaNodeModel*>(index.model());
    if( model )
    {
       setSchemaNodeModel( schemamodel1,fldName,  index, parentIndex );
       return;
    }
    const TBsonSchemaModel* schemamodel2;
    model = schemamodel2 = dynamic_cast<const TBsonSchemaModel*>(index.model());
    if( model )
    {
       setBsonSchemaModel( schemamodel2,fldName,  index, parentIndex );
    } else
      {
        const TBsonModel* bsonmodel;
        model = bsonmodel = dynamic_cast<const TBsonModel*>(index.model());
        if( bsonmodel)
        {
            setBsonModel( bsonmodel,fldName, index );
        }
     }
}

ModelLineDialog::~ModelLineDialog()
{
    delete ui;
}

void  ModelLineDialog::setSchemaNodeModel( const TSchemaNodeModel* schemamodel,
 const QString& fldName, const QModelIndex& index, const QModelIndex& parentIndex )
{
  model = schemamodel;
  useSchemas = true;
  SchemaNodeLine *line_ =  schemamodel->lineFromIndex(parentIndex);
  SchemaNode *node = schemamodel->lineFromIndex(index)->dataNode;

  strDef = node->getThriftStructDef(); //_strDef;
  ui->typeBox->setEnabled(false);

  if( getArrayInf )
  {
     ui->nameBox->addItem( fldName );
     ui->nameBox->setCurrentText( fldName );
     ui->nameBox->setEnabled(false);

     // get old table sizes
     sizes = node->getArraySizes();
  }
  else
    {
      // set up names box
      for(uint ii=0; ii<line_->noUsedFields.size(); ii++ )
        ui->nameBox->addItem( line_->noUsedKeys[ii].c_str() );

      QObject::connect( ui->nameBox, SIGNAL( currentIndexChanged(const QString & ) ),
                       this, SLOT( setDatabyName(const QString & ) ) );

      if( ui->nameBox->count()<1 )
       return;
      ui->nameBox->setCurrentIndex(0);
    }
   setDatabyName(ui->nameBox->currentText() );
}

void ModelLineDialog::setBsonSchemaModel( const TBsonSchemaModel* schemamodel,
        const QString& fldName, const QModelIndex& index, const QModelIndex& parentIndex )
{
    model = schemamodel;
    useSchemas = true;
    BsonSchemaLine *line_ =  schemamodel->lineFromIndex(parentIndex);
    strDef = schemamodel->lineFromIndex(index)->strDef;
    ui->typeBox->setEnabled(false);

    if( getArrayInf )
    {
       ui->nameBox->addItem( fldName );
       ui->nameBox->setCurrentText( fldName );
       ui->nameBox->setEnabled(false);

       // get old table sizes
       BsonSchemaLine *itline = schemamodel->lineFromIndex(index);
       fldDef = itline->fldDef;
       auto it = fldDef->fTypeId.begin();
       while( (*it == T_SET || *it == T_LIST || *it == T_MAP ) &&
                 it< fldDef->fTypeId.end() )
       {
           if( itline )
           {  uint sizef = itline->children.size();
              sizes.push_back(sizef);
              if(sizef>0)
               itline = itline->children[0].get();
           }
           else
             sizes.push_back(0);
           it++;
       }
    } else
      {
        // set up names box
        for(uint ii=0; ii<line_->noUsedFields.size(); ii++ )
          ui->nameBox->addItem( line_->noUsedFields[ii].c_str() );

        QObject::connect( ui->nameBox, SIGNAL( currentIndexChanged(const QString & ) ),
                         this, SLOT( setDatabyName(const QString & ) ) );

        if( ui->nameBox->count()<1 )
         return;
        ui->nameBox->setCurrentIndex(0);
      }
  setDatabyName(ui->nameBox->currentText() );
}

void ModelLineDialog::setBsonModel( const TBsonModel* bsonmodel,
             const QString& fldName, const QModelIndex& index )
{
    model = bsonmodel;
    ui->docLine->hide();//setText("Free bson object");
    if( getArrayInf )
    {
       ui->nameBox->addItem( fldName );
       ui->nameBox->setCurrentText( fldName );
       ui->nameBox->setEnabled(false);

       // get old table sizes
       BsonLine *itline = bsonmodel->lineFromIndex(index);
       sizes.push_back(itline->children.size());
       sizeModel->resetData(sizes);
       ui->labelDef->setText("Element def");
       ui->labelType->setText("Element type");
       setDatabyType(0);
     } else
      {
         ui->labelSize->hide();
         ui->sizeView->hide();
         ui->nameBox->setEditable(true);
         ui->defEdit->setText("string");
      }
    QObject::connect( ui->typeBox, SIGNAL( currentIndexChanged(int ) ),
                    this, SLOT( setDatabyType(int) ) );
}

void ModelLineDialog::setDatabyName(const QString& text)
{
    string new_object = text.toUtf8().data();
    fldDef = ((ThriftStructDef*)strDef)->getFieldDef(new_object);
    if( !fldDef )
     return;

    ui->docLine->setText(fldDef->fDoc.c_str());
    ui->defEdit->setText(fldDef->insertedDefault.c_str());
    int abson_type = getBsonType( fldDef->fTypeId[0] );
    if( fldDef->fName == JDBIDKEYNAME )
       abson_type =  BSON_OID;
    setEditorByType( getBsonType(fldDef->fTypeId.back())  );
    ui->typeBox->setCurrentText( getBsonTypeName( abson_type ).c_str());

    if( !getArrayInf)
      sizes.clear();
    auto it = fldDef->fTypeId.begin();
    if(*it == T_SET || *it == T_LIST || *it == T_MAP )
      {
        // define size table
        if( !getArrayInf )
        { //auto it = fldDef->fTypeId.begin();
          while( (*it == T_SET || *it == T_LIST || *it == T_MAP ) &&
                  it< fldDef->fTypeId.end() )
          {
             sizes.push_back(0);
             it++;
          }
        }
        sizeModel->resetData(sizes);
        ui->labelSize->show();
        ui->sizeView->show();
        ui->labelDef->setText("Element def");
      }
    else
     {
        ui->labelSize->hide();
        ui->sizeView->hide();
        ui->labelDef->setText("Default");
     }
}

void ModelLineDialog::setDatabyType(int /*ndx*/)
{
    int new_type = getType();
    setEditorByType( new_type  );
    ui->defEdit->setText( getDefValueBson( new_type, "" ).c_str() );
}

void ModelLineDialog::setEditorByType( int _type )
{
    ui->defEdit->setEnabled(true);
    switch( _type  )
    {
     case BSON_INT:
     case BSON_LONG:
         ui->defEdit->setValidator(new QIntValidator(ui->defEdit));
          break;
     case BSON_DOUBLE:
         ui->defEdit->setValidator(new QDoubleValidator(ui->defEdit));
        break;
     case BSON_BOOL:
     case BSON_NULL:
     case BSON_STRING:
         ui->defEdit->setValidator( 0 );
          break;
    // main constructions
     default:
     case BSON_OBJECT:
     case BSON_ARRAY:
        ui->defEdit->setEnabled(false);
     //   ui->defEdit->setText("");
    }
}

int ModelLineDialog::getType() const
{
  int new_type = BSON_NULL;
  switch( ui->typeBox->currentIndex()  )
  {
   case 0:  new_type = BSON_STRING;
         break;
   case 1:  new_type = BSON_DOUBLE;
         break;
   case 2:  new_type = BSON_INT;
         break;
   case 3:  new_type = BSON_LONG;
         break;
   case 4:  new_type = BSON_BOOL;
         break;
   case 5:  new_type = BSON_OBJECT;
         break;
   case 6:  new_type = BSON_ARRAY;
         break;
 }
  return new_type;
}

QString ModelLineDialog::getName() const
{
  return ui->nameBox->currentText(); //.toUtf8().data();
}

QString ModelLineDialog::getDefValue() const
{
   return ui->defEdit->text(); //.toUtf8().data();
}

} // namespace bsonui
