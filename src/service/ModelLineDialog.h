//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file ModelLineDialog.h
/// Declaration of ModelLineDialog - service to set up field description
/// into model based on bson and our JSON schemas
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef MODELLINEDIALOG_H
#define MODELLINEDIALOG_H

#include <QDialog>
#include <QAbstractItemModel>
#include <QItemDelegate>
#ifdef addBSONIO
#include "thrift_schema.h"
#else
#include "bsonio/thrift_schema.h"
#endif
using namespace bsonio;

namespace Ui {
class ModelLineDialog;
}

namespace bsonui {

class TBsonModel;
class TBsonSchemaModel;
class TSchemaNodeModel;

///  \class TSizeModel represents the data sizes and is responsible for fetchin
///  the data is neaded for viewing and for writing back any changes.
class TSizeModel: public QAbstractTableModel
{
    Q_OBJECT

  vector<int>&  data_;

 public:

  TSizeModel( vector<int>&  sizes,   QObject* parent = 0 ):
      QAbstractTableModel(parent), data_(sizes)
  { }
  ~TSizeModel()
  { }

  int rowCount ( const QModelIndex&  ) const
  { return data_.size();  }
  int columnCount ( const QModelIndex&   ) const
  { return 1;  }
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant& value, int role );
  Qt::ItemFlags flags(const QModelIndex & /*index*/) const
  {
      return   Qt::ItemIsEditable | Qt::ItemIsEnabled ;
  }
  void resetData( vector<int>&  sizes );

};

/// \class TSizeDelegate individual items in views are rendered and edited using delegates
class TSizeDelegate: public QItemDelegate
{
    Q_OBJECT

public:

     TSizeDelegate( QObject * parent = 0 ):
         QItemDelegate( parent )
     { }
     QWidget *createEditor(QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};

/// \class ModelLineDialog provides a dialog that allow users to difine json object
class ModelLineDialog : public QDialog
{
    Q_OBJECT

   const QAbstractItemModel* model;   ///< Current model
   bool useSchemas;
   bool getArrayInf;

   const ThriftStructDef* strDef;      ///< Struct definition
   ThriftFieldDef*  fldDef;      ///< Field definition
   vector<int>  sizes;

   void setEditorByType( int _type );

public slots:
   void setDatabyName(const QString & text);
   void setDatabyType(int ndx);

public:

   explicit ModelLineDialog(
        const QModelIndex& index, const QModelIndex& parentIndex,
        const QString& fldName,  QWidget *parentWidget = 0);
   ~ModelLineDialog();

   /// Get object name
   QString getName() const;
   /// Get object type
   int getType() const;
   /// Get default value of object
   QString getDefValue() const;
   /// Get object size (if object is array type)
   const vector<int>& getArraySizes() const
   { return sizes;   }

private:

    Ui::ModelLineDialog *ui;
    TSizeModel* sizeModel;

    void setBsonModel( const TBsonModel* bsonmodel,
        const QString& fldName, const QModelIndex& index );
    void setBsonSchemaModel( const TBsonSchemaModel* schemamodel,
     const QString& fldName, const QModelIndex& index, const QModelIndex& parentIndex );
    void setSchemaNodeModel( const TSchemaNodeModel* schemamodel,
     const QString& fldName, const QModelIndex& index, const QModelIndex& parentIndex );
};

} // namespace bsonui

#endif // MODELLINEDIALOG_H
