//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file TBSONProtocol.h
/// Declarations of bson protocol for Thrift
/// Implements a protocol which uses bson as the wire-format
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef _TBSONPROTOCOL_H_
#define _TBBSOPROTOCOL_H_ 1

#include <thrift/protocol/TVirtualProtocol.h>
#include <string>
#include <stack>
#ifdef addBSONIO
#include "v_json.h"
#else
#include "bsonio/v_json.h"
#endif

using namespace std;
using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

namespace bsonui {

const string ismapkey("-");

/**
 * Class to serve output as base BSON context
 */
class TBSONWriteContext {

    string name;
    TType fieldType;
    int16_t fieldId;
    int16_t list_id;
    string mapkey;

public:

  TBSONWriteContext( const char* aname, const TType afieldType,
                const int16_t afieldId, const int16_t id=0 ):
      name(aname), fieldType(afieldType), fieldId(afieldId),
      list_id(id), mapkey("")
  { }

  const char *getName()
  {
    return name.c_str();
  }

  string getKey()
  {
    string key = name;
    switch( fieldType )
    {
     case T_SET:
     case T_LIST:
         { key = std::to_string(list_id++);
           break;
         }
     case T_MAP:
          if( !list_id )
          {  list_id++;
             key = ismapkey;
           } else
            { list_id=0;
              key = mapkey;
            }
           break;
      default: break;
     }
    return key;
  }

  void setMapKey(const string& amapkey )
  {
    mapkey = amapkey;
  }

};

/**
 * Class to serve input as base BSON context
 */
class TBSONReadContext {

    string name;
    bson_iterator itval;
    bool isArray;
    bool isMap;
    int mapndx;

public:

  TBSONReadContext( const char* aname, const bson_iterator aitval,
                bool aislist, bool aismap ):
      name(aname), itval(aitval), isArray(false/*aislist*/), isMap(aismap),
      mapndx(0)
  {
     if( /*isArray ||*/ isMap || aislist )
       bson_iterator_from_buffer(&itval, bson_iterator_value(&itval) );
  }

  const string& getName() const
  {
    return name;
  }

  bson_iterator *getIterator( string& mapkey )
  {
    mapkey = "";
    if( isMap )
    {
        if( !mapndx )
        {
           bson_iterator_next(&itval);
           mapkey =bson_iterator_key(&itval);
           mapndx = 1;
        } else
           mapndx = 0;

    }
    else if( isArray )
         bson_iterator_next(&itval);

   return &itval;

  }

  void setArray( /*bson_iterator aval*/ )
  {
    isArray = true;
    //bson_iterator_from_buffer(&itval, bson_iterator_value(&aval) );
  }

};


/**
 * bson protocol for Thrift.
 *
 * Implements a protocol which uses bson as the wire-format.
 *
 * Thrift types are represented as described below:
 *
 * 1. Every Thrift integer type is represented as a bson_iterator_int, bson_iterator_bool.
 *
 * 2. Thrift doubles are represented as bson_iterator_double
 *
 * 3. Thrift string values are emitted as bson_iterator_string.
 *
 * 4. Thrift binary values are represented as bson_iterator_bin_data.
 *
 * 5. Thrift structs are represented as BSON object
 *
 * 6. Thrift lists and sets are represented as BSON arrays
 *
 * 7. Thrift maps are represented as BSON object, by the count of the Thrift pairs, followed by a
 *    BSON object containing the key-value pairs. Note that BSON keys can only
 *    be strings, which means that the key type of the Thrift map should be
 *    restricted to numeric or string types -- in the case of numerics, they
 *    are serialized as strings.
 *
 * 8. Thrift messages are represented as BSON arrays, with the protocol
 *    version #, the message name, the message type, and the sequence ID as
 *    the first 4 elements.
 *
 */
class TBSONProtocol : public TVirtualProtocol<TBSONProtocol>
{
public:

  TBSONProtocol(::apache::thrift::stdcxx::shared_ptr<TTransport> ptrans );

  ~TBSONProtocol();

private:

  void tranWriteStart()
  {
      if( contexts_.size() == 0 )
      {
        bson_destroy(&bobj_tansport);
        bson_init(&bobj_tansport);
      }
  }


  uint32_t transWriteEnd()
  {
    uint32_t returns = 0;
    if( contexts_.size() == 0 )
    {
      bson_finish(&bobj_tansport);
      returns = print_bson_object_to_bin( &bobj_tansport );
    }
    return returns;
  }

  uint32_t transReadStart()
  {
      uint32_t returns = 0;
      const char *data;
      if( bitr.size() > 0 )
      {
          std::string mapkey;
          bson_iterator *it = readcntxt_.top().getIterator( mapkey );
          //cout << bson_iterator_key(it)<< endl;
          data = bson_iterator_value(it);
      }
      else
      {
          returns = read_bson_object_from_bin( getBson() );
          data = getBson()->data;
      }

      bson_iterator itr;
      bson_iterator_from_buffer(&itr, data);
      bitr.push(itr);

      return returns;
  }


  void transReadEnd()
  {
    bitr.pop();
  }

  int readInt();

  // Print bson object to binary file
  uint32_t print_bson_object_to_bin( const bson *b);

  // Load bson object from binary file
  uint32_t read_bson_object_from_bin( bson *b );

public:

  //  Writing functions.


  uint32_t writeMessageBegin(const std::string& name,
                             const TMessageType messageType,
                             const int32_t seqid);

  uint32_t writeMessageEnd();

  uint32_t writeStructBegin(const char* name);

  uint32_t writeStructEnd();

  uint32_t writeFieldBegin(const char* name, const TType fieldType, const int16_t fieldId);

  uint32_t writeFieldEnd();

  uint32_t writeFieldStop();

  uint32_t writeMapBegin(const TType keyType, const TType valType, const uint32_t size);

  uint32_t writeMapEnd();

  uint32_t writeListBegin(const TType elemType, const uint32_t size);

  uint32_t writeListEnd();

  uint32_t writeSetBegin(const TType elemType, const uint32_t size);

  uint32_t writeSetEnd();

  uint32_t writeBool(const bool value);

  uint32_t writeByte(const int8_t byte);

  uint32_t writeI16(const int16_t i16);

  uint32_t writeI32(const int32_t i32);

  uint32_t writeI64(const int64_t i64);

  uint32_t writeDouble(const double dub);

  uint32_t writeString(const std::string& str);

  uint32_t writeBinary(const std::string& str);

  //  Reading functions

  uint32_t readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid);

  uint32_t readMessageEnd();

  uint32_t readStructBegin(std::string& name);

  uint32_t readStructEnd();

  uint32_t readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId);

  uint32_t readFieldEnd();

  uint32_t readMapBegin(TType& keyType, TType& valType, uint32_t& size);

  uint32_t readMapEnd();

  uint32_t readListBegin(TType& elemType, uint32_t& size);

  uint32_t readListEnd();

  uint32_t readSetBegin(TType& elemType, uint32_t& size);

  uint32_t readSetEnd();

  uint32_t readBool(bool& value);

  // Provide the default readBool() implementation for std::vector<bool>
  using TVirtualProtocol<TBSONProtocol>::readBool;

  uint32_t readByte(int8_t& byte);

  uint32_t readI16(int16_t& i16);

  uint32_t readI32(int32_t& i32);

  uint32_t readI64(int64_t& i64);

  uint32_t readDouble(double& dub);

  uint32_t readString(std::string& str);

  uint32_t readBinary(std::string& str);


  bson *getBson()
      {
        return &bobj_tansport;
      }

  private:

  bson bobj_tansport;

  std::stack<TBSONWriteContext> contexts_;
  std::stack<bson_iterator> bitr;
  std::stack<TBSONReadContext> readcntxt_;

  TTransport* trans_;
 };

/**
 * Constructs input and output protocol objects given transports.
 */
class TBSONProtocolFactory : public TProtocolFactory {
public:
  TBSONProtocolFactory() {}

  virtual ~TBSONProtocolFactory() {}

  ::apache::thrift::stdcxx::shared_ptr<TProtocol> getProtocol(::apache::thrift::stdcxx::shared_ptr<TTransport> trans) {
    return ::apache::thrift::stdcxx::shared_ptr<TProtocol>(new TBSONProtocol(trans));
  }
};

} // namespace bsonui

#endif // #define _TBSONPROTOCOL_H_ 1
