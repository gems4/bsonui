//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file dbkeysmodel.h
/// Declarations of classes TKeyListTableNew, TKeyTable and DBKeysModel
/// implements data container, widget and model to work with data base record&keys.
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef DBKEYSMODEL_H
#define DBKEYSMODEL_H

#include <iostream>
#include "model_table.h"
#ifdef addBSONIO
#include "dbedgedoc.h"
#else
#include "bsonio/dbedgedoc.h"
#endif

class QSortFilterProxyModel;

namespace bsonui {

class QueryWidget;
class TableEditWidget;
class BSONUIBase;

using  Openfunction = std::function<void( const QModelIndex& index)>;

/// Class for key table data container
class TKeyListTable : public TAbstractDataContainer
{
    bsonio::TDBSchemaDocument *dbclient;

    QVector< QString > colHeads;
    //QVector< QString > colDoc;
    QVector< QVector<QVariant> > matrix;

 public:

   TKeyListTable( const char * aname, bsonio::TDBSchemaDocument *adbclient ):
      TAbstractDataContainer(aname), dbclient(adbclient)
   { resetData(); }
    virtual ~TKeyListTable() {}

   int rowCount() const
   { return matrix.size();  }

   int columnCount() const
   { return colHeads.size(); }

   QVariant data( int line, int column ) const
   {  return matrix.at( line ).at( column ); }

   bool setData( int /*line*/, int /*column*/, const QVariant & /*value*/ )
   {    //matrix[line].replace(column, value);
        return true;
   }

   virtual QString headerData ( int section ) const
   { return colHeads[section]; }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return false; }

   virtual int getType( int /*line*/, int /*column*/ ) const
   { return ftString; }

   virtual QString getToolTip( int /*line*/, int column ) const
   { return colHeads[column]; }

   void resetData();
};

/// Class for database keys/fields table data container
class TKeyListTableNew : public TAbstractDataContainer
{
    bsonio::TDBSchemaDocument *dbclient;
    const vector<string>& fields_;
    const bsonio::TableOfValues& tabl_;
    vector<string> keys;

    void resetKeys()
    {
      keys.clear();
      auto it = tabl_.begin();
      while( it!= tabl_.end() )
          keys.push_back(it++->first);
    }

 public:

   TKeyListTableNew( const char * aname, bsonio::TDBSchemaDocument *adbclient ):
      TAbstractDataContainer(aname), dbclient(adbclient),
      fields_( adbclient->GetQueryFields() ),
      tabl_ ( dbclient->GetQueryResult() )
   { resetData(); }

   virtual ~TKeyListTableNew() {}

   int rowCount() const
   {
     // cout << "rowCount " << keys.size()<< endl;
       return keys.size();  }

   int columnCount() const
   {
      // cout << "columnCount " << fields_.size()<< endl;
       return fields_.size()+1; }

   QVariant data( int line, int column ) const
   {
       if(column == 0)
          return keys[line].c_str();
         else
          { //auto data = tabl_.at(keys[line]);
            //return data[column-1].c_str();
           return tabl_.at(keys[line])[column-1].c_str();
          }
   }

   bool setData( int /*line*/, int /*column*/, const QVariant & /*value*/ )
   {    return true;  }

   virtual QString headerData ( int section ) const
   {
       if(section == 0)
          return "key";
         else
           return fields_[section-1].c_str();
     //    return QString("%1").arg(section);
   }

   virtual bool  IsEditable( int /*line*/, int /*column*/ ) const
   { return false; }

   virtual int getType( int /*line*/, int /*column*/ ) const
   { return ftString; }

   virtual QString getToolTip( int /*line*/, int column ) const
   {
       if(column == 0)
          return "key";
         else
           return fields_[column-1].c_str();
   }

   void resetData()
   {
       resetKeys();
   }
};

///  \class TKeyTable implements a table view that displays
/// items from database keys/fields table.
class TKeyTable: public TMatrixTable
{
        Q_OBJECT

    // QMainWindow *parentTask;
    Openfunction _openf;

  protected:
    void keyPressEvent(QKeyEvent* e);

  public slots:
    void CmNext();
    void CmPrevious();

  public:
       TKeyTable( QWidget * parent, Openfunction fn ):
           TMatrixTable(parent, TMatrixTable::tbNoMenu ), _openf(fn)
       {}
};

//---------------------------------------------------

/// \class DBKeysModel object to work with database keys/fields table
class DBKeysModel : public QObject
{
    Q_OBJECT

    // parent data

    BSONUIBase *parent;
    std::shared_ptr<bsonio::TDBVertexDocument> dbgraph;
    TKeyTable *table;

    // internal data

    /// Table internal data
    TKeyListTableNew *dataTable;

    /// Table model
    TMatrixModel* tableModel;
    QSortFilterProxyModel *proxyModel;

    /// Keys query window
    QueryWidget *queryWindow;

    /// Keys list demo window
    TableEditWidget *resultWindow;

    /// Reset keys table model
    void resetKeysTable();
    /// Set up keys table
    void defKeysTable();

public:

    DBKeysModel( const std::shared_ptr<bsonio::TDBVertexDocument>& adbgraph,
                 TKeyTable *atable, BSONUIBase *aparent );
    ~DBKeysModel();

    /// Reset current DB client
    void resetDBClient(  const std::shared_ptr<bsonio::TDBVertexDocument>& adbgraph );

    /// Update keys model data
    void changeKeyList( const string& key );

    /// Update keys model data
    void changeKeyList( bson *curRecord )
    {
        changeKeyList( dbgraph->getKeyFromBson( curRecord->data ) );
    }

    /// Show query window
    void showQuery( const char* title, const vector<string>& schemalst );

    /// Update keys model for changed query
    void setOneQuery( QueryWidget* queryW );

    void changeQueryToId( const string& id );

    void resetInOutQuery( const string& id );

    /// Show table window with database keys/fields table
    void showResult( const char* title );

    /// Close addition windows
    void Close();
};

} // namespace bsonui

#endif // DBKEYSMODEL_H
