//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_query.cpp
/// Implementation of TQueryModel, TQueryDelegate and TQueryView
/// implements a tree view of query structure
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QKeyEvent>
#include <QComboBox>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QInputDialog>
#include <QMessageBox>
#include "QueryWidget.h"
#include "preferences.h"
using namespace bsonio;
using namespace ::apache::thrift::protocol;

namespace bsonui {

//--------------------------------------------------------------------------------------
//  class TQueryModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//---------------------------------------------------------------------------------------
TQueryModel::TQueryModel( QObject* parent ):
      QAbstractItemModel(parent)
{
  // define edit tree view
  hdData << "key" << "value"  ;
  rootNode = new QueryLine(0, "$root", 0 );
  connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
  			parent,  SLOT(objectChanged()) );	 
}

TQueryModel::~TQueryModel()
{
  if(rootNode )
     delete rootNode;
}

QueryLine *TQueryModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<QueryLine *>(index.internalPointer());
    } else {
        return rootNode;
    }
}


QModelIndex TQueryModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    QueryLine *parentItem = lineFromIndex( parent );
    if(parentItem->children.size() > 0 )
       return createIndex(row, column, parentItem->children[row].get());
    else
       return QModelIndex();
}

QModelIndex TQueryModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    QueryLine *childItem = lineFromIndex(child);
    QueryLine *parentItem = childItem->parent;
    if (parentItem == rootNode )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}


int TQueryModel::rowCount( const QModelIndex& parent ) const
{
   if (!rootNode)
       return 0;
  if (parent.column() > 0)
      return 0;
  QueryLine *parentItem = lineFromIndex( parent );
  return parentItem->children.size();
}	

int TQueryModel::columnCount( const QModelIndex& /*parent*/ ) const
{
  return 2;
}	

Qt::ItemFlags TQueryModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  QueryLine *item = lineFromIndex( index );
  if( index.column() == 1 &&
    (item->type == QueryLine::tField || item->type == QueryLine::tObject
     || item->type == QueryLine::tString ) )
   {  flags |= Qt::ItemIsEditable;
             return flags;
   }
  else
     return (flags & ~Qt::ItemIsEditable);
}

QVariant TQueryModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
 if( role == Qt::DisplayRole  && orientation == Qt::Horizontal )
	   return hdData[section];
  return QVariant();
}

QVariant TQueryModel::data( const QModelIndex& index, int role ) const
{
   if(!index.isValid())
	 return QVariant();	

   switch( role )
   { case Qt::DisplayRole:
     case Qt::EditRole:
            {  QueryLine *item = lineFromIndex( index );
               if( index.column()== 0 )
                   return item->keyname.c_str();
               else
                   if( index.column()== 1 )
                       return item->value.c_str();
           }
      case Qt::ForegroundRole:
       if ( index.column() == 0 )
               {
                 return QVariant( QColor( Qt::darkCyan ) );
               }
      default: break;
   }
   return QVariant();
}

bool TQueryModel::setData( const QModelIndex& index, const QVariant& value_, int role)
{
	if( index.isValid() && ( role == Qt::EditRole ) )
	{
      if(  index.column()== 1)
		  {	 
             QueryLine *line =lineFromIndex(index);
             line->value = value_.toString().toUtf8().data();
		  }	 
	 return true;
	} 
	return false;
}

bool TQueryModel::isAll( const QModelIndex& index ) const
{
    QueryLine *line =  lineFromIndex(index);
    return (  line->type == QueryLine::tConditionAdd  );
}


bool TQueryModel::isAdd( const QModelIndex& index ) const
{
    QueryLine *line =  lineFromIndex(index);
    return ( ( line->type == QueryLine::tConditionOne ||
               line->type == QueryLine::tField ) && line->children.size()<1  );
}

bool TQueryModel::isResize( const QModelIndex& index ) const
{
    QueryLine *line =  lineFromIndex(index);
    return ( line->type == QueryLine::tList );
}

void TQueryModel::resizeArray( QWidget* parentWidget, const QModelIndex& index )
{
    QueryLine *line =  lineFromIndex(index);

    // get new size
    bool ok = 0;
    uint size = QInputDialog::getInt( parentWidget, "Please, select new array size",
             "Array size ", line->children.size(), 0, 999, 1, &ok );
    if(!ok) // cancel command
              return;

    // resizeline item
    if( size == line->children.size() )
       return;

    // delete if smaler
    if( size < line->children.size() )
    {
       beginRemoveRows( index, size, line->children.size()-size);
       line->children.resize(size);
       endRemoveRows();
       return;
    }

    // add new elements
    beginInsertRows( index, line->children.size(), size-1);
     QueryLine *linechild;
     for( uint ii=line->children.size(); ii<size; ii++)
     {
        string key_ =  to_string(ii);
        linechild = new QueryLine( ii, "", line);
        linechild->keyname = key_;
     }
     endInsertRows();
}

void TQueryModel::delObject( QWidget* /*parentWidget*/, const QModelIndex& index )
{
    QueryLine *line =  lineFromIndex(index);
    //QString msg = QString("Confirm deletion of %1 data object?").arg(line->keyname );
    //QMessageBox::StandardButton reply;
    //reply = QMessageBox::question( parentWidget->window(), "Data object to delete", msg,
    //         QMessageBox::Yes|QMessageBox::No);
    //if (reply == QMessageBox::No)
    //   return;
    const QModelIndex parentNdx = parent(index);

    QueryLine *parent_ = line->parent;
    beginRemoveRows( parentNdx, index.row(), index.row());
    // resizeline item
    parent_->children.erase( parent_->children.begin() + index.row() );
    for(uint ii=index.row(); ii<parent_->children.size(); ii++ )
    {
        if( parent_->type == QueryLine::tList )
          parent_->children[ii].get()->keyname = to_string(ii);
        parent_->children[ii].get()->ndx--;
    }
    endRemoveRows();
}

const QModelIndex TQueryModel::addField( const string& _fldsName,
      const SchInternalData& _fldData, const QModelIndex& parentIndex )
{
    QueryLine *line =  lineFromIndex(parentIndex);
    // add to end
    beginInsertRows( parentIndex, line->children.size(), line->children.size());
    QueryLine* newline = new QueryLine(line->children.size(), _fldsName.c_str(), line );
    newline->schdata = _fldData;
    endInsertRows();
    return index( line->children.size()-1, 0 , parentIndex);
}

const QModelIndex TQueryModel::addObject( const string& akey, const QModelIndex& parentIndex )
{
    QueryLine *line =  lineFromIndex(parentIndex);
    // add to end
    beginInsertRows( parentIndex, line->children.size(), line->children.size());
       new QueryLine(line->children.size(), akey, line );
    endInsertRows();
    return index( line->children.size()-1, 0 , parentIndex);
}


string TQueryModel::getQuery( int queryType )
{
  string recJson;

  if(rootNode->children.size()<1)
    return "";  // empty table

  if( queryType == bsonio::DBQueryDef::qEJDB )
  {
     bson bsrec;
     rootNode->query2bson( ioSettings().Schema(),  &bsrec);
     jsonFromBson( bsrec.data, recJson );
     bson_destroy(&bsrec);
  }
  return recJson;
}

//-------------------------------------------------------------------------------------
// TQueryDelegate -  individual items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TQueryDelegate::TQueryDelegate(  QObject * parent ):
    QItemDelegate( parent )
{ }

// Editing QTreeView for objects in System page
QWidget *TQueryDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    QueryLine *line =  ((TQueryModel *)(index.model() ))->lineFromIndex(index);
    if( index.column() > 0 )
    {
       if( line->keyname == "$exists")
       {  QComboBox *accessComboBox = new QComboBox(parent);
          accessComboBox->addItem(tr("false"));
          accessComboBox->addItem(tr("true"));
          return accessComboBox;
       }
       else
           switch( line->schdata.thrifttype )
           {
                 case T_BOOL:
                       {
                         QComboBox *accessComboBox = new QComboBox(parent);
                         accessComboBox->addItem(tr("false"), tr("false"));
                         accessComboBox->addItem(tr("true"), tr("true"));
                         return accessComboBox;
                        }
                 case T_I08:
                 case T_I16:
                 case T_I32:
                       {
                          string enumName = line->schdata.enumname;
                          if( !enumName.empty() )
                          {
                            ThriftEnumDef* enumdef = ioSettings().Schema()->getEnum( enumName );
                            if(enumdef != nullptr )
                            {
                                vector<string> lst = enumdef->getNamesList();
                                QComboBox *accessComboBox = new QComboBox(parent);
                                for( uint ii=0; ii<lst.size(); ii++ )
                                {
                                    string itname =lst[ii];
                                    accessComboBox->addItem(tr(itname.c_str()), enumdef->getId(itname) );
                                    accessComboBox->setItemData(ii, enumdef->getDoc(itname).c_str(), Qt::ToolTipRole);
                                }
                                return accessComboBox;
                            }
                         }
                       }
                 case T_I64:
                 case T_U64:
                     { QLineEdit *lineEdit = new QLineEdit(parent);
                       QIntValidator *ivalid = new QIntValidator(lineEdit);
                       if( line->schdata.thrifttype == T_U64 )
                          ivalid->setBottom(0);
                       lineEdit->setValidator(ivalid);
                       return lineEdit;
                     }
                case T_DOUBLE:
                      { QLineEdit *lineEdit = new QLineEdit(parent);
                        QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
                        lineEdit->setValidator(dvalid);
                        return lineEdit;
                      }
                 case T_UTF8:
                 case T_STRING:
                 default:
                     { QLineEdit *lineEdit = new QLineEdit(parent);
                       return lineEdit;
                     }
         }
   }
   return QAbstractItemDelegate::createEditor( parent, option,  index );
}


//-------------------------------------------------------------------------------------
// class TQueryView implements a tree view that displays items from a model to query.
//-------------------------------------------------------------------------------------

TQueryView::TQueryView( QWidget * parent ):
     QTreeView( parent )
{
     setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
     header()->setSectionsClickable(true);
     header()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );

     setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
     setFocusPolicy(Qt::StrongFocus);
     setTabKeyNavigation( false );
     setContextMenuPolicy(Qt::CustomContextMenu);
     setSelectionMode( QAbstractItemView::SingleSelection );
     setSelectionBehavior( QAbstractItemView::SelectRows );
     setIndentation( 30 );

     connect( this, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(slotPopupContextMenu(QPoint)));
     connect( header(), SIGNAL(sectionClicked(int)),
             this, SLOT(changeCurrent(int)));
}

void TQueryView::changeCurrent( int section )
{
    if( !currentIndex().isValid() )
    { QModelIndex index = model()->index( 0, section, rootIndex());
      setCurrentIndex(index);
    }
}

void TQueryView::slotPopupContextMenu(const QPoint &pos)
{
    QModelIndex index = indexAt( pos );
    QMenu *menu = contextMenu( index);
    if( menu)
    { menu->exec( viewport()->mapToGlobal(pos) );
      delete menu;
    }
}

void TQueryView::CmContextMenu( )
{
    QModelIndex index = currentIndex();
    QMenu *menu = contextMenu( index);
    if( menu)
    { menu->exec( QCursor::pos() );
      delete menu;
    }
}

QMenu* TQueryView::contextMenu(const QModelIndex &index)
{
    const TQueryModel* model_ =dynamic_cast<const TQueryModel*>(model());
    //const TQueryModel* model =dynamic_cast<const TQueryModel*>(index.model());
    if( !model_ )
        return 0;
    QMenu *menu = new QMenu(this);
    QAction* act;

    // add new condition
    //QAction* act =  new QAction(tr("Add Structured Data Fieldpath ..."), this);
    //act->setShortcut(tr("F7"));
    //act->setStatusTip(tr("Add new Fieldpath(s) into object"));
    //connect(act, SIGNAL(triggered()), this, SLOT(CmAddObjects()));
    //menu->addAction(act);

    act =  new QAction(tr("Add \"&and\""), this);
    act->setShortcut(tr("Ctr+A"));
    act->setStatusTip(tr("Add \"and\" joined condition"));
    connect(act, SIGNAL(triggered()), this, SLOT(CmAddAnd()));
    menu->addAction(act);

    act =  new QAction(tr("Add \"&or\""), this);
    act->setShortcut(tr("Ctr+O"));
    act->setStatusTip(tr("Add \"or\" joined condition"));
    connect(act, SIGNAL(triggered()), this, SLOT(CmAddOr()));
    menu->addAction(act);

    act =  new QAction(tr("Add &Fieldpath"), this);
    act->setShortcut(tr("Ctr+F"));
    act->setStatusTip(tr("Add fieldpath"));
    connect(act, SIGNAL(triggered()), this, SLOT(CmAddField()));
    menu->addAction(act);

    menu->addSeparator();

    if(  model_->isAdd(index) )
    {
        act =  new QAction(tr("Add \"&exists\""), this);
        act->setShortcut(tr("Ctr+E"));
        act->setStatusTip(tr("Fieldpath existence checking:"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddExists()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"&not\""), this);
        act->setShortcut(tr("Ctr+N"));
        act->setStatusTip(tr("Add \"not\" condition"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddNot()));
        menu->addAction(act);

        menu->addSeparator();
        act =  new QAction(tr("Add \"&in\""), this);
        act->setShortcut(tr("Ctr+I"));
        act->setStatusTip(tr("Field equals to any member in the provided set"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddIn()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"nin\""), this);
        act->setShortcut(tr("Ctr+J"));
        act->setStatusTip(tr("The field value is not equal to any of provided alternatives"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddNin()));
        menu->addAction(act);

        menu->addSeparator();
        act =  new QAction(tr("Add \"&begin\""), this);
        act->setShortcut(tr("Ctr+B"));
        act->setStatusTip(tr("Fieldpath starts with the specified prefix"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddBegin()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"gt\""), this);
        act->setShortcut(tr("Ctr+>"));
        act->setStatusTip(tr("Value greater than >"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddGt()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"&gte\""), this);
        act->setShortcut(tr("Ctr+G"));
        act->setStatusTip(tr("Value greater than or equal to >="));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddGte()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"lt\""), this);
        act->setShortcut(tr("Ctr+<"));
        act->setStatusTip(tr("Value lesser than <"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddLt()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"&lte\""), this);
        act->setShortcut(tr("Ctr+L"));
        act->setStatusTip(tr(" Value lesser than or equal to <="));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddLte()));
        menu->addAction(act);

        act =  new QAction(tr("Add \"eq\""), this);
        act->setShortcut(tr("Ctr+="));
        act->setStatusTip(tr("Value equal to"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddEq()));
        menu->addAction(act);

        menu->addSeparator();
        act =  new QAction(tr("Add \"i&case\""), this);
        act->setShortcut(tr("Ctr+C"));
        act->setStatusTip(tr("Add case insensitive string matching"));
        connect(act, SIGNAL(triggered()), this, SLOT(CmAddICase()));
        menu->addAction(act);
        menu->addSeparator();
    }

    act =  new QAction(tr("Remove Selected Row"), this);
    act->setShortcut(tr("F8"));
    act->setStatusTip(tr("Delete current line from object"));
    connect(act, SIGNAL(triggered()), this, SLOT(CmDelObject()));
    menu->addAction(act);

    act =  new QAction(tr("Clear Selected Row"), this);
    act->setStatusTip(tr("Clear current line "));
    connect(act, SIGNAL(triggered()), this, SLOT(CmClearObject()));
    menu->addAction(act);

    if(  model_->isResize(index) )
    {
       menu->addSeparator();
       act =  new QAction(tr("Resize Selected List ..."), this);
       act->setShortcut(tr("F4"));
       act->setStatusTip(tr("Change current array size"));
       connect(act, SIGNAL(triggered()), this, SLOT(CmResizeList()));
       menu->addAction(act);
    }
    return menu;
 }

void TQueryView::keyPressEvent(QKeyEvent* e)
{
  if ( e->modifiers() & Qt::ControlModifier )
  {
   switch ( e->key() )
   {
    case Qt::Key_E:
         CmAddExists();
         return;
    case Qt::Key_F:
        CmAddField();
         return;
    case Qt::Key_A:
        CmAddAnd();
         return;
    case Qt::Key_O:
         CmAddOr();
          return;
    case Qt::Key_N:
         CmAddNot();
          return;
    case Qt::Key_I:
         CmAddIn();
          return;
    case Qt::Key_J:
         CmAddNin();
          return;
    case Qt::Key_B:
         CmAddBegin();
          return;
    case Qt::Key_Greater:
         CmAddGt();
          return;
    case Qt::Key_G:
         CmAddGte();
          return;
    case Qt::Key_Less:
         CmAddLt();
         return;
    case Qt::Key_L:
         CmAddLte();
         return;
    case Qt::Key_Equal:
         CmAddEq();
         return;
    case Qt::Key_C:
         CmAddICase();
         return;
   }
  }

  switch( e->key() )
  {
     //case Qt::Key_F7:
     //      CmAddObjects();
     //     return;
     case Qt::Key_F8:
           CmDelObject();
          return;
     case Qt::Key_F4:
           CmResizeList();
          return;
   }
  QTreeView::keyPressEvent(e);
}

bool TQueryView::testCurrentIndex( )
{
    if( currentIndex().isValid() )
     return  true;
   QMessageBox::information( window(), "View object",
        "Please, determine the current index" );
   return false;
}

void TQueryView::CmAddObjects( const vector<string>& _fieldsList,
       const vector<SchInternalData>& _fieldsTypes )
{
    TQueryModel* model_ =(TQueryModel*)(model());
    try {
        if(  model_  )
          for(uint ii=0; ii<_fieldsList.size(); ii++)
             model_->addField( _fieldsList[ii], _fieldsTypes[ii], rootIndex());
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
}

void TQueryView::addObject( const string& akey  )
{
    QModelIndex index = currentIndex();
    TQueryModel* model_ =  (TQueryModel*)(model());

    try {
         if(  model_ && model_->isAdd(index) )
         {
             QModelIndex ndx = model_->addObject( akey, index);
             selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
          }
         else
           QMessageBox::information( window(), "Add to Object",
               "Please, determine object to be expanded" );
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
}

void TQueryView::CmAddField()
{
   TQueryModel* model_ =(TQueryModel*)(model());
   try {
         QueryWidget* parentdlg = dynamic_cast<QueryWidget*>(window());
         if(  model_ && parentdlg)
          {
             string fldpath;
             SchInternalData flddata;
             parentdlg->selectLine(fldpath, flddata);
             QModelIndex index = currentIndex();
             if( !index.isValid() || !model_->isAll( index ))
               index = rootIndex();
             QModelIndex ndx = model_->addField( fldpath, flddata, index );
             selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
          }
         //else
         // QMessageBox::information( window(), "Delete from Object",
         //    "Please, determine the key-value pair to be deleted" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
     }
}

void TQueryView::addTopObject( const string& akey  )
{
    TQueryModel* model_ =  (TQueryModel*)(model());
    if( !model_)
      return;

    try {
          QModelIndex index = currentIndex();
          //if( !index.isValid())
          //  index = rootIndex();
          QueryLine *line =  model_->lineFromIndex(index);
          if( line->type != QueryLine::tConditionAdd )
            index = rootIndex();
          QModelIndex ndx = model_->addObject( akey, index);
          selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
}

void TQueryView::CmDelObject()
{
   QModelIndex index = currentIndex();
   TQueryModel* model =(TQueryModel*)(index.model());
   try {
          if(  model )
          {
              selectionModel()->clear();
              collapse(index);
              model->delObject( window(), index);
         } else
          QMessageBox::information( window(), "Delete from Object",
             "Please, determine the key-value pair to be deleted" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
     }
}

void TQueryView::CmClearObject()
{
   QModelIndex index = currentIndex();
   TQueryModel* model =(TQueryModel*)(index.model());
   try {
          if(  model )
           model->setData(index.sibling(index.row(), 1), "",  Qt::EditRole);
          else
           QMessageBox::information( window(), "Clear Object",
             "Please, determine the key-value pair to be clear" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
     }
}


void TQueryView::CmResizeList()
{
    QModelIndex index = currentIndex();
    TQueryModel* model =(TQueryModel*)(index.model());
    try {
        if(  model && model->isResize(index) )
       {
            // collapse(index);
            model->resizeArray( window(), index);
       }
       else
          QMessageBox::information( window(), "Resize Array",
             "Please, determine arrays key to be resized" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
}


} // namespace bsonui

//---------------------- End of file  model_query.cpp ---------------------------
