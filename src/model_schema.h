//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_schema.h
/// Declaration of TBsonSchemaModel, TBsonSchemaDelegate
/// implements a tree view of bson with our JSON schemas structure
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef _model_schema_h
#define _model_schema_h

#include "model_bson.h"
#ifdef addBSONIO
#include "thrift_schema.h"
#else
#include "bsonio/thrift_schema.h"
#endif
using namespace bsonio;

namespace bsonui {

/// \class BsonSchemaLine represents an item in a tree view.
/// Item data defines one thift schema field object
class BsonSchemaLine
{
public:

    BsonSchemaLine( ThriftStructDef* astrDef, ThriftFieldDef*  afldDef, int ndx,
                    const QString& akey, int atipe, const QString& avalue, int level );
    BsonSchemaLine( const BsonSchemaLine* data );
    ~BsonSchemaLine();

    ThriftStructDef* strDef;   ///< Struct definition
    ThriftFieldDef*  fldDef;   ///< Field definition
    QVector<QString> fldValue; ///< Values into columns

    // internal data
    int ndx;                       ///< internal ndx
    int type;                      ///< bson type of field
    int level;                     ///< index into fldDef->fTypeId array
    vector<string> noUsedFields;   ///< Field names can be add
    bool isMap;

    BsonSchemaLine *parent;
    vector<unique_ptr<BsonSchemaLine>> children;
};

/// \class TBsonSchemaModel
/// Class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to bson + thrift schema object
class TBsonSchemaModel: public TBsonAbstractModel
{
	Q_OBJECT

    const ThriftSchema* schema;
    ThriftStructDef* strDef;
    QStringList hdData;
    bson* bsonData;
    string schemaName;
    BsonSchemaLine* rootNode;

    QString getDescription( const QModelIndex& index ) const;

    void valueFromBson(int type, bson_iterator* it, QString& value );
    /// Read internal data from bson structure and json schema
    void struct2model( ThriftStructDef* strDef,
              const char *bson_data, BsonSchemaLine* parent );
    /// Read internal data from json schema
    void struct2model( ThriftStructDef* strDef, BsonSchemaLine* parent );
    bool field2model( int level, ThriftStructDef* strDef, ThriftFieldDef*  afldDef,
                      bson_iterator* it, int datatype, BsonSchemaLine* parent );
    void list2model( int level, ThriftStructDef* strDef, ThriftFieldDef*  afldDef,
                       const char *data, BsonSchemaLine* parent );

    /// Write internal data to bson structure
    void list_to_bson_full(BsonSchemaLine* headline, bson* bsonobj );
    void list_to_bson(BsonSchemaLine* line, bson* bsonobj );

 public:
	  
    static bool showComments;	///< Extern flag to show schema comments into editor
    static bool useEnumNames;	///< Extern flag to show enum names into editor
    static bool editID;	        ///< Extern flag to edit system data

  TBsonSchemaModel( const ThriftSchema* aschema, bson* abson, const string& schemaName,
              const QStringList& aHeaderData,   QObject* parent = 0 );
  ~TBsonSchemaModel();
	 
  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;     //ok
  int columnCount ( const QModelIndex& parent  ) const; // ok
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant & value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const; 
  Qt::ItemFlags flags ( const QModelIndex& index ) const;
	 
  // special functions
  BsonSchemaLine* lineFromIndex(const QModelIndex& index) const;
  string helpName( const QModelIndex& index ) const;
  bool isNumber( const QModelIndex& index ) const;
  bool isArray( const QModelIndex& index ) const;
  void resizeArray( QWidget* parent, const QModelIndex& index );
  void resizeArray(uint level, const vector<int>& sizes,
              const QString& defvalue, const QModelIndex& index );
  bool isCanBeRemoved( const QModelIndex& index ) const;
  bool isStruct( const QModelIndex& index ) const;
  void delObject( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObjects( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObject( QWidget* parent, const QModelIndex& index );
  void setupModelData( bson* abson, const string& schema );
// new for Union
  bool isUnion( const QModelIndex&  ) const;
  void delObjectsUnion( QWidget* , const QModelIndex&  );

  string  getFieldPath( const QModelIndex& index );
  string  getFieldData( const QModelIndex&  index );

  /// Get description of enum
  ThriftEnumDef* getEnum(const string& enumName)
  { return schema->getEnum( enumName );   }

};


/// \class TBsonSchemaDelegate individual items in views are rendered and edited using delegates
class TBsonSchemaDelegate: public QItemDelegate
{
	Q_OBJECT

public:
	
     TBsonSchemaDelegate( QObject * parent = 0 );
	 QWidget *createEditor(QWidget *parent,
	                       const QStyleOptionViewItem &option,
	                       const QModelIndex &index) const;
     void setEditorData(QWidget *editor, const QModelIndex &index) const;
     void setModelData(QWidget *editor, QAbstractItemModel *model,
                          const QModelIndex &index) const;
};

} // namespace bsonui

#endif   // _model_schema_h
