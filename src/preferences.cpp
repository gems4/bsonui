//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file preferences.cpp
/// Implementation of UIPreferences object for monitoring changes
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include "model_node.h"
#include "PreferencesBSONUI.h"
#ifdef addBSONIO
#include "dbconnect.h"
#else
#include "bsonio/dbconnect.h"
#endif


namespace bsonui {

UIPreferences& uiSettings()
{
  static  UIPreferences data;
  return data;
}

UIPreferences::UIPreferences():
    _iosettings(ioSettings()), _database( new bsonio::TDataBase() )
{
    setDefValues();
}

void UIPreferences::setDefValues()
{
  // load main programm settingth
  TSchemaNodeModel::showComments = _iosettings.getBool("ShowComments", TSchemaNodeModel::showComments);
  TSchemaNodeModel::useEnumNames = _iosettings.getBool("UseEnumNames", TSchemaNodeModel::useEnumNames);
  TSchemaNodeModel::editID = _iosettings.getBool("Edit_id", TSchemaNodeModel::editID);
  TBsonView::expandedFields = _iosettings.getBool("KeepExpanded", TBsonView::expandedFields);
}

//------------------------------------------------------------

void UIPreferences::CmSettingth()
{
  try
  {
     // define new preferences
     PreferencesBSONUI dlg(_iosettings);
     // signal to reset database
     QObject::connect( &dlg, SIGNAL(dbdriveChanged()), this, SIGNAL( dbChanged()));

     if( !dlg.exec() )
          return;

     if( _iosettings.updateSchemaDir() )
         emit schemaChanged();

     emit viewMenuChanged();
     emit modelChanged();
     emit tableChanged();
  }
  catch(std::exception& e)
  {
    cout << "CmSettingth " <<  e.what() << endl;
    emit errorSettings(e.what());
  }

}

void UIPreferences::CmShowComments( bool checked )
{
    TSchemaNodeModel::showComments = checked;
    _iosettings.setValue("ShowComments",  TSchemaNodeModel::showComments );
    emit viewMenuChanged();
    emit modelChanged();
}

void UIPreferences::CmDisplayEnums( bool checked )
{
    TSchemaNodeModel::useEnumNames = checked;
    _iosettings.setValue("UseEnumNames",  TSchemaNodeModel::useEnumNames );
    emit viewMenuChanged();
    emit tableChanged();
}

void UIPreferences::CmEditID( bool checked )
{
    TSchemaNodeModel::editID = checked;
    _iosettings.setValue("Edit_id",  TSchemaNodeModel::editID );
    emit viewMenuChanged();
}

void UIPreferences::CmEditExpanded( bool checked )
{
  TBsonView::expandedFields = checked;
  _iosettings.setValue("KeepExpanded",  TBsonView::expandedFields );
   // signal to update menu all
   emit viewMenuChanged();
}

void UIPreferences::CmSetUserDir( QString dirPath)
{
   _iosettings.setUserDir( dirPath.toStdString() );
}

} // namespace bsonui
