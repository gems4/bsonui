//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file FormatImpexWidget.cpp
/// Implementation of class FormatImpexWidget - work with Foreign Import/Export File Format
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <QKeyEvent>
#include <QLineEdit>
#include "FormatImpexWidget.h"
#include "ui_FormatImpexWidget.h"
#include "SelectDialog.h"
#ifdef addBSONIO
#include "json2cfg.h"
#else
#include "bsonio/json2cfg.h"
#endif

using namespace bsonio;


namespace bsonui {

TDBSchemaDocument* newDBImpexClient(  const string& queryString  )
{
    return TDBSchemaDocument::newDBSchemaDocument(
      uiSettings().database(), "ImpexFormat", "impexdefs", defSchemaKeyFldsInf,  queryString);
}

StructDataIEFile* connectFormatStructDataFile( int amode, const ThriftSchema* schema, bson *formatBson, const string& lua_lib_path )
{
    FormatStructDataFile fformatdata;
    readDataFromBsonSchema( schema, formatBson, "FormatStructDataFile",  &fformatdata );
    //Create input structure
    StructDataIEFile*  inputIEFile=  new  StructDataIEFile( amode, schema, lua_lib_path, fformatdata );
    return inputIEFile;
}

KeyValueIEFile* connectFormatKeyValueFile( int amode, const ThriftSchema* schema, bson *formatBson, const string& lua_lib_path )
{
    FormatKeyValueFile fformatdata;
    readDataFromBsonSchema( schema, formatBson, "FormatKeyValueFile",  &fformatdata );
    //Create input structure
    KeyValueIEFile*  inputIEFile=  new  KeyValueIEFile( amode, schema, lua_lib_path, fformatdata );
    return inputIEFile;
}


TableIEFile* connectFormatTableFile( int amode, const ThriftSchema* schema, bson *formatBson, const string& lua_lib_path )
{
    FormatTableFile fformatdata;
    readDataFromBsonSchema( schema, formatBson, "FormatTableFile",  &fformatdata );
    //Create input structure
    TableIEFile*  inputIEFile=  new  TableIEFile( amode, schema, lua_lib_path, fformatdata );
    return inputIEFile;
}

AbstractIEFile* connectFormatDataFile( int amode, const string& impexSchemaName, bson *formatBson )
{
    string lua_lib_path = ioSettings().luaLibPath();
    if( impexSchemaName == "FormatStructDataFile" )
         return connectFormatStructDataFile( amode, ioSettings().Schema(), formatBson,  lua_lib_path );
    if( impexSchemaName == "FormatKeyValueFile" )
         return connectFormatKeyValueFile( amode, ioSettings().Schema(), formatBson,  lua_lib_path );
    if( impexSchemaName == "FormatTableFile" )
         return connectFormatTableFile( amode, ioSettings().Schema(), formatBson,  lua_lib_path );
    // other do not realize
    return 0;
 }


void FormatImpexWidget::closeEvent(QCloseEvent* e)
{
    if( !onCloseEvent(this) )
           e->ignore();
       else
           QWidget::closeEvent(e);
}


FormatImpexWidget::FormatImpexWidget(  int mode, const string& arunschemaName,  QWidget *parent) :
    BSONUIBase( "", parent),
    _mode(mode), parentSchemaName(arunschemaName), ui(new Ui::FormatImpexWidget)
{
   ui->setupUi(this);
   ui->splitter->setStretchFactor(0, 1);
   ui->splitter->setStretchFactor(1, 2);

   setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

   QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
   setWindowTitle(title);

   //ui->direcBox->setEnabled(false);

   //set up main parameters
   bson_init(&curRecord);
   bson_finish(&curRecord);

   ui->impexschema->addItem("FormatStructDataFile");
   ui->impexschema->addItem("FormatKeyValueFile");
   ui->impexschema->addItem("FormatTableFile");
   ui->impexschema->addItem("FormatTextFile");
   curSchemaName = ui->impexschema->currentText().toStdString();

   //define schema checkbox
   vector<string> vertexNames = ioSettings().Schema()->getVertexesList();
   for( uint ii=0; ii<vertexNames.size(); ii++ )
      ui->schemaBox->addItem(vertexNames[ii].c_str());
   vertexNames = ioSettings().Schema()->getEdgesList();
   for( uint ii=0; ii<vertexNames.size(); ii++ )
      ui->schemaBox->addItem(vertexNames[ii].c_str());

   if( !parentSchemaName.empty() )
     ui->schemaBox->setCurrentText(parentSchemaName.c_str());

   if( _mode )
     ui->direcBox->setCurrentIndex(_mode-1);

   // define edit tree view
   aHeaderData << "key" << "value" << "comment"  ;
   fieldTable =  new TBsonView(  ui->bsonWidget );
   model_schema = new TSchemaNodeModel(  ioSettings().Schema(), &curRecord,
           curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
   deleg_schema = new TSchemaNodeDelegate();

   fieldTable->setModel(model_schema);
   fieldTable->setItemDelegate(deleg_schema);
   fieldTable->setColumnWidth( 0, 250 );
   fieldTable->expandToDepth(0);
   ui->gridLayout_3->addWidget(fieldTable, 1, 0, 1, 2);

   // define menu
   _execF = [](AbstractIEFile *, const string&, bool ){ };
   setActions();
   resetDBClient();

   // set up default data
   CmNew();
}

FormatImpexWidget::~FormatImpexWidget()
{
    delete ui;

    //cout << "~FormatImpexWidget" << endl;
}

bool FormatImpexWidget::resetBson(const string& schemaName )
{
   // test legal schema name
   if( !curSchemaName.empty() &&
       !schemaName.empty() && schemaName != curSchemaName )
   return false;
   fieldTable->updateModelData( &curRecord, curSchemaName );
   fieldTable->update();
   return true;
}

//-------------------------------------------------------------


/// Change current View menu selections
void FormatImpexWidget::updtViewMenu()
{
    ui->action_Show_comments->setChecked( TSchemaNodeModel::showComments );
    ui->action_Display_enums->setChecked(TSchemaNodeModel::useEnumNames);
    ui->action_Edit_id->setChecked(TSchemaNodeModel::editID );
    ui->actionKeep_Data_Fields_Expanded->setChecked(TBsonView::expandedFields );
}

/// Change model ( show/hide comments)
void FormatImpexWidget::updtModel()
{
  if( !curSchemaName.empty() )
  {
      model_schema->updateModelData();
      fieldTable->header()->resizeSection(0, 250);
      fieldTable->header()->resizeSection(1, 250);
      fieldTable->expandToDepth(0);
  }
}

/// Change table (Show Enum Names Instead of Values)
void FormatImpexWidget::updtTable()
{
    fieldTable->hide();
    fieldTable->show();
}

/// Update after change DB locations
void FormatImpexWidget::updtDB()
{
  //resetDBClient();
    dbquery->runQuery();
}

//--------------------------------------------------------------------

//  Connect all actions
void FormatImpexWidget::setActions()
{
    connect( ui->impexschema, SIGNAL(currentIndexChanged(const QString&)),
             this, SLOT(schemaChanged(const QString&)));
    QObject::connect( ui->datafileButton, SIGNAL(clicked()), this, SLOT(CmSelectDataFile()));

    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    //connect( ui->action_Clone_Structured_Data_Object , SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));
    connect( ui->actionSelect_Data_File, SIGNAL( triggered()), this, SLOT(CmSelectDataFile()));
    connect( ui->action_Run_script, SIGNAL( triggered()), this, SLOT(CmRunScript()));

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
    connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
    connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    //connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));
    updateViewMenu();

    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));

   /*if( !curSchemaName.empty() )
   { QLineEdit* pLineTask = new QLineEdit( ui->nameToolBar );
     pLineTask->setEnabled( true );
     pLineTask->setFocusPolicy( Qt::ClickFocus );
     pLineTask->setReadOnly( true );
     pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
     QString title =  curSchemaName.c_str();
     pLineTask->setText(title);
     ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
     ui->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
   }*/

   if( _mode == editMode)
   {    ui->runToolBar->hide();
        ui->action_Run_script->setDisabled(true);
        ui->actionSelect_Data_File->setDisabled(true);
        ui->datafileButton->setDisabled(true);
        ui->datafileEdit->setDisabled(true);
        ui->overwriteBox->setDisabled(true);
   }else
       ui->editToolBar->hide();
}

// Menu commands -----------------------------------------------------------

/// Change current Edge
void FormatImpexWidget::schemaChanged(const QString & text)
{
  try {
        curSchemaName = text.toUtf8().data();

        bson_destroy( &curRecord );
        bson_init( &curRecord );
        bson_finish( &curRecord );
        model_schema->setupModelData( &curRecord, curSchemaName);
        fieldTable->expandToDepth(0);
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Set default bson record
void FormatImpexWidget::CmNew()
{
  try{
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          resetBson("");
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/* Clone bson record (clear _id)
void FormatImpexWidget::CmClone()
{
  try{
        TSchemaNodeModel*model = dynamic_cast<TSchemaNodeModel*>(model_schema);
        if( model)
        {  string key ="";
           model->setValue("_id",key);
           fieldTable->hide();
           fieldTable->show();
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}*/

// Save current record to DB file as new
void FormatImpexWidget::CmCreateInsert()
{
    try
    {
        if( dbquery.get() == 0 )
          return;

        bson obj;
        impexToBson(&obj);
        string key = dbquery->SetBson( &obj );
        bson_destroy(&obj);
        dbquery->InsertRecord();
        contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void FormatImpexWidget::CmRead()
{
    try
    {
        if( dbquery.get() == 0 )
          return;

        // Select keys to load
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        dbquery->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to read/view", aValList, 0 );
         if( !selDlg.exec() )
          return;

        string reckey = aKeyList[selDlg.selIndex()];
        // Read Record
        dbquery->GetRecord( reckey.c_str() );
        bson obj;
        dbquery->GetBson( &obj );
        impexFromBson( obj.data );
        bson_destroy(&obj);
        contentsChanged = false;

   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void FormatImpexWidget::CmUpdate()
{
    try
    {
       if( dbquery.get() == 0 )
          return;
       bson obj;
       impexToBson(&obj);
       string key = dbquery->SetBson( &obj );
       bson_destroy(&obj);
       dbquery->SaveRecord( key.c_str() );
       contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void FormatImpexWidget::CmDelete()
{
    try
    {
        if( dbquery.get() == 0 )
          return;
        // get current key
        string key = ui->nameEdit->text().toUtf8().data();
        key+=":";
        dbquery->DelRecord( key.c_str() );
        contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void FormatImpexWidget::CmImportJSON()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with format", "*.json"  ))
       {
          FJson file( fileName);
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          if( !resetBson(schemafromName( fileName )) )
          {
            model_schema->saveToBson();
            bsonioErr( fileName , "Try to read another schema format file" );
          }
          contentsChanged = true;
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void FormatImpexWidget::CmExportJSON()
{
   try {
         string fileName = fileShemaExt( curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the format", "*.json", fileName  ))
         {
            FJson file( fileName);
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void FormatImpexWidget::CmSelectDataFile()
{
    try{
          string fileName = ui->datafileEdit->text().toUtf8().data();
          if(  ChooseFileOpen( this, fileName,
                       "Select Input File Path", ""  ))
          {
            ui->datafileEdit->setText( fileName.c_str() );
         }
      }
     catch(bsonio_exeption& e)
     {
         QMessageBox::critical( this, e.title(), e.what() );
     }
     catch(std::exception& e)
      {
         QMessageBox::critical( this, "std::exception", e.what() );
      }
}

void FormatImpexWidget::CmRunScript()
{
  try{
       unique_ptr<AbstractIEFile> inputIEFile( connectFormatFile( &curRecord ) );
       string datafile = ui->datafileEdit->text().toUtf8().data();
       bsonioErrIf(datafile.empty(), "Undefined Data to Import","Please, select file with data to import");
       bool overwrite = ui->overwriteBox->isChecked();
       _execF(inputIEFile.get(), datafile,overwrite);

       close();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


//-------------internal

AbstractIEFile* FormatImpexWidget::connectFormatFile( bson *formatBson )
{
    int mode = ui->direcBox->currentIndex();
    return connectFormatDataFile( mode, curSchemaName, formatBson );

    /*string lua_lib_path = theSettings().luaLibPath();
    if( curSchemaName == "FormatStructDataFile" )
         return connectFormatStructDataFile( theSettings().Schema(), formatBson,  lua_lib_path );
    if( curSchemaName == "FormatKeyValueFile" )
         return connectFormatKeyValueFile( theSettings().Schema(), formatBson,  lua_lib_path );
    if( curSchemaName == "FormatTableFile" )
         return connectFormatTableFile( theSettings().Schema(), formatBson,  lua_lib_path );
    // other do not realize
    return 0;*/
 }

// set data to bson
void FormatImpexWidget::impexToBson( bson *fbson )
{
     // set data to bson
     bson_init(fbson);
     string key = ui->nameEdit->text().toUtf8().data();
     /*if( Find( key) )
     { bson_oid_t oid;
       bson_oid_from_string( &oid, _id.c_str() );
       bson_append_oid( fbson, JDBIDKEYNAME, &oid);
     }*/
     bson_append_string( fbson, "name", key.c_str() );
     bson_append_string( fbson, "impexschema", ui->impexschema->currentText().toUtf8().data() );
     bson_append_string( fbson, "schema", ui->schemaBox->currentText().toUtf8().data() );
     bson_append_string( fbson, "comment", ui->descEdit->text().toUtf8().data() );
     bson_append_int( fbson, "direction", ui->direcBox->currentIndex() );
     string impexstr;
     jsonFromBson( curRecord.data, impexstr );
    //bson_append_bson( &fbson, "find", &curRecord  );
      bson_append_string( fbson, "impex", impexstr.c_str()  );

      // not used now
//      bson_append_string( fbson, "format", "" );
//      bson_append_string( fbson, "extension", "" );
       bson_finish(fbson);
}

void FormatImpexWidget::impexFromBson( const char *fbson )
{

   //  char oidhex[25];
   //  bson_oid_to_string(bson_iterator_oid(it), oidhex);
   //  str = oidhex;

   // setup data from bson
   string getdata;
   if(!bson_find_string( fbson, "name", getdata ) )
             getdata = "new_impex";
   ui->nameEdit->setText(getdata.c_str());
   if(!bson_find_string( fbson, "impexschema", getdata ) )
             getdata = "";
   ui->impexschema->setCurrentText( getdata.c_str());
   if(!bson_find_string( fbson, "schema", getdata ) )
             getdata = "";
   ui->schemaBox->setCurrentText( getdata.c_str());
   if(!bson_find_string( fbson, "comment", getdata ) )
             getdata = "";
   ui->descEdit->setText(getdata.c_str());

   int direc;
   if(!bson_find_value( fbson, "direction", direc ) )
             direc = 0;
   ui->direcBox->setCurrentIndex(direc);

    // read impex
   if( bson_to_string( fbson, "impex", getdata ))
   {
      bson_destroy(&curRecord );
      jsonToBson( &curRecord, getdata );
      resetBson("");
   }

// not used now
    if(!bson_find_string( fbson, "format", getdata ) )
              getdata = "";
    if(!bson_find_string( fbson, "extension", getdata ) )
              getdata = "";
}

void FormatImpexWidget::resetDBClient()
{
    if( curCollectionName.empty())
       return;

   try{
       string queryString = "";
       if( _mode!=editMode && !parentSchemaName.empty() )
       {
         queryString = "{ 'schema': '";
         queryString += parentSchemaName;
         queryString += "', ";
         if( _mode == runModeImport )
            queryString += " 'direction': 0 }";
        else
            queryString += " 'direction': 1 }";
       }

       TDBSchemaDocument* newClient = TDBSchemaDocument::newDBSchemaDocument(
         uiSettings().database(), "ImpexFormat", "impexdefs", defSchemaKeyFldsInf,  queryString);
       // no schema
      if( newClient == nullptr )
       dbquery.reset();
      else
       dbquery.reset( newClient );

    }
    catch(std::exception& e)
      {
         cout << "Internal comment " << e.what() << endl;
         throw;
      }
}

} // namespace bsonui

// end of FormatImpexWidget.cpp
