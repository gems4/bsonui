//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file PreferencesBSONUI.cpp
/// Implementation dialog for define preferences into BSONUI
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QFileDialog>
#include "PreferencesBSONUI.h"
#include "ui_PreferencesBSONUI.h"
using namespace bsonio;

namespace bsonui {

PreferencesBSONUI::PreferencesBSONUI( bsonio::BsonioSettings& aset,QWidget *parent) :
    QDialog(parent),iosettings(aset),
    ui(new Ui::PreferencesBSONUI)
{
    ui->setupUi(this);
    QValidator *validator = new QIntValidator( this );
    ui->portEdit->setValidator(validator);

    {
        ui->resourcesEdit->setText( iosettings.value("ResourcesDirectory", string("")).c_str() );
        ui->schemasEdit->setText( iosettings.value("SchemasDirectory", string("")).c_str() );
        ui->usersEdit->setText( iosettings.value("UserFolderPath", string(".")).c_str() );

        ui->commentBox->setChecked( iosettings.getBool("ShowComments", false) );
        ui->enumBox->setChecked( iosettings.getBool("UseEnumNames", false) );
        ui->idBox->setChecked( iosettings.getBool("Edit_id", false) );
        ui->expandedBox->setChecked( iosettings.getBool("KeepExpanded", true) );

        //ui->localdbEdit->setText( settings->value("LocalDBPath", "./EJDB/localejdb").toString() );
        ui->localdbEdit->setText( iosettings.value("LocalDBDirectory", string("./EJDB")).c_str() );
        ui->localdbName->setText( iosettings.value("LocalDBName", string("localdb")).c_str() );
        ui->localdbCollection->setText( iosettings.value("LocalDBCollection", string("data")).c_str() );
        ui->localdbBox->setChecked( iosettings.getBool("UseLocalDB", false) );

        ui->hostEdit->setText( iosettings.value( "DBSocketHost", defHost ).c_str() );
        ui->portEdit->setText( QString::number(iosettings.value("DBSocketPort", defPort )) );
    }

    localDBDrive = ui->localdbBox->isChecked();
    DBStateChanged(localDBDrive);

    QObject::connect( ui->buttonBox, SIGNAL(accepted()), this, SLOT(CmSave()));
    QObject::connect( ui->buttonBox, SIGNAL(helpRequested()), this, SLOT(CmHelp()));
    QObject::connect( ui->usersButton, SIGNAL(clicked()), this, SLOT(CmProjectDir()));
    QObject::connect( ui->schemasButton, SIGNAL(clicked()), this, SLOT(CmSchemasDir()));
    QObject::connect( ui->resourcesButton, SIGNAL(clicked()), this, SLOT(CmResourcesDir()));
    QObject::connect( ui->localdbButton, SIGNAL(clicked()), this, SLOT(CmLocalDBDir()));
    QObject::connect( ui->localdbBox, SIGNAL(stateChanged(int)), this, SLOT(DBStateChanged(int)));
}

PreferencesBSONUI::~PreferencesBSONUI()
{
    delete ui;
}

void PreferencesBSONUI::DBStateChanged(int state)
{
    ui->hostEdit->setEnabled(!state);
    ui->portEdit->setEnabled(!state);
    ui->localdbEdit->setEnabled(state);
    ui->localdbName->setEnabled(state);
}

void PreferencesBSONUI::CmSave()
{
    // save current settings
    iosettings.setValue("ResourcesDirectory", ui->resourcesEdit->text().toStdString() );
    iosettings.setValue("SchemasDirectory", ui->schemasEdit->text().toStdString() );
    iosettings.setValue("ShowComments",  ui->commentBox->isChecked() );
    iosettings.setValue("UseEnumNames",  ui->enumBox->isChecked() );
    iosettings.setValue("Edit_id",  ui->idBox->isChecked() );
    iosettings.setValue("KeepExpanded",  ui->expandedBox->isChecked() );
    //settings->setValue("LocalDBPath",   ui->localdbEdit->text() );
    iosettings.setValue("LocalDBDirectory",   ui->localdbEdit->text().toStdString() );
    iosettings.setValue("LocalDBName",   ui->localdbName->text().toStdString() );
    iosettings.setValue("LocalDBCollection",   ui->localdbCollection->text().toStdString() );
    iosettings.setValue("UseLocalDB",    ui->localdbBox->isChecked() );
    iosettings.setValue("DBSocketHost",  ui->hostEdit->text().toStdString() );
    iosettings.setValue("DBSocketPort",   ui->portEdit->text().toInt() );

    // setup global values
    iosettings.setUserDir( ui->usersEdit->text().toStdString() );
    uiSettings().updateUiSettings();

    bool newDBDrive = ui->localdbBox->isChecked();
    bool ejdbChanged = false;
    bool serverChanged = false;

    // test local db settings
    if(  iosettings.updateLocalDB()  )
       ejdbChanged = true;

    // change server settingth
    if( iosettings.updateClientDB() )
       serverChanged = true;

    // test to reset db driver => close all windows (closeAll) or only (updateDB)
    if( (newDBDrive != localDBDrive) || (localDBDrive&&ejdbChanged) || (!localDBDrive&&serverChanged) )
    {
        uiSettings().updateDatabase() ;
        emit dbdriveChanged();
    }
    accept();
}

void PreferencesBSONUI::CmProjectDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Project Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->usersEdit->setText( dir );
}

void PreferencesBSONUI::CmSchemasDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Schemas Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->schemasEdit->setText( dir );
}

void PreferencesBSONUI::CmResourcesDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Resource Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->resourcesEdit->setText( dir );
}

void PreferencesBSONUI::CmLocalDBDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Local EJDB Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    //QString dir = QFileDialog::getSaveFileName(  this, "Select Local EJDB file ",
    //      "", "All files (*)", 0,  QFileDialog::DontConfirmOverwrite );
    if( !dir.isEmpty() )
      ui->localdbEdit->setText( dir );
}

void PreferencesBSONUI::CmHelp()
{ }

} // namespace bsonui

// ------------------------ end of PreferencesBSONUI.cpp ------------------------------------------
