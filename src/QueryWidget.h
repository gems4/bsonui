//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file QueryWidget.h
/// Declarations of class QueryWidget widget to work with database queries
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef QUERYWIDGET_H
#define QUERYWIDGET_H

#include <QMainWindow>
#ifdef addBSONIO
#include "dbschemadoc.h"
#else
#include "bsonio/dbschemadoc.h"
#endif
#include "model_query.h"
using namespace std;

namespace Ui {
class QueryWidget;
}

namespace bsonui {

class TStringVectorModel;
class TQueryView;

/// \class QueryWidget window to insert/edit  database query
class QueryWidget : public QMainWindow
{
    Q_OBJECT

    friend class TQueryView;

    string _schemaName;
    //string _id;
    //::apache::thrift::stdcxx::shared_ptr<bsonio::TDBSchemaDocument> dbquery;
    std::shared_ptr<bsonio::TDBSchemaDocument> dbquery;
    bsonio::DBQueryDef defaultQr;
    bsonio::DBQueryDef queryDef;

    ///  Connect all actions
    void setActions();

    void queryToBson( bson *obj );
    void queryFromBson( const char *obj );
    bool selectLine( string& fldpath, bsonio::SchInternalData& flddata );

protected slots:

    void objectChanged(){}
    void schemaChanged(const QString & text);

    // Files
    void CmSaveQuery();
    void CmLoadQuery();
    void CmExecute();
    void CmApplay()
    {
      CmExecute();
      close();
    }

    // Select part
    void CmSelectFields();
    void CmAddSelectFields();
    void CmSelectRestore();

    // Where part
    void CmWhereApply();
    void CmWhereRestore();
    void CmClearTree();

    // Menu
    void CmAddWhereFields();

    // Record
    void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();

    // Help
    void CmHelp(){}
    void CmHelpAbout(){}

public:

    explicit QueryWidget(const char* title,  const vector<string>& schemaNames,
                         const bsonio::DBQueryDef& query,
                         QWidget *resultW, QWidget *parent = 0);
    ~QueryWidget();

    /// Return query ( Where )
    const bsonio::DBQueryDef& getQueryDef();

    /// Update query
    void Update( const bsonio::DBQueryDef& query );

    /// Reset current DB client
    void resetDBClient();

private:

    Ui::QueryWidget *ui;
    TStringVectorModel* lstmodel;
    TQueryView* queryEdit;
    QWidget* resultWidget;
    QLineEdit* qName;

};

} // namespace bsonui

#endif // QUERYWIDGET_H
