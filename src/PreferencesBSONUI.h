//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file PreferencesBSONUI.h
/// Declaration of PreferencesBSONUI  dialog for define preferences into BSONUI
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef PREFERENCESBSONUI_H
#define PREFERENCESBSONUI_H

#include <QDialog>
#include "preferences.h"

namespace Ui {
class PreferencesBSONUI;
}

namespace bsonui {

/// \class  PreferencesBSONUI  dialog for insert/edit settings
class PreferencesBSONUI : public QDialog
{
    Q_OBJECT

    bsonio::BsonioSettings& iosettings;
    bool localDBDrive;

signals:
    void dbdriveChanged();

public slots:
    void CmSave();
    void CmProjectDir();
    void CmSchemasDir();
    void CmResourcesDir();
    void CmLocalDBDir();
    void CmHelp();
    void DBStateChanged(int);

public:
    explicit PreferencesBSONUI( bsonio::BsonioSettings& aset, QWidget *parent = 0 );
    ~PreferencesBSONUI();

private:
    Ui::PreferencesBSONUI *ui;
};

} // namespace bsonui

#endif // PREFERENCESBSONUI_H
