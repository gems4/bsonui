//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_node.cpp
/// Implementation of TSchemaNodeModel, TSchemaNodeDelegate
/// implements a tree view of internal DOM based on our JSON schemas
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>
#include "model_node.h"
#include "SelectDialog.h"
#include "ModelLineDialog.h"
using namespace bsonio;

namespace bsonui {

bool TSchemaNodeModel::showComments = false;
bool TSchemaNodeModel::useEnumNames = false;
bool TSchemaNodeModel::editID = false;

SchemaNodeLine::SchemaNodeLine( SchemaNode* afldDef, int andx ):
    dataNode(afldDef),ndx(andx)
{
  // make children
  addAllChildren( false );
}

// make children
void SchemaNodeLine::addAllChildren( bool all )
{
  SchemaNode* child;
  SchemaNodeLine *line;
  for(uint ii=0, ndx=0; ii< dataNode->getChildNum(); ii++ )
  {
      child = dataNode->children(ii);
      if( all || child->isSetUp() )
      {
          line = new SchemaNodeLine( child, ndx++ );
          line->parent = this;
          children.push_back( unique_ptr<SchemaNodeLine>(line) );

      } else
        {     noUsedFields.push_back( child );
              noUsedKeys.push_back( child->getKey() );
        }
   }
}

//--------------------------------------------------------------------------------------
//  class TSchemaNodeModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//  Reading/writing data from/to DOM thrift schemas data
//---------------------------------------------------------------------------------------
TSchemaNodeModel::TSchemaNodeModel( const ThriftSchema *aschema, bson *abson, const string& aschemaName,
                                    const QStringList& aHeaderData,   QObject* parent ):
        TBsonAbstractModel(parent), _schema(aschema), _schemaName(aschemaName),
        _rootNode(0), _rootLine(0), _hdData( aHeaderData), _bsonData(abson)
{
  setupModelData(abson, aschemaName);
  connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
  			parent,  SLOT(objectChanged()) );	 
}

TSchemaNodeModel::~TSchemaNodeModel()
{
    if(_rootNode )
       delete _rootNode;
    if(_rootLine )
       delete _rootLine;
}

SchemaNodeLine *TSchemaNodeModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<SchemaNodeLine *>(index.internalPointer());
    } else {
        return _rootLine;
    }
}


void TSchemaNodeModel::setupModelData( bson *abson, const string& aschemaName )
{
    const char *bsdata = 0;
    if( abson && (abson->cur - abson->data) > 5 )
        bsdata = abson->data;

    _bsonData = abson;
    _schemaName = aschemaName;

    // test existed name
    ThriftStructDef* strDef = _schema->getStruct( _schemaName );
    if( strDef == nullptr )
       bsonioErr( _schemaName, "Undefined struct definition" );

   try {
         beginResetModel();

        if( _rootNode )
           delete _rootNode;
       _rootNode = 0;
       if( _rootLine )
          delete _rootLine;
      _rootLine = 0;

      _rootNode = SchemaNode::newSchemaStruct( _schemaName, bsdata );
      _rootLine = new SchemaNodeLine( _rootNode, 0 );

       endResetModel();
   } catch( ... )
     {
        endResetModel();
        throw;
      }
}

QModelIndex TSchemaNodeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!_rootLine)
        return QModelIndex();
    SchemaNodeLine *parentItem = lineFromIndex( parent );
    return createIndex(row, column, parentItem->children[row].get());
}

QModelIndex TSchemaNodeModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    SchemaNodeLine *parentItem = lineFromIndex(child)->parent;
    if (parentItem == _rootLine )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}


int TSchemaNodeModel::rowCount( const QModelIndex& parent ) const
{
   if (!_rootLine)
       return 0;
   if (parent.column() > 0)
      return 0;
   SchemaNodeLine *parentItem = lineFromIndex( parent );
   return parentItem->children.size();
}	

int TSchemaNodeModel::columnCount( const QModelIndex& /*parent*/ ) const
{
   if( showComments )
     return 3;
   return 2;
}	

Qt::ItemFlags TSchemaNodeModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  SchemaNodeLine *item = lineFromIndex( index );

  if( !editID && item->dataNode->getKey()[0] == '_' )
  {
    return (flags & ~Qt::ItemIsEditable);
  }

  if( index.column() == 0 && item->parent && item->parent->dataNode->isMap() )
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }

  if( index.column() == 1 &&
      !( item->dataNode->isStruct() || item->dataNode->isArray() ) )
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }
  else
     return (flags & ~Qt::ItemIsEditable);
}

QVariant TSchemaNodeModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
 if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<_hdData.size())
       return _hdData[section];
  return QVariant();
}

QString TSchemaNodeModel::getValue( int column, SchemaNode *node ) const
{
   switch( column )
   {
     case 0: return  QString(node->getKey().c_str());
     case 2: return  QString(node->getDescription().c_str());
     case 1:
     {  string val;
        node->getValue(val);
        return  QString(val.c_str());
     }
   }
   return QString("");
}

QVariant TSchemaNodeModel::data( const QModelIndex& index, int role ) const
{
   if(!index.isValid())
	 return QVariant();	

   SchemaNodeLine *item = lineFromIndex( index );
   SchemaNode *node = item->dataNode;

   switch( role )
   {
     case Qt::DisplayRole:
          {
              string enumName;
              if( useEnumNames && !(enumName = node->getEnumName()).empty() )
              {
                 ThriftEnumDef* enumdef = _schema->getEnum(enumName );
                 if( enumdef != nullptr )
                 {
                     int idx;
                     node->getValue(idx);
                     string name = enumdef->getNamebyId(idx);

                     if( index.column() == 1 )
                      return  QString(name.c_str());
                     if( index.column() == 2 )
                      return  QString(enumdef->getDoc(name).c_str());
                  }
              }
              return getValue( index.column(), node );
           }
           break;
      case Qt::EditRole:
          return getValue( index.column(), node );
      case Qt::ToolTipRole:
      case Qt::StatusTipRole:
              return  QString(node->getFullDescription().c_str() );
      case Qt::ForegroundRole:
          {
            if ( index.column() == 0 && item->parent && !item->parent->dataNode->isMap()  )
               {
                 return QVariant( QColor( Qt::darkCyan ) );
               }
            //return QVariant( QColor( Qt::black ) );
          }
      default: break;
   }
   return QVariant();
}

bool TSchemaNodeModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
	if( index.isValid() && ( role == Qt::EditRole ) )
	{
       SchemaNode *node = lineFromIndex( index )->dataNode;
       string val = value.toString().toUtf8().data();
       switch( index.column() )
       {
         case 0: node->setMapKey( val );
                break;
         case 1:
                node->setValue(val);
                break;
         default:
                break;
       }

       node_to_bson();
       return true;
	} 
	return false;
}

//---------------------------------------------------------------------------------------
void TSchemaNodeModel::resizeArray( QWidget *parentWidget, const QModelIndex& index )
{
    // get array size and def value
    SchemaNodeLine *arline =  lineFromIndex(index);
    SchemaNode *arnode = arline->dataNode;
    ModelLineDialog dlg( index, index, arnode->getKey().c_str(), parentWidget );
    if( !dlg.exec() )
       return;
    string new_value = dlg.getDefValue().toUtf8().data();
    vector<int> sizes = dlg.getArraySizes();

    // delete old data
    if(  rowCount( index ) )
    {
      beginRemoveRows( index, 0, rowCount( index ) );
      arline->children.clear();
      endRemoveRows();
    }

    // insert new list
    try {
         beginInsertRows( index, 0, sizes[0]-1 );
         arnode->resizeArray( sizes,  new_value );
         arline->addAllChildren(true);
         endInsertRows();
     } catch( ... )
       {
        endInsertRows();
        throw;
       }

    node_to_bson();
}

void TSchemaNodeModel::delObject( QWidget * parentWidget, const QModelIndex& index )
{
    SchemaNodeLine *line =  lineFromIndex(index);
    SchemaNode *node = line->dataNode;

    if( node->isReqiered( )  )
    {
       QMessageBox::warning(parentWidget, "Data object to delete",
                "Required data object cannot be deleted");
        return;
    }

    QString msg = QString("Confirm deletion of %1 data object?").arg( node->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data object to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    beginRemoveRows( parent(index), index.row(), index.row());

    node->removeField();
    SchemaNodeLine *parent_ = line->parent;
    if( !parent_->dataNode->isMap() )
    { parent_->noUsedFields.push_back( node );
      parent_->noUsedKeys.push_back( node->getKey() );
    }
    parent_->children.erase( parent_->children.begin() + index.row() );
    // copy childrens
    for(uint ii=index.row(); ii<parent_->children.size(); ii++ )
      parent_->children[ii].get()->ndx--;

    endRemoveRows();
    node_to_bson();
}

void TSchemaNodeModel::resetObject( QWidget * parentWidget, const QModelIndex& index )
{
    SchemaNodeLine *line =  lineFromIndex(index);
    SchemaNode *node = line->dataNode;


    QString msg = QString("Confirm clear of %1 data object?").arg( node->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data object to clear", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    // delete old data
    if(  rowCount( index ) )
    {
      beginRemoveRows( index, 0, rowCount( index ) );
      line->children.clear();
      endRemoveRows();
    }

    node->clearNode();

    try{
         // start insert row
        uint nchildr = node->getChildNum();
        if( nchildr )
        {
           beginInsertRows( index, 0, nchildr );
           line->addAllChildren(false);
           endInsertRows();
        }
    } catch( ... )
      {
       endInsertRows();
       throw;
      }

    node_to_bson();
}


void TSchemaNodeModel::delObjectsUnion( QWidget * parentWidget, const QModelIndex& index )
{
    SchemaNodeLine *line =  lineFromIndex(index);
    SchemaNode *node = line->dataNode;

    QString msg = QString("Confirm deletion of all alternatives to %1 data object union variant?").arg(
                node->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data objects to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    // resizeline item
    SchemaNodeLine *parent_ = line->parent;
    SchemaNode *delnode;
    uint current_next = index.row()+1;

    if( current_next < parent_->children.size()  )
    { //remove rows after current
       beginRemoveRows( parent(index), current_next, parent_->children.size()-1 );
       for(uint ii=current_next; ii<parent_->children.size(); ii++ )
       {   delnode = parent_->children[ii]->dataNode;
           delnode->removeField();
           parent_->noUsedFields.push_back( delnode );
           parent_->noUsedKeys.push_back( delnode->getKey() );
       }
       parent_->children.erase( parent_->children.begin()+current_next, parent_->children.end() );
       endRemoveRows();
    }

    current_next = index.row();
    if( current_next > 0  )
    { //remove rows before current
       beginRemoveRows( parent(index), 0, current_next );
       for(uint ii=0; ii<current_next; ii++ )
       {   delnode = parent_->children[ii]->dataNode;
           delnode->removeField();
           parent_->noUsedFields.push_back( delnode );
           parent_->noUsedKeys.push_back( delnode->getKey() );
       }
       parent_->children.erase( parent_->children.begin(), parent_->children.begin()+current_next );
       parent_->children[0].get()->ndx = 0;
       endRemoveRows();
    }
    node_to_bson();
}

const QModelIndex TSchemaNodeModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    uint row;
    QModelIndex parentIndex;
    SchemaNodeLine *line =  lineFromIndex(cindex);

    if( line->dataNode->isStruct() && line->children.size() == 0 )
    {
        parentIndex = cindex;
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex);
        row =  rowCount( parentIndex ); //line->parent->children.size();
    }
    SchemaNodeLine *parent_ = lineFromIndex(parentIndex);

    // select fields to be added
    vector<string> sch_lst = parent_->noUsedKeys;
    vector<int> sel;
    if( sch_lst.empty() )
      return cindex;

    SelectDialog selDlg( parentWidget, "Please, select objects to be added", sch_lst, sel );
    if( !selDlg.exec() )
      return cindex;
    sel = selDlg.allSelected();

    // insert from list
    try {
         vector<SchemaNode*> noUsedFields_copy = parent_->noUsedFields;

         beginInsertRows( parentIndex, row, row+sel.size() );
         for(uint ii=0; ii<sel.size(); ii++ )
         {
             SchemaNode* child = noUsedFields_copy[sel[ii]];
             // add object
             child->addField();
             SchemaNodeLine *chline = new SchemaNodeLine( child, row+ii );
             chline->parent = parent_;
             parent_->children.push_back( unique_ptr<SchemaNodeLine>(chline) );
             // del from list
             parent_->noUsedKeys.erase( std::find(parent_->noUsedKeys.begin(),
                                      parent_->noUsedKeys.end(), sch_lst[sel[ii]]) );
             parent_->noUsedFields.erase( std::find(parent_->noUsedFields.begin(),
                                      parent_->noUsedFields.end(), child ));
         }
        endInsertRows();
     } catch( ... )
       {
        endInsertRows();
        throw;
       }

   node_to_bson();
   return index( rowCount( parentIndex )-1, 0, parentIndex);
   //cindex.sibling( parent_->children.size()-1, 0);
}

const QModelIndex  TSchemaNodeModel::addObject( QWidget* parentWidget, const QModelIndex& cindex )
{
    uint row;
    QModelIndex parentIndex;
    SchemaNodeLine *line =  lineFromIndex(cindex);
    if( line->dataNode->isStruct() && line->children.size() == 0 )
    {
        parentIndex = cindex;
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex);
        row =  rowCount( parentIndex ); //line->parent->children.size();
    }

    // Select insert data
    ModelLineDialog dlg(cindex, parentIndex, "",parentWidget);
    if( !dlg.exec() )
       return cindex;
    string new_object = dlg.getName().toUtf8().data();
    string new_value = dlg.getDefValue().toUtf8().data();
    vector<int> sizes = dlg.getArraySizes();

    SchemaNodeLine *parent_ =  lineFromIndex(parentIndex);
    auto ndxinsert = std::find(parent_->noUsedKeys.begin(),
                  parent_->noUsedKeys.end(), new_object);
    if(ndxinsert == parent_->noUsedKeys.end() )
         return cindex;

    try{
         // start insert row
        beginInsertRows( parentIndex, row, row );

        int ndx = ndxinsert - parent_->noUsedKeys.begin();
        SchemaNode* child = parent_->noUsedFields[ndx];
        if( !child->isArray())
           child->setValue( new_value );
        else
          {
            child->addField();
            child->resizeArray( sizes,  new_value );
          }
        SchemaNodeLine *chline = new SchemaNodeLine( child, row );
        chline->parent = parent_;
        parent_->children.push_back( unique_ptr<SchemaNodeLine>(chline) );
        // del from list
        parent_->noUsedKeys.erase( std::find(parent_->noUsedKeys.begin(),
                                 parent_->noUsedKeys.end(), new_object ) );
        parent_->noUsedFields.erase( std::find(parent_->noUsedFields.begin(),
                                 parent_->noUsedFields.end(), child ));
        endInsertRows();
    } catch( ... )
      {
       endInsertRows();
       throw;
      }

    node_to_bson();
    return index( rowCount( parentIndex )-1, 0, parentIndex);
    //cindex.sibling( parent_->children.size()-1, 0);
}

string TSchemaNodeModel::getFieldPath( const QModelIndex& index )
{
    SchemaNodeLine *line =  lineFromIndex(index);
    SchemaNode *node = line->dataNode;
    return node->getFieldPath();
 }

string TSchemaNodeModel::getFieldData( const QModelIndex& index )
{
    SchemaNodeLine *line =  lineFromIndex(index);
    SchemaNode *node = line->dataNode;
    return node->getFieldJSON();
 }

void  TSchemaNodeModel::setFieldData( const QModelIndex& index, const string& data_)
{
    SchemaNodeLine *line =  lineFromIndex(index);
    SchemaNode *node = line->dataNode;

    if( !editID && node->getKey()[0] == '_' )
        return;

    try {
          beginResetModel();

          node->setComplexValue(data_);
          if( _rootLine )
           delete _rootLine;
          _rootLine = 0;
          _rootLine = new SchemaNodeLine( _rootNode, 0 );
          node_to_bson();

           endResetModel();
    } catch( ... )
      {
         endResetModel();
         throw;
       }

 }

//-------------------------------------------------------------------------------------
// TSchemaNodeDelegate -  individual items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TSchemaNodeDelegate::TSchemaNodeDelegate( QObject * parent ):
	QItemDelegate( parent )
{ }

// Editing QTreeView for objects in System page
QWidget *TSchemaNodeDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    TSchemaNodeModel *model = (TSchemaNodeModel *)(index.model() );
    SchemaNodeLine *line =  model->lineFromIndex(index);
    SchemaNode* node = line->dataNode;

    int type;
    // map key
    if( index.column() == 0 )
       type = node->getType(true);
    else
       type = node->getType();

    switch( type )
    {
          case T_BOOL:
                {
                  QComboBox *accessComboBox = new QComboBox(parent);
                  accessComboBox->addItem(tr("false"), tr("false"));
                  accessComboBox->addItem(tr("true"), tr("true"));
                  return accessComboBox;
                 }
          case T_I08:
          case T_I16:
          case T_I32:
                {
                   string enumName = node->getEnumName();
                   if( !enumName.empty() )
                   {
                     ThriftEnumDef* enumdef = model->getEnum( enumName );
                     if(enumdef != nullptr )
                     {
                         vector<string> lst = enumdef->getNamesList();
                         QComboBox *accessComboBox = new QComboBox(parent);
                         for( uint ii=0; ii<lst.size(); ii++ )
                         {
                             string itname =lst[ii];
                             accessComboBox->addItem(tr(itname.c_str()), enumdef->getId(itname) );
                             accessComboBox->setItemData(ii, enumdef->getDoc(itname).c_str(), Qt::ToolTipRole);
                         }
                         return accessComboBox;
                     }
                  }
                }
          case T_I64:
          case T_U64:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                QIntValidator *ivalid = new QIntValidator(lineEdit);
                if( type == T_U64 )
                   ivalid->setBottom(0);
                if( node->minValue() != DOUBLE_EMPTY )
                  ivalid->setBottom(node->minValue());
                if( node->maxValue() != DOUBLE_EMPTY )
                  ivalid->setTop(node->maxValue());
                lineEdit->setValidator(ivalid);
                return lineEdit;
              }
         case T_DOUBLE:
               { QLineEdit *lineEdit = new QLineEdit(parent);
                 QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
                 if( node->minValue() != DOUBLE_EMPTY )
                   dvalid->setBottom(node->minValue());
                 if( node->maxValue() != DOUBLE_EMPTY )
                   dvalid->setTop(node->maxValue());
                 lineEdit->setValidator(dvalid);
                 return lineEdit;
               }
          case T_UTF8:
          case T_STRING:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
              }
          // main constructions
          default:
             bsonioErr("TBsonDelegate" ,"can't print type " );

  }

   return QAbstractItemDelegate::createEditor( parent, option,  index );
}

void TSchemaNodeDelegate::setEditorData(QWidget *editor,
                                  const QModelIndex &index) const
{
   QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
   if( cellEdit)
   {
       //if( currentData().isValid() )
       {
         int idx = cellEdit->findData(index.data(Qt::EditRole));
         if ( idx != -1 )  // -1 for not found
            cellEdit->setCurrentIndex(idx);
       }
       //else
       //    cellEdit->setCurrentIndex( index.data(Qt::EditRole) );
   }
    else
       QItemDelegate::setEditorData(editor, index);
}

void TSchemaNodeDelegate::setModelData(QWidget *editor,
                                 QAbstractItemModel *model,
                                 const QModelIndex &index) const
{
   QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
   if( cellEdit)
   {
      model->setData(index, cellEdit->currentData(),  Qt::EditRole);
   } else
      QItemDelegate::setModelData(editor, model, index);
 }

} // namespace bsonui

//---------------------- End of file  model_node.cpp ---------------------------
