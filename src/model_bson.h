//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_bson.h
/// Declaration of TBsonModel, TBsonDelegate and TBsonView
/// implements a tree view of bson structure
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef _model_bson_h
#define _model_bson_h

#include <vector>
#include <memory>
#include <QAbstractItemModel>
#include <QTreeView>
#include <QItemDelegate>
#ifdef addBSONIO
#include "v_json.h"
#else
#include "bsonio/v_json.h"
#endif

namespace bsonui {

class ExpandedLevelLine;

#ifdef __APPLE__
const char  splitRow = '\r';
const char  splitCol = '\t';
#else
const char  splitRow = '\n';
const char  splitCol = '\t';
#endif

/// \class BsonLine represents an item in a tree view.
/// Item data defines one bson object
class BsonLine
{
public:

    BsonLine( int ndx, const QString& akey, int atipe, const QString& avalue );
    BsonLine( const BsonLine* data );
    ~BsonLine();

    int ndx;
    QString keyname; ///< Name of field
    int type;        ///< Type of field
    QString value;   ///< Value of field

    BsonLine *parent;                      ///< Parent object
    vector<unique_ptr<BsonLine>> children; ///< Children objects
};

/// \class TBsonAbstractModel
/// Abstract class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to json type objects
class TBsonAbstractModel: public QAbstractItemModel
{
    Q_OBJECT

 public:

  TBsonAbstractModel( QObject* parent = 0 ):
      QAbstractItemModel(parent)
  { }
  ~TBsonAbstractModel() {}

  virtual string helpName( const QModelIndex& index ) const = 0;
  virtual bool isNumber( const QModelIndex& index ) const = 0;
  virtual bool isArray( const QModelIndex&  ) const
  { return false; }
  virtual bool isCanBeRemoved( const QModelIndex& index ) const
  { return isStruct(index); }
  virtual void resizeArray( QWidget* , const QModelIndex&  ) { }
  virtual bool isStruct( const QModelIndex&  ) const
  { return false; }
  virtual bool isUnion( const QModelIndex&  ) const
  { return false; }
  virtual void delObject( QWidget* , const QModelIndex&  ) { }
  virtual void delObjectsUnion( QWidget* , const QModelIndex&  ) { }
  virtual const QModelIndex addObject( QWidget* , const QModelIndex& index )
  { return index; }
  virtual const QModelIndex addObjects( QWidget* , const QModelIndex& index  )
  { return index; }
  virtual void resetObject( QWidget* , const QModelIndex&  ) { }
  virtual void setupModelData( bson* abson, const string& schema ) = 0;

  virtual string  getFieldPath( const QModelIndex&  )
   { return "undefined function"; }
  virtual string  getFieldData( const QModelIndex&  )
  { return "undefined function"; }
  virtual void  setFieldData( const QModelIndex& , const string& )
  { //undefined function
  }

  void updateModelData( )
  {
      beginResetModel();
      endResetModel();
  }

  virtual void saveToBson() = 0 ;
};

/// \class TBsonModel
/// Class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to bson objects
class TBsonModel: public TBsonAbstractModel
{
	Q_OBJECT

    QStringList hdData;
    bson *bsonData;
    BsonLine* rootNode;

    QString getDescription( const QModelIndex& index ) const;
    bool getNewLineType(QWidget * parentWidget, int& new_type, QString& new_value);

 public:
	  
  TBsonModel( bson *abson,
              const QStringList& aHeaderData,   QObject* parent = 0 );
  ~TBsonModel();
	 
  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;     //ok
  int columnCount ( const QModelIndex& parent  ) const; // ok
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant& value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const; 
  Qt::ItemFlags flags ( const QModelIndex& index ) const;
	 
  BsonLine* lineFromIndex(const QModelIndex& index) const;
  string helpName( const QModelIndex& index ) const;
  bool isNumber( const QModelIndex& index ) const;
  bool isArray( const QModelIndex& index ) const;
  void resizeArray( QWidget* parent, const QModelIndex& index );
  bool isCanBeRemoved( const QModelIndex& index ) const;
  bool isStruct( const QModelIndex& index ) const;
  void delObject( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObjects( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObject( QWidget* parent, const QModelIndex& index );
  void setupModelData( bson* abson, const string& schema );
  void saveToBson();

  string  getFieldPath( const QModelIndex& index );
  string  getFieldData( const QModelIndex&  index );
  void  setFieldData( const QModelIndex& , const string& );

};


/// \class TBsonDelegate individual items in views are rendered and edited using delegates
class TBsonDelegate: public QItemDelegate
{
	Q_OBJECT

public:
     TBsonDelegate( QObject * parent = 0 );
	 QWidget *createEditor(QWidget *parent,
	                       const QStyleOptionViewItem &option,
	                       const QModelIndex &index) const;
};

/// \class TBsonView implements a tree view of bson structure
/// that displays items from a TBsonAbstractModel model.
class TBsonView: public QTreeView
{
	Q_OBJECT
	
	void keyPressEvent(QKeyEvent* e);
	QString createString();
    QString createHeader();
    bool testCurrentIndex();
    void selectWithChildren(const QModelIndex& parIndex);
    void copyWithChildren( const QModelIndex& parIndex,
        const QModelIndexList& selIndexes, QString& clipText );

    void saveExpandedState( ExpandedLevelLine *topList );
    void restoreExpandedState( ExpandedLevelLine *topList );
    void saveExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *parent );
    void restoreExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *topData );

 protected slots:
    void slotPopupContextMenu(const QPoint& pos);
    void changeCurrent( int section );

 public slots:
    void CmHelp();
    void CmCalc();
    void CmAddObjects();
    void CmAddObject();
    void CmDelObject();
    void CmDelObjectsUnion();
    void CmResizeArray();
    void CmResetObject();

    void _SelectRow();
    void _SelectColumn();
    void _SelectAll();
    void _SelectGroup();
    void _CopyData();
    void _CopyDataHeader();

    void CopyFieldPath();
    void CopyField();
    void PasteField();

 public:

    static bool expandedFields;	///< Extern flag to keep data fields expanded

     TBsonView( QWidget * parent = 0 );

     /// Update model data with restore Expanded Level
     void updateModelData( bson *abson, const string& schemaname );

};

} // namespace bsonui

#endif   // _model_bson_h
