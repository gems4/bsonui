//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_node.h
/// Declaration of TSchemaNodeModel, TSchemaNodeDelegate
/// implements a tree view of internal DOM based on our JSON schemas
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef _model_node_h
#define _model_node_h

#include "model_bson.h"
#ifdef addBSONIO
#include "thrift_node.h"
#else
#include "bsonio/thrift_node.h"
#endif
using namespace bsonio;

namespace bsonui {

/// \class SchemaNodeLine represents an item in a tree view.
/// Item data defines one bsonio::SchemaNode  object
/// ( internal DOM based on our JSON schemas )
class SchemaNodeLine
{
public:

    SchemaNodeLine( SchemaNode* afldDef, int andx );

    ~SchemaNodeLine()
    { children.clear(); }

    void addAllChildren(bool all);
    bool emptyStruct() const
    {
      return  dataNode->isStruct() && children.empty();
    }

    SchemaNode* dataNode;              ///< Field definition
    int ndx;                           ///< Internal ndx
    vector<SchemaNode*> noUsedFields;  ///< Field ptr  can be add
    vector<string> noUsedKeys;         ///< Field names can be add

    SchemaNodeLine *parent;                      ///< Parent DOM object
    vector<unique_ptr<SchemaNodeLine>> children; ///< Children objects
};

/// \class TSchemaNodeModel
/// Class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to SchemaNode object
class TSchemaNodeModel: public TBsonAbstractModel
{
	Q_OBJECT

    const ThriftSchema* _schema;
    string _schemaName;
    SchemaNode* _rootNode;
    SchemaNodeLine* _rootLine;

    QStringList _hdData;
    bson* _bsonData;

    QString getValue( int column, SchemaNode *node ) const;
    void node_to_bson()
    {
        // structNode to bson
         bson_destroy(_bsonData );
        _rootNode->getStructBson(_bsonData);
    }

 public:
	  
    static bool showComments;	///< Extern flag to show schema comments into editor
    static bool useEnumNames;	///< Extern flag to show enum names into editor
    static bool editID;	        ///< Extern flag to edit system data

  TSchemaNodeModel( const ThriftSchema* aschema, bson* abson, const string& schemaName,
              const QStringList& aHeaderData,   QObject* parent = 0 );
  ~TSchemaNodeModel();
  void setupModelData( bson* abson, const string& schemaName );


  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;     //ok
  int columnCount ( const QModelIndex& parent  ) const; // ok
  QVariant data ( const QModelIndex& index, int role ) const;
  bool setData ( const QModelIndex& index, const QVariant & value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const; 
  Qt::ItemFlags flags ( const QModelIndex& index ) const;
	 
  // special functions
  SchemaNodeLine* lineFromIndex(const QModelIndex& index) const;
  string helpName( const QModelIndex& index ) const
  {
    // get name from thrift schema
    return  lineFromIndex(index)->dataNode->getFieldKey();
  }

  bool isNumber( const QModelIndex& index ) const
  {
    return  lineFromIndex(index)->dataNode->isNumber();
  }

  bool isArray( const QModelIndex& index ) const
  {
     return  lineFromIndex(index)->dataNode->isArray();
  }
  void resizeArray( QWidget* parent, const QModelIndex& index );

  bool isCanBeRemoved( const QModelIndex& index ) const
  {
      auto line = lineFromIndex(index);
      return  line->dataNode->isTop() || line->parent->dataNode->isStruct()
              || line->parent->dataNode->isMap() ;
  }

  bool isStruct( const QModelIndex& index ) const
  {
      auto line = lineFromIndex(index);
      return  line->dataNode->isTop() || line->parent->dataNode->isStruct()
              || line->emptyStruct() ;
  }
  void delObject( QWidget* parent, const QModelIndex& index );
  void resetObject( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObjects( QWidget* parent, const QModelIndex& index );
  const QModelIndex addObject( QWidget* parent, const QModelIndex& index );

  bool isUnion( const QModelIndex&  index ) const
  {
     return  lineFromIndex(index)->dataNode->isUnion();
  }
  void delObjectsUnion( QWidget* , const QModelIndex&  );

  string  getFieldPath( const QModelIndex& index );
  string  getFieldData( const QModelIndex&  index );
  void  setFieldData( const QModelIndex& , const string& );

  /// Get description of enum
  ThriftEnumDef* getEnum(const string& enumName) const
  {
      return _schema->getEnum( enumName );
  }

  void saveToBson()
  {
    node_to_bson();
  }

  void setOid( const string& oidstr )
  {
      SchemaNode* oidNode = _rootNode->field("_id");
      if( oidNode != nullptr )
          oidNode->setValue(oidstr);
      node_to_bson();
  }

  // working with internal data

  /// Get Value from Node
  /// If field is not type T, the false will be returned.
  template <class T>
  bool getValue( const string& fldpath, T& value  )
  {
      SchemaNode *data = _rootNode->field(  fldpath );
      if( data == nullptr)
        return false;
      else
        return  data->getValue( value );
  }

  /// Set Value to Node
  template <class T>
  void setValue( const string& fldpath, const T& value  )
  {
      SchemaNode *data = _rootNode->field(  fldpath );
      if( data != nullptr)
        data->setValue(value);
      node_to_bson();
  }

};


/// \class TSchemaNodeDelegate individual items in views are rendered and edited using delegates
class TSchemaNodeDelegate: public QItemDelegate
{
	Q_OBJECT

public:
	
     TSchemaNodeDelegate( QObject * parent = 0 );
	 QWidget *createEditor(QWidget *parent,
	                       const QStyleOptionViewItem &option,
	                       const QModelIndex &index) const;
     void setEditorData(QWidget *editor, const QModelIndex &index) const;
     void setModelData(QWidget *editor, QAbstractItemModel *model,
                          const QModelIndex &index) const;
};

} // namespace bsonui

#endif   // _model_node_h
