//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file VertexWidget.h
/// Declaration of class VertexWidget - Widget to work with vertex -
/// internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef VERTEXWIDGET_H
#define VERTEXWIDGET_H

#include <QCloseEvent>
#include "BSONUIBase.h"
#include "model_node.h"
#include "dbkeysmodel.h"

namespace Ui {
class VertexWidget;
}

namespace bsonui {

/// \class VertexWidget - window to work with Vertexes (TDBGraph)
class VertexWidget : public BSONUIBase
{
    Q_OBJECT

    // Internal data
    bson curRecord;
    std::shared_ptr<bsonio::TDBVertexDocument> dbgraph;
    bool contentsChanged = false;

    // Work functions

    /// Set up menu commands
    void setActions();
    /// Set up current bson data to view model
    bool resetBson(const string& schemaName );
    /// Reset current DB client
    void resetDBClient(const string& schemaName );

    void closeEvent(QCloseEvent* e);

    // update after change preferences
    virtual void updtViewMenu();
    virtual void updtModel();
    virtual void updtTable();
    virtual void updtDB();

signals:

    /// Emit signal, when delete record(s)
    void vertexDeleted();

public slots:

    // internal slots
    void openRecordKey(  const string& key, bool resetInOutQuery = false  );
    void openRecordKey(  const QModelIndex& index );
    void changeKeyList()
       { keysModel->changeKeyList( &curRecord ); }

    void objectChanged()
       { contentsChanged = true; }

    // View
    void CmFollowInEdges();
    void CmFollowOutEdges();

   // Record
    //void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();
    void CmImportFormat();
    void CmExportFormat();
    void CmDeleteSelect();
    void CmDisplaySearchResult();
    void CmSearchQuery();
    void CmBackupQueriedRecordstoFile();
    void CmRestoreMultipleRecordsfromFile();
    void CmBackupGraphtoFile();
    void CmRestoreGraphfromFile();

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();

public:

    explicit VertexWidget( const string& aschemaName, QWidget *parent = 0);
    ~VertexWidget();

    /// Update query
    void setQuery( QueryWidget* queryW  );

private:

    Ui::VertexWidget *ui;

    // tree view editor
    QStringList aHeaderData;
    TSchemaNodeModel* model_schema;
    QItemDelegate *deleg_schema;
    TBsonView* fieldTable;

    DBKeysModel *keysModel; ///< Keys list model
    TKeyTable *keysTable;  ///< Keys list table

};

} // namespace bsonui

#endif // VERTEXWIDGET_H
