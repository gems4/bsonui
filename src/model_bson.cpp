//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_bson.cpp
/// Implementation of TBsonModel, TBsonDelegate and TBsonView
/// implements a tree view of bson structure
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QKeyEvent>
#include <QComboBox>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QApplication>
#include <QClipboard>
#include <QInputDialog>
#include <QMessageBox>
#include "model_bson.h"
#include "SelectDialog.h"
#include "CalcDialog.h"
#include "ModelLineDialog.h"
#ifdef addBSONIO
#include "ar2base.h"
#else
#include "bsonio/ar2base.h"
#endif
using namespace bsonio;

namespace bsonui {

bool TBsonView::expandedFields = true;

/// Internal structure to save expanded fields
class ExpandedLevelLine
{
public:

    QString keyname;           ///< Name of field
    ExpandedLevelLine *parent;
    vector<unique_ptr<ExpandedLevelLine>> children;

    ExpandedLevelLine( const QString& akey, ExpandedLevelLine* parentline ):
        keyname( akey), parent( parentline )
    {
       if( parent)
            parent->children.push_back( unique_ptr<ExpandedLevelLine>(this) );
    }

    ~ExpandedLevelLine()
    {   children.clear();   }

    ExpandedLevelLine* findKey( const QString& akey )
    {
        auto it = children.begin();
        while( it != children.end() )
        {
          if( it->get()->keyname == akey)
           return it->get();
          it++;
        }
        return nullptr;
    }
};

BsonLine::BsonLine( int andx, const QString& akey, int atype, const QString& avalue):
  ndx( andx), keyname( akey), type( atype ), value( avalue)
{
    parent = 0;
}

BsonLine::BsonLine( const BsonLine* data ):
  ndx( data->ndx), keyname( data->keyname), type( data->type ), value( data->value)
{
    parent = data->parent;

    // copy childrens
    for(uint ii=0; ii<data->children.size(); ii++ )
    {
        BsonLine *line = new BsonLine( data->children[ii].get() );
        line->parent = this;
        children.push_back( unique_ptr<BsonLine>(line) );
    }
}

BsonLine::~BsonLine()
{
    children.clear();
    // qDeleteAll(children);
}

void bson_to_list( const char *data, int datatype, BsonLine* parent );

void bson_to_list( const char *data, int /*datatype*/, BsonLine* parent )
{
    bson_iterator i;
    QString key;
    QString value;
    int ndx=0;

    bson_iterator_from_buffer(&i, data);
    while (bson_iterator_next(&i))
    {
       bson_type t = bson_iterator_type(&i);
       if (t == 0)
          break;

       key = bson_iterator_key(&i);

       switch (t)
       {
         // impotant datatypes
         case BSON_NULL:
              value = "null";
             break;
         case BSON_BOOL:
              value =( bson_iterator_bool(&i) ? "true" : "false");
              break;
         case BSON_INT:
              value = QString( "%1").arg( bson_iterator_int(&i));
              break;
         case BSON_LONG:
              value = QString( "%1").arg( (uint64_t) bson_iterator_long(&i));
              break;
         case BSON_DOUBLE:
              //value = QString( "%1").arg( bson_iterator_double(&i));
              value.setNum( bson_iterator_double(&i), 'g', 10 );
              break;
         case BSON_STRING:
              value = QString( "%1").arg( bson_iterator_string(&i));
              break;
         case BSON_OID:
           { char oidhex[25];
             bson_oid_to_string(bson_iterator_oid(&i), oidhex);
             value = oidhex;
            }
            break;
         // main constructions
         case BSON_OBJECT:
         case BSON_ARRAY:
              value = "";
              break;
         default:
              bsonioErr("bson_to_list" ,"can't print type " );
       }

       BsonLine *line = new BsonLine(ndx++, key, t , value );
       line->parent = parent;
       parent->children.push_back( unique_ptr<BsonLine>(line) );

       switch (t)
       {
          // main constructions
          case BSON_OBJECT:
                // !! next level
               bson_to_list( bson_iterator_value(&i), BSON_OBJECT, line );
                 break;
          case BSON_ARRAY:
               // !! next level
               bson_to_list( bson_iterator_value(&i), BSON_ARRAY, line );
          default:     break;
       }
    }
}

void list_to_bson(BsonLine *line, bson* bsonobj );

void list_to_bson_full(BsonLine *headline, bson* bsonobj )
{
    bson_destroy(bsonobj );
    bson_init( bsonobj );
    for(uint ii=0; ii< headline->children.size(); ii++ )
      list_to_bson( headline->children[ii].get(),  bsonobj );
    bson_finish( bsonobj );
}

void list_to_bson(BsonLine *line, bson* bsonobj )
{
    uint ii;
    switch (line->type)
    {
      // impotant datatypes
      case BSON_NULL:
           bson_append_null(bsonobj, line->keyname.toUtf8().data());
           break;
      case BSON_BOOL:
           bson_append_bool(bsonobj, line->keyname.toUtf8().data(),
              ( line->value == "true"? true : false));
           break;
      case BSON_INT:
      case BSON_LONG:
      case BSON_DOUBLE:
           bson_append_double(bsonobj, line->keyname.toUtf8().data(), line->value.toDouble() );
           break;
      case BSON_OID:
         // oid
         if( !line->value.isEmpty()&& line->value != emptiness.c_str() )
         {  // oid
           bson_oid_t oid;
           bson_oid_from_string( &oid, line->value.toUtf8().data() );
           bson_append_oid( bsonobj, JDBIDKEYNAME, &oid);
         }
          break;
      case BSON_STRING:
          bson_append_string(bsonobj, line->keyname.toUtf8().data(), line->value.toUtf8().data() );
           break;
      // main constructions
      case BSON_OBJECT:
           bson_append_start_object(bsonobj, line->keyname.toUtf8().data());
           for( ii=0; ii< line->children.size(); ii++ )
             list_to_bson( line->children[ii].get(),  bsonobj );
           bson_append_finish_object( bsonobj );
           break;
    case BSON_ARRAY:
        bson_append_start_array(bsonobj, line->keyname.toUtf8().data());
        for( ii=0; ii< line->children.size(); ii++ )
          list_to_bson( line->children[ii].get(),  bsonobj );
        bson_append_finish_array( bsonobj );
        break;
      default:
           bsonioErr("list_to_bson" ,"can't print type " );
    }

}


//--------------------------------------------------------------------------------------
//  class TBsonModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//  Reading/writing data from/to TObject and TObjList classes
//---------------------------------------------------------------------------------------
TBsonModel::TBsonModel( bson *abson, const QStringList& aHeaderData, QObject* parent ):
        TBsonAbstractModel(parent), hdData( aHeaderData)
{
  rootNode = 0;
  setupModelData(abson, "");
  
  connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
  			parent,  SLOT(objectChanged()) );	 
}

TBsonModel::~TBsonModel()
{
  if(rootNode )
     delete rootNode;
}

void TBsonModel::saveToBson()
{
  list_to_bson_full( rootNode, bsonData );
}

BsonLine *TBsonModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<BsonLine *>(index.internalPointer());
    } else {
        return rootNode;
    }
}


void TBsonModel::setupModelData( bson *abson, const string&  )
{
    bsonData = abson;

    beginResetModel();
    if( rootNode )
      delete rootNode;

    rootNode = new BsonLine(0, "start", BSON_OBJECT , "" );
    bson_to_list( bsonData->data, BSON_OBJECT, rootNode );
    endResetModel();
}

QModelIndex TBsonModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    BsonLine *parentItem = lineFromIndex( parent );
    if(parentItem->children.size() > 0 )
       return createIndex(row, column, parentItem->children[row].get());
    else
       return QModelIndex();
}

QModelIndex TBsonModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();
 
    BsonLine *childItem = lineFromIndex(child);
    BsonLine *parentItem = childItem->parent;
    if (parentItem == rootNode )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}


int TBsonModel::rowCount( const QModelIndex& parent ) const
{
   if (!rootNode)
       return 0;
  if (parent.column() > 0)
      return 0;
  BsonLine *parentItem = lineFromIndex( parent );
  return parentItem->children.size();
}	

int TBsonModel::columnCount( const QModelIndex& /*parent*/ ) const
{
  return 2;
}	

Qt::ItemFlags TBsonModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  BsonLine *item = lineFromIndex( index );
  if( index.column() == 1 && (item->type != BSON_OBJECT && item->type != BSON_ARRAY ) )
   {  flags |= Qt::ItemIsEditable;
             return flags;
   }
  else
     return (flags & ~Qt::ItemIsEditable);
}

QVariant TBsonModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
 if( role == Qt::DisplayRole  && orientation == Qt::Horizontal )
	   return hdData[section];
  return QVariant();
}

QString TBsonModel::getDescription( const QModelIndex& index ) const
{
    BsonLine *item = lineFromIndex( index );
    bool ok;
    item->keyname.toInt(&ok,10);
    if( ok && item->parent->type ==  BSON_ARRAY )
      item = item->parent;

    return item->keyname; // may be string from io2format array
}

QVariant TBsonModel::data( const QModelIndex& index, int role ) const
{
   if(!index.isValid())
	 return QVariant();	

   switch( role )
   { case Qt::DisplayRole:
     case Qt::EditRole:
            {  BsonLine *item = lineFromIndex( index );
               if( index.column()== 0 )
                   return item->keyname;
               else
                   if( index.column()== 1 )
                       return item->value;
                   //else
                   //    return getDescription( index );
           }
      case Qt::ToolTipRole:
      case Qt::StatusTipRole:
              return  getDescription(index );
      case Qt::ForegroundRole:
       if ( index.column() == 0 )
               {
                 return QVariant( QColor( Qt::darkCyan ) );
               }
       //return QVariant( QColor( Qt::black ) );
      default: break;
   }

   return QVariant();
}

bool TBsonModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
	if( index.isValid() && ( role == Qt::EditRole ) )
	{
      if(  index.column()== 1)
		  {	 
             BsonLine *line =lineFromIndex(index);
             line->value = QVariant(value).toString();
             list_to_bson_full( rootNode, bsonData );
		  }	 
	 return true;
	} 
	return false;
}

string TBsonModel::helpName( const QModelIndex& index ) const
{
    BsonLine *line =  lineFromIndex(index);
    bool ok;
    line->keyname.toInt(&ok,10);
    if( ok && line->parent->type ==  BSON_ARRAY )
      line = line->parent;

   return line->keyname.toUtf8().data();
}

bool TBsonModel::isNumber( const QModelIndex& index ) const
{
    BsonLine *line =  lineFromIndex(index);
    return (line->type == BSON_INT || line->type == BSON_LONG ||
                line->type == BSON_DOUBLE );
}

bool TBsonModel::isArray( const QModelIndex& index ) const
{
    BsonLine *line =  lineFromIndex(index);
    return ( line->type == BSON_ARRAY );
}

void TBsonModel::resizeArray( QWidget* parentWidget, const QModelIndex& index )
{
    BsonLine *line =  lineFromIndex(index);

    if( line->children.size() > 0  )
    { bool ok = 0;
      uint size = QInputDialog::getInt( parentWidget, "Please, select new array size",
         "Array size ", line->children.size(), 0, 999, 1, &ok );
      if(!ok) // cancel command
          return;

      // resizeline item
      if( size == line->children.size() )
          return;

      // delete if smaler
      if( size < line->children.size() )
      {
       beginRemoveRows( index, size, line->children.size()-size);
       line->children.resize(size);
       endRemoveRows();
       list_to_bson_full( rootNode, bsonData );
       return;
      }

     // add new elements
     beginInsertRows( index, line->children.size(), size-1);
     BsonLine *linechild;
     for( uint ii=line->children.size(); ii<size; ii++)
     {
        QString key_ = QString( "%1").arg( ii );
        linechild = new BsonLine( line->children.back().get() );
        linechild->ndx = ii;
        linechild->keyname = key_;
        linechild->parent = line;
        line->children.push_back( unique_ptr<BsonLine>(linechild) );
     }
     endInsertRows();
     list_to_bson_full( rootNode, bsonData );
     return;
    }

    // Alloc free array
    ModelLineDialog dlg(index, index, line->keyname, parentWidget);
    if( !dlg.exec() )
       return;
    QString new_value = dlg.getDefValue();
    int new_type = dlg.getType();
    uint size = dlg.getArraySizes()[0];
    beginInsertRows( index, 0, size-1);
    BsonLine *linechild;
    for( uint ii=0; ii<size; ii++)
    {
       QString key_ = QString( "%1").arg( ii );
       linechild = new BsonLine(ii, key_, new_type, new_value );
       linechild->parent = line;
       line->children.push_back( unique_ptr<BsonLine>(linechild) );
    }
    endInsertRows();
    list_to_bson_full( rootNode, bsonData );
}

bool TBsonModel::isStruct( const QModelIndex& index ) const
{
    BsonLine *line =  lineFromIndex(index);
    BsonLine *parentItem = line->parent;
    return ( parentItem->type == BSON_OBJECT ||
      (line->type== BSON_OBJECT && line->children.size() == 0 ) );
}

bool TBsonModel::isCanBeRemoved( const QModelIndex& index ) const
{
    BsonLine *line =  lineFromIndex(index);
    BsonLine *parentItem = line->parent;
    return ( parentItem->type == BSON_OBJECT );
}

void TBsonModel::delObject( QWidget* parentWidget, const QModelIndex& index )
{
    BsonLine *line =  lineFromIndex(index);
    QString msg = QString("Confirm deletion of %1 data object?").arg(line->keyname );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question( parentWidget->window(), "Data object to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    beginRemoveRows( parent(index), index.row(), index.row());
    // resizeline item
    BsonLine *parent_ = line->parent;
    parent_->children.erase( parent_->children.begin() + index.row() );
    for(uint ii=index.row(); ii<parent_->children.size(); ii++ )
      parent_->children[ii].get()->ndx--;
    endRemoveRows();

   list_to_bson_full( rootNode, bsonData );
}

const QModelIndex TBsonModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    return addObject( parentWidget, cindex );

    /*BsonLine *line =  lineFromIndex(cindex);

    bool ok;
    // Select line name
    QString new_object = QInputDialog::getText(parentWidget,
        tr("Please, input new object name"), tr("New object Name:"),
         QLineEdit::Normal, tr("new_object"), &ok);
    if(!ok) // cancel command
      return cindex;

    // define object type
    QString new_value;
    int new_type;
    if( !getNewLineType( parentWidget, new_type, new_value))
     return cindex;

    // empty object
    if(line->type== BSON_OBJECT && line->children.size() == 0 )
    {
        beginInsertRows( cindex, 0, 0);
        BsonLine *childrenline = new BsonLine(0, new_object, new_type, new_value );
        childrenline->parent = line;
        line->children.push_back( unique_ptr<BsonLine>(childrenline) );
        endInsertRows();
        list_to_bson_full( rootNode, bsonData );
        return sibling( cindex.row(), 0, cindex );
    }
    else
    {
        BsonLine *parent_ = line->parent;
        beginInsertRows( parent(cindex), cindex.row(), cindex.row());
        BsonLine *childrenline = new BsonLine(cindex.row(), new_object, new_type, new_value );
        childrenline->parent = parent_;
        parent_->children.insert( parent_->children.begin()+cindex.row(),
                                  unique_ptr<BsonLine>(childrenline) );
        for(uint ii=cindex.row()+1; ii<parent_->children.size(); ii++ )
          parent_->children[ii].get()->ndx++;
        endInsertRows();
        list_to_bson_full( rootNode, bsonData );
        return index( cindex.row(), 0, parent(cindex) );
   }
   */
}

const QModelIndex TBsonModel::addObject( QWidget * parentWidget, const QModelIndex& cindex )
{
    QModelIndex parentIndex;
    BsonLine *line =  lineFromIndex(cindex);
    if(line->type== BSON_OBJECT && line->children.size() == 0 )
        parentIndex = cindex;
    else
        parentIndex = parent(cindex);

    ModelLineDialog dlg(cindex, parentIndex, "", parentWidget);
    if( !dlg.exec() )
       return cindex;

    // Select line name
    QString new_object = dlg.getName();
    // define object type
    int new_type = dlg.getType();
    QString new_value = dlg.getDefValue();

    // empty object
    if(line->type== BSON_OBJECT && line->children.size() == 0 )
    {
        beginInsertRows( cindex, 0, 0);
        BsonLine *childrenline = new BsonLine(0, new_object, new_type, new_value );
        childrenline->parent = line;
        line->children.push_back( unique_ptr<BsonLine>(childrenline) );
        endInsertRows();
    }
    else
    {
        BsonLine *parent_ = line->parent;
        beginInsertRows( parent(cindex), cindex.row(), cindex.row());
        BsonLine *childrenline = new BsonLine(cindex.row(), new_object, new_type, new_value );
        childrenline->parent = parent_;
        parent_->children.insert( parent_->children.begin()+cindex.row(),
                                  unique_ptr<BsonLine>(childrenline) );
        for(uint ii=cindex.row()+1; ii<parent_->children.size(); ii++ )
          parent_->children[ii].get()->ndx++;
        endInsertRows();
   }
   list_to_bson_full( rootNode, bsonData );
   return cindex;
}

string TBsonModel::getFieldPath( const QModelIndex& index )
{
    BsonLine *line =  lineFromIndex(index);
    string data_ = line->keyname.toUtf8().data();
    line = line->parent;
    while( line->parent )
    {
      data_ =  string(line->keyname.toUtf8().data())+"."+data_;
      line = line->parent;
    }
    return data_;
 }

string TBsonModel::getFieldData( const QModelIndex& index )
{
    BsonLine *line =  lineFromIndex(index);

    string data_ = line->value.toUtf8().data();
    if(  line->type == BSON_OBJECT || line->type == BSON_ARRAY )
    {
        bson bobj;
        bson_init( &bobj );
        for(uint ii=0; ii< line->children.size(); ii++ )
            list_to_bson( line->children[ii].get(),  &bobj );
        bson_finish( &bobj );
        if( line->type == BSON_OBJECT )
          jsonFromBson( bobj.data, data_ );
        else
            jsonArrayFromBson( bobj.data, data_ );
        bson_destroy( &bobj );
    }
    return data_;
}

void  TBsonModel::setFieldData( const QModelIndex& index, const string& data_)
{
    BsonLine *line =  lineFromIndex(index);
    bson bobj;

    try {
          beginResetModel();

          line->children.clear();

          auto pos = data_.find_first_of("[{" );
          if( pos == string::npos )
             line->value = data_.c_str();
          else
           {   line->value = "";
               if(  data_[pos] == '{' )
               {
                   line->type = BSON_OBJECT;
                   jsonToBson( &bobj, data_ );
                   bson_to_list( bobj.data, BSON_OBJECT, line );
               }
               else
               {
                   line->type = BSON_ARRAY;
                   jsonToBsonArray( &bobj, data_ );
                   bson_to_list( bobj.data, BSON_ARRAY, line );
                }
               bson_destroy(&bobj);
          }
           endResetModel();
    } catch( ... )
      {
         endResetModel();
         throw;
       }

 }

//-------------------------------------------------------------------------------------
// TBsonDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TBsonDelegate::TBsonDelegate( QObject * parent ):
	QItemDelegate( parent )
{ }

// Editing QTreeView for objects in System page
QWidget *TBsonDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    BsonLine *line =  ((TBsonModel *)(index.model() ))->lineFromIndex(index);
    if( index.column() > 0 )
    {
        switch( line->type  )
        {
        case BSON_BOOL:
                { QComboBox *accessComboBox = new QComboBox(parent);
                  accessComboBox->addItem(tr("false"));
                  accessComboBox->addItem(tr("true"));
                  return accessComboBox;
                 }
        case BSON_INT:
        case BSON_LONG:
        case BSON_DOUBLE:
               { QLineEdit *lineEdit = new QLineEdit(parent);
                 lineEdit->setValidator(new QDoubleValidator(lineEdit));
                 //lineEdit->setValidator(new QDoubleValidator(-999.0,
                 //            999.0, 2, lineEdit));
                 return lineEdit;
               }
        case BSON_NULL:
        case BSON_STRING:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
              }
        // main constructions
       case BSON_OBJECT:
       case BSON_ARRAY:
       default:
             bsonioErr("TBsonDelegate" ,"can't print type " );

       }
    }	
   return QAbstractItemDelegate::createEditor( parent, option,  index );
}


//-------------------------------------------------------------------------------------
// class TBsonView implements a tree view that displays items from a model to bson.
//-------------------------------------------------------------------------------------

TBsonView::TBsonView( QWidget * parent ):
     QTreeView( parent )
{
     setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
#if QT_VERSION >= 0x050000
     header()->setSectionsClickable(true);
     header()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );
#else
     header()->setResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );
#endif
	    setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
	    setFocusPolicy(Qt::StrongFocus);
	    setTabKeyNavigation( false );
        setContextMenuPolicy(Qt::CustomContextMenu);
        setSelectionMode( QAbstractItemView::/*MultiSelection*/ExtendedSelection );
        setSelectionBehavior( QAbstractItemView::SelectItems );
        setIndentation( 30 );

        connect( this, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(slotPopupContextMenu(QPoint)));
        connect( header(), SIGNAL(sectionClicked(int)),
             this, SLOT(changeCurrent(int)));
}

/*virtual void TBsonView::drawRow(QPainter* p_painter, const QStyleOptionViewItem& option,
                     const QModelIndex& index) const
{
    QStyleOptionViewItem optionCustom = option;
    if (index.column() == 0)
    {
        optionCustom.palette.setColor(QPalette::Text, Qt::green);
    }
    QTreeView::drawRow(p_painter, optionCustom, index);
 }*/

void TBsonView::changeCurrent( int section )
{
    if( !currentIndex().isValid() )
    { QModelIndex index = model()->index( 0, section, rootIndex());
      setCurrentIndex(index);
    }
}

 void TBsonView::slotPopupContextMenu(const QPoint &pos)
 {
    QModelIndex index = indexAt( pos );
    const TBsonAbstractModel* model =dynamic_cast<const TBsonAbstractModel*>(index.model());
    QMenu *menu = new QMenu(this);
  
     QAction* act =  new QAction(tr("&Help"), this);
     act->setIcon(QIcon(":/menu/ShowHelpWindowIcon.png"));
     act->setShortcut(tr("F1"));
     act->setStatusTip(tr("Help for specified cell"));
     connect(act, SIGNAL(triggered()), this, SLOT(CmHelp()));
     menu->addAction(act);

     menu->addSeparator();

     if(  model && model->isStruct(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Add Structured Data Fields ..."), this);
            act->setShortcut(tr("F7"));
            act->setStatusTip(tr("Add new line(s) into object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmAddObjects()));
            menu->addAction(act);
         }

     if(  model && model->isStruct(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Add &One Data Field ..."), this);
            act->setShortcut(tr("Ctr+O"));
            act->setStatusTip(tr("Add new line into object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmAddObject()));
            menu->addAction(act);
         }

     if( model && model->isCanBeRemoved(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Remove Selected Data Field"), this);
            act->setShortcut(tr("F8"));
            act->setStatusTip(tr("Delete current line from object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmDelObject()));
            menu->addAction(act);
         }
     if( model && model->isUnion(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Remove Alternatives (&Union)"), this);
            act->setShortcut(tr("Ctr+U"));
            act->setStatusTip(tr("Delete other of current line from object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmDelObjectsUnion()));
            menu->addAction(act);
         }

     if(  model && model->isArray(index) )
         {
            menu->addSeparator();
            act =  new QAction(tr("Resize Selected List/Map/Set ..."), this);
            act->setShortcut(tr("F4"));
            act->setStatusTip(tr("Change current array size"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmResizeArray()));
            menu->addAction(act);
         }

     if( model && model->isStruct(index) )
         {
            //menu->addSeparator();
            act =  new QAction(tr("Reset Data Object to &Defaults"), this);
            act->setShortcut(tr("Ctrl+D"));
            act->setStatusTip(tr("Delete current line from object"));
            connect(act, SIGNAL(triggered()), this, SLOT(CmResetObject()));
            menu->addAction(act);
         }

     if( index.column()==1 && model && model->isNumber(index) )
         {
            menu->addSeparator();
            act =  new QAction(tr("Calculator"), this);
            act->setShortcut(tr("F5"));
            act->setStatusTip(tr("Use Calculator for specified cells"));
       	    connect(act, SIGNAL(triggered()), this, SLOT(CmCalc()));
            menu->addAction(act);
         }

      /*menu->addSeparator();
         
      act =  new QAction(tr("Select &row"), this);
      act->setShortcut(tr("Ctrl+R"));
      act->setStatusTip(tr("Select current row"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectRow()));
      menu->addAction(act);
 
      act =  new QAction(tr("Select co&lumn"), this);
      act->setShortcut(tr("Ctrl+L"));
      act->setStatusTip(tr("Select current column"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectColumn()));
      menu->addAction(act);
       
      act =  new QAction(tr("Select &group"), this);
      act->setShortcut(tr("Ctrl+G"));
      act->setStatusTip(tr("Select current item with children"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectGroup()));
      menu->addAction(act);

      act =  new QAction(tr("Select &all"), this);
      act->setShortcut(tr("Ctrl+A"));
      act->setStatusTip(tr("Select all"));
      connect(act, SIGNAL(triggered()), this, SLOT(SelectAll()));
      menu->addAction(act);

      menu->addSeparator();

      act =  new QAction(tr("&Copy"), this);
      act->setShortcut(tr("Ctrl+C"));
      act->setStatusTip(tr("Copy selected data"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyData()));
      menu->addAction(act);

      act =  new QAction(tr("Copy &with names"), this);
      act->setShortcut(tr("Ctrl+W"));
      act->setStatusTip(tr("Copy selected header&cells"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyDataHeader()));
      menu->addAction(act);
      */
      menu->addSeparator();

      act =  new QAction(tr("Copy Field &Path"), this);
      act->setShortcut(tr("Ctrl+P"));
      act->setStatusTip(tr("Copy selected field path"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyFieldPath()));
      menu->addAction(act);

      act =  new QAction(tr("Copy &Field"), this);
      act->setShortcut(tr("Ctrl+F"));
      act->setStatusTip(tr("Copy selected field data"));
      connect(act, SIGNAL(triggered()), this, SLOT(CopyField()));
      menu->addAction(act);

      act =  new QAction(tr("Paste Field"), this);
      //act->setShortcut(tr("Ctrl+W"));
      act->setStatusTip(tr("Paste data to field"));
      connect(act, SIGNAL(triggered()), this, SLOT(PasteField()));
      menu->addAction(act);

     menu->exec( viewport()->mapToGlobal(pos) );
     delete menu;
 }

  void TBsonView::keyPressEvent(QKeyEvent* e)
 {
	    if ( e->modifiers() & Qt::ControlModifier ) 
	    {
			switch ( e->key() ) 
			{
              /*case Qt::Key_R:
			    SelectRow();
			    return;
			  case Qt::Key_L:
			    SelectColumn();
			    return;
			  case Qt::Key_G:
			    SelectGroup();
			    return;
			  case Qt::Key_A:
			    SelectAll();
			    return;
			  case Qt::Key_C:
			    CopyData();
			    return;
             case Qt::Key_W:
                CopyDataHeader();
                return;*/
            case Qt::Key_U:
               CmDelObjectsUnion();
               return;
            case Qt::Key_O:
                CmAddObject();
               return;
            case Qt::Key_D:
                CmResetObject();
               return;
            case Qt::Key_P:
                CopyFieldPath();
               return;
            case Qt::Key_F:
                CopyField();
               return;
            }
		}

      switch( e->key() )
 	  {
 	    case Qt::Key_F1:
 	        CmHelp();
 	        return;
        case Qt::Key_F5:
           CmCalc();
          return;
        case Qt::Key_F7:
           CmAddObjects();
          return;
        case Qt::Key_F8:
           CmDelObject();
          return;
       case Qt::Key_F4:
           CmResizeArray();
          return;
    }
 	QTreeView::keyPressEvent(e);
 }

  bool TBsonView::testCurrentIndex( )
  {
    if( currentIndex().isValid() )
     return  true;
   QMessageBox::information( window(), "View object",
        "Please, determine the current index" );
   return false;
  }

  void TBsonView::_SelectRow()
  {
    if( testCurrentIndex() )
      selectionModel()->select( currentIndex(),
            QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect );
  }

  void TBsonView::_SelectColumn()
  {
    if( testCurrentIndex() )
      selectionModel()->select( currentIndex(),
              QItemSelectionModel::Columns|QItemSelectionModel::ClearAndSelect );
  }

  void TBsonView::_SelectAll()
  {
    if( testCurrentIndex() )
     selectWithChildren(rootIndex());
     //   selectAll();
  }

  void TBsonView::selectWithChildren(const QModelIndex& parIndex)
  {
     selectionModel()->select( parIndex,
             QItemSelectionModel::Rows|QItemSelectionModel::Select );

     for(int row = 0; row < model()->rowCount( parIndex ); row++ )
     {
        QModelIndex  childIndex = model()->index( row, 0, parIndex);
        selectWithChildren(childIndex);
     }
 }

  void TBsonView::_SelectGroup()
  {
     if( !testCurrentIndex() )
        return;

     selectWithChildren(currentIndex());
  }


  void TBsonView::copyWithChildren( const QModelIndex& parIndex,
      const QModelIndexList& selIndexes, QString& clipText )
  {
      QModelIndex index;
      QString cText;
      bool frst = true;
      int row = parIndex.row(),  col;

      for (col = 0; col < model()->columnCount( parIndex ); col++ )
      {
          index = parIndex.sibling( row, col);
          if( selIndexes.contains( index ) )
          {
            if( !frst )
               clipText += splitCol;
             frst = false;
             cText = model()->data(index).toString();
             if( cText == emptiness.c_str() )
                   cText = "  ";//"\r";
             clipText += cText;
          }
     }
     if( !frst )
       clipText += splitRow;

     for(int rw = 0; rw < model()->rowCount( parIndex ); rw++ )
     {
        index = model()->index( rw, 0, parIndex);
        copyWithChildren( index, selIndexes, clipText );
     }
 }

  QString TBsonView::createString()
  {
    QString clipText;
    QModelIndexList selIndexes = selectedIndexes();
    copyWithChildren( rootIndex(),selIndexes, clipText );
	return clipText;  
  }

  QString TBsonView::createHeader()
  {
    QString cText;
	QString clipText;
    int col;
    bool frst = true;
    for( col = 0; col < model()->columnCount( rootIndex() ); col++ ) 
	{
	    if( selectionModel()->columnIntersectsSelection( col,  rootIndex() ) )
       	{
   		  if( !frst )
            clipText += splitCol;
   		  frst = false;
   		  cText = model()->headerData( col, Qt::Horizontal, Qt::DisplayRole ).toString();
   		  if( cText == emptiness.c_str() )
   			  cText = "  ";//"\r"; 
   	      clipText += cText;
	    }
	 }  
     if( !frst )
        clipText += splitRow;
	return clipText;  
  }

  void TBsonView::_CopyData()
  {
 	QString clipText = createString();
    QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
  }

  void TBsonView::_CopyDataHeader()
  {
 	QString clipText = createHeader();
	clipText += createString();
    QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
  }

  void TBsonView::CmCalc()
  {
    QModelIndex index = currentIndex();
    TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());
    if( index.column()==1 && model && model->isNumber(index) )
    {
       CalcDialog calc( window() );
       if( calc.exec() )
       {
            QString res = calc.computeFunctionString( index.data(Qt::EditRole).toDouble() );
            model->setData(index, res, Qt::EditRole);
       }
    }
   }

  void TBsonView::CmAddObjects()
  {
    QModelIndex index = currentIndex();
    TBsonAbstractModel* model_;
    if (index.isValid())
        model_ =(TBsonAbstractModel*)(index.model());
    else
       model_ =(TBsonAbstractModel*)(model());

    try {
        if(  model_ && model_->isStruct(index) )
        {  const QModelIndex ndx = model_->addObjects( window(), index);
           selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
       else
        QMessageBox::information( window(), "Add to Object",
             "Please, determine object to be expanded" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
  }


  void TBsonView::CmAddObject()
  {
    QModelIndex index = currentIndex();
    //TBsonAbstractModel* model_ =  (TBsonAbstractModel*)(index.model());
    TBsonAbstractModel* model_ =  (TBsonAbstractModel*)(model());

    try {
         if(  model_ && model_->isStruct(index) )
         {
             QModelIndex ndx = model_->addObject( window(), index);
             selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
         }
        else
           QMessageBox::information( window(), "Add to Object",
               "Please, determine object to be expanded" );
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
   }


  void TBsonView::CmResetObject()
  {
    QModelIndex index = currentIndex();
    //TBsonAbstractModel* model_ =  (TBsonAbstractModel*)(index.model());
    TBsonAbstractModel* model_ =  (TBsonAbstractModel*)(model());

    try {
         if(  model_ && model_->isStruct(index) )
         {   // collapse(index);
             model_->resetObject( window(), index);
             // selectionModel()->setCurrentIndex( index, QItemSelectionModel::ClearAndSelect );
         }
        else
           QMessageBox::information( window(), "Reset Object",
               "Please, determine object to be expanded" );
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( window(), e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( window(), "std::exception", e.what() );
    }
   }

  void TBsonView::CmDelObject()
  {
    QModelIndex index = currentIndex();
    TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());
    try {
          if(  model && model->isCanBeRemoved(index) )
          {
              selectionModel()->clear();
              collapse(index);
              model->delObject( window(), index);
      //update();
         } else
          QMessageBox::information( window(), "Delete from Object",
             "Please, determine the key-value pair to be deleted" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
     }
  }

  void TBsonView::CmDelObjectsUnion()
  {
    QModelIndex index = currentIndex();
    TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());
    try {
          if(  model && model->isUnion(index) )
          {
              collapse(index);
              model->delObjectsUnion( window(), index);
      //update();
         } else
          QMessageBox::information( window(), "Delete from Object",
             "Please, determine the key-value pair to be deleted" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
    catch(...)
     {
        QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
     }
  }

  void TBsonView::CmResizeArray()
  {
    QModelIndex index = currentIndex();
    TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());
    try {
        if(  model && model->isArray(index) )
       {
            // collapse(index);
            model->resizeArray( window(), index);
       }
       else
          QMessageBox::information( window(), "Resize Array",
             "Please, determine arrays key to be resized" );
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( window(), e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( window(), "std::exception", e.what() );
     }
   }

  // Help on F1 pressed on data field
 void TBsonView::CmHelp()
    {
      QModelIndex index = currentIndex();
      const TBsonAbstractModel* model =dynamic_cast<const TBsonAbstractModel*>(index.model());
      string hlp= "";
      if(model)
         hlp =  model->helpName( index );
      std::cout << hlp.c_str() << endl;
      //pVisorImp->OpenHelp( 0, hlp.c_str() );
    }

// Save expand level  -----------------------------------------


void TBsonView::updateModelData( bson *abson, const string& curSchemaName )
{
    TBsonAbstractModel* model_table =(TBsonAbstractModel*)( model() );
    if( !model_table )
      return;

    // save old expand level
    int sec0size = 250, sec1size = 250;
    ExpandedLevelLine *topList = 0;
    if( expandedFields )
    {
      sec0size = header()->sectionSize(0);
      sec1size = header()->sectionSize(1);
      topList = new ExpandedLevelLine("root", 0);
      saveExpandedState( topList );
    }

    // update model
    model_table->setupModelData(abson, curSchemaName );

    // restore expand level
    if( expandedFields )
      restoreExpandedState( topList );
    else
       expandToDepth(0);
    header()->resizeSection(0, sec0size);
    header()->resizeSection(1, sec1size);
    if( topList )
      delete topList;

}

void TBsonView::saveExpandedState( ExpandedLevelLine *topList )
{
    for(int row = 0; row < model()->rowCount(); ++row)
        saveExpandedOnLevel( model()->index(row,0), topList);
}

void TBsonView::restoreExpandedState( ExpandedLevelLine *topList )
{
    setUpdatesEnabled(false);
    for(int row = 0; row < model()->rowCount(); ++row)
        restoreExpandedOnLevel( model()->index(row,0), topList );
    setUpdatesEnabled(true);
}

void TBsonView::saveExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *parent )
{
    if( index.isValid() && isExpanded(index))
    {
        QString key = index.data(Qt::EditRole).toString();
        ExpandedLevelLine *child = new ExpandedLevelLine(key, parent);
        for(int row = 0; row < model()->rowCount(index); ++row)
            saveExpandedOnLevel(index.child(row,0), child );
    }
}

void TBsonView::restoreExpandedOnLevel(const QModelIndex& index, ExpandedLevelLine *topData )
{
    if( index.isValid() )
    {
      QString key = index.data(Qt::EditRole).toString();
      ExpandedLevelLine *child = topData->findKey(key);
      if(child != nullptr )
      {
        setExpanded(index, true);
        for(int row = 0; row < model()->rowCount(index); ++row)
            restoreExpandedOnLevel(index.child(row,0), child );
      }
   }
}

void TBsonView::CopyFieldPath()
{
  QModelIndex index = currentIndex();
  TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());
  try {
        if(  model )
        {
            QString clipText = model->getFieldPath( index ).c_str();
            QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
        }
  }
  catch(bsonio_exeption& e)
  {
      QMessageBox::critical( window(), e.title(), e.what() );
  }
  catch(std::exception& e)
   {
      QMessageBox::critical( window(), "std::exception", e.what() );
   }
  catch(...)
   {
      QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
   }
}


void TBsonView::CopyField()
{
  QModelIndex index = currentIndex();
  TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());
  try {
        if(  model )
        {
            QString clipText = model->getFieldData( index ).c_str();
            QApplication::clipboard()->setText(clipText/*, QClipboard::Clipboard*/);
        }
  }
  catch(bsonio_exeption& e)
  {
      QMessageBox::critical( window(), e.title(), e.what() );
  }
  catch(std::exception& e)
   {
      QMessageBox::critical( window(), "std::exception", e.what() );
   }
  catch(...)
   {
      QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
   }
}

void TBsonView::PasteField()
{
  string clipText = QApplication::clipboard()->text(QClipboard::Clipboard).toUtf8().data();
  ExpandedLevelLine *topList = 0;

  if(clipText.empty())
    return;
  QModelIndex index = currentIndex();
  TBsonAbstractModel* model =(TBsonAbstractModel*)(index.model());

  try {
        if(  model )
        {

            // save old expand level
            int sec0size = 250, sec1size = 250;
            sec0size = header()->sectionSize(0);
            sec1size = header()->sectionSize(1);
            topList = new ExpandedLevelLine("root", 0);
            saveExpandedState( topList );

            model->setFieldData( index, clipText );

            restoreExpandedState( topList );
            header()->resizeSection(0, sec0size);
            header()->resizeSection(1, sec1size);
        }
  }
  catch(bsonio_exeption& e)
  {
      QMessageBox::critical( window(), e.title(), e.what() );
  }
  catch(std::exception& e)
   {
      QMessageBox::critical( window(), "std::exception", e.what() );
   }
  catch(...)
   {
      QMessageBox::critical( window(), "std::exception", "Undefined exeption" );
   }
  if( topList )
    delete topList;
}

} // namespace bsonui

//---------------------- End of file  model_bson.cpp ---------------------------
