//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file VertexMenu.cpp
/// Implementation of class VertexWidget - Widget to work with vertex
/// internal DOM based on our JSON schemas data
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <QMessageBox>
#include <QLineEdit>
#include "VertexWidget.h"
#include "ui_VertexWidget.h"
#include "SelectDialog.h"
#include "FormatImpexWidget.h"
#ifdef addBSONIO
#include "json2cfg.h"
#include "traversal.h"
#else
#include "bsonio/json2cfg.h"
#include "bsonio/traversal.h"
#endif
using namespace bsonio;

namespace bsonui {

//  Connect all actions
void VertexWidget::setActions()
{
    // File
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->action_Clone_Structured_Data_Object , SIGNAL( triggered()), this, SLOT(CmClone()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));

    // Edit
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
    connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
    connect(ui->actionReset_Data_to_Defaults, SIGNAL(triggered()), fieldTable, SLOT(CmResetObject()));
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionCopy_Field_Path, SIGNAL(triggered()), fieldTable, SLOT(CopyFieldPath()));
    connect(ui->actionCopy_Field, SIGNAL(triggered()), fieldTable, SLOT(CopyField()));
    connect(ui->actionPaste_Field, SIGNAL(triggered()), fieldTable, SLOT(PasteField()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->actionKeep_Data_Fields_Expanded, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmEditExpanded(bool)));
    updateViewMenu();
    connect( ui->actionFollow_Edges, SIGNAL( triggered()), this, SLOT(CmFollowOutEdges()));
    connect( ui->actionFollow_Edges_Incoming, SIGNAL( triggered()), this, SLOT(CmFollowInEdges()));

    // Record
    connect( ui->actionNext_Record, SIGNAL( triggered()), keysTable, SLOT(CmNext()));
    connect( ui->actionPrevious_Record, SIGNAL( triggered()), keysTable, SLOT(CmPrevious()));
    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    //connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));
    connect( ui->actionImport, SIGNAL( triggered()), this, SLOT(CmImportFormat()));
    connect( ui->actionExport, SIGNAL( triggered()), this, SLOT(CmExportFormat()));
    connect( ui->actionDeleteMultiple, SIGNAL( triggered()), this, SLOT(CmDeleteSelect()));
    connect( ui->actionSearch_Results, SIGNAL( triggered()), this, SLOT(CmDisplaySearchResult()));
    connect( ui->actionSearch, SIGNAL( triggered()), this, SLOT(CmSearchQuery()));

    connect( ui->actionBackup_Queried_Records_to_File, SIGNAL( triggered()),
               this,  SLOT(CmBackupQueriedRecordstoFile()));
    connect( ui->actionRestore_Multiple_Records_from_File, SIGNAL( triggered()),
               this, SLOT(CmRestoreMultipleRecordsfromFile()));

    connect( ui->actionBackup_Graph_to_File, SIGNAL( triggered()),
               this,  SLOT(CmBackupGraphtoFile()));
    connect( ui->actionRestore_Graph_from_File, SIGNAL( triggered()),
               this, SLOT(CmRestoreGraphfromFile()));

    QLineEdit* pLineTask = new QLineEdit( ui->nameToolBar );
    pLineTask->setEnabled( true );
    pLineTask->setFocusPolicy( Qt::ClickFocus );
    pLineTask->setReadOnly( true );
    pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  curSchemaName.c_str();
    pLineTask->setText(title);
    ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    ui->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
}

// Menu commands -----------------------------------------------------------


/// Set default bson record
void VertexWidget::CmNew()
{
  try{
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          resetBson("");
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Clone bson record (clear _id)
void VertexWidget::CmClone()
{
  try{
        string key ="";
        model_schema->setValue("_id",key);
        fieldTable->hide();
        fieldTable->show();
   }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/* Save current record to DB file as new
void VertexWidget::CmCreateInsert()
{
    try
    {
        if( dbclient.get() == 0 )
          return;
        string stroid = genOid();
        TSchemaNodeModel*model = dynamic_cast<TSchemaNodeModel*>(model_schema);
        if( model)
          model->setOid(stroid);
        string recBsonText;
        jsonFromBson( curRecord.data, recBsonText );
        dbclient->SetJsonYaml( recBsonText,  true );
        dbclient->InsertRecord( );
        contentsChanged = false;
        changeKeyList();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}
*/

/// Read new record from DB
void VertexWidget::CmRead()
{
    try
    {
        if( dbgraph.get() == 0 )
          return;

        // Select keys to load
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        dbgraph->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to read/view", aValList, 0,
                             TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
         if( !selDlg.exec() )
          return;

        string reckey = aKeyList[selDlg.selIndex()];
        // Read Record
        openRecordKey(  reckey  );
        contentsChanged = false;
        changeKeyList();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void VertexWidget::CmUpdate()
{
    try
    {
       if( dbgraph.get() == 0 )
          return;

       //string recBsonText;
       // get current key
       string key = dbgraph->getKeyFromBson( curRecord.data );
       if( !dbgraph->Find( key.c_str() ))
       {
           string stroid = dbgraph->genOid();
           model_schema->setOid(stroid);
           //jsonFromBson( curRecord.data, recBsonText );
           //dbclient->SetJsonYaml( recBsonText,  true );
           dbgraph->SetBson(&curRecord);
           dbgraph->InsertRecord();
           fieldTable->hide();
           fieldTable->show();
       }
       else
       {
           //jsonFromBson( curRecord.data, recBsonText );
           //dbclient->SetJsonYaml( recBsonText,  true );
           dbgraph->SetBson(&curRecord);
           dbgraph->SaveRecord( key.c_str() );
       }

        contentsChanged = false;
        changeKeyList();
        // ??? might be error if changes into key
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void VertexWidget::CmDelete()
{
    try
    {
        if( dbgraph.get() == 0 )
          return;
        // get current key
        string key = dbgraph->getKeyFromBson( curRecord.data );
        dbgraph->DelRecord( key.c_str() );
        contentsChanged = false;
        changeKeyList();
        emit vertexDeleted();

    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Delete records from List
void VertexWidget::CmDeleteSelect()
{
    try
    {
        if( dbgraph.get() == 0 )
          return;

        // Select keys to delete
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        vector<int> selNdx;
        dbgraph->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to delete", aValList, selNdx );
         if( !selDlg.exec() )
             return;

        selNdx =  selDlg.allSelected();
        for( uint ii=0; ii<selNdx.size(); ii++ )
        { string key = aKeyList[selNdx[ii]];
          dbgraph->DelRecord( key.c_str() );
        }
        changeKeyList();
        emit vertexDeleted();

    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void VertexWidget::CmImportJSON()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with JSON object", "*.json"  ))
       {
          FJson file( fileName);
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          if( !resetBson(schemafromName( fileName )) )
          {
            model_schema->saveToBson();
            bsonioErr( fileName , "Try to read another schema format file" );
          }
          contentsChanged = true;
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void VertexWidget::CmExportJSON()
{
   try {
         string fileName = fileShemaExt( curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write the data", "*.json", fileName  ))
         {
            FJson file( fileName);
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson records from format file fileName
void VertexWidget::CmImportFormat()
{
   try{
        // Select format&input file
       FormatImpexWidget* dlg = new FormatImpexWidget(
                   FormatImpexWidget::runModeImport, curSchemaName, this );

       ExecuteFunction execF = [=]( AbstractIEFile *inputIEFile,
                                   const string& dataFile, bool overwrite )
       {
          if( !inputIEFile )
            return; // only for defined types

          // select main schema
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          if( !resetBson(inputIEFile->getDataName()) )
          {
            model_schema->saveToBson();
            bsonioErr( inputIEFile->getDataName() , "Try to read another schema format file" );
          }

          // test access to data base to save
          if( dbgraph.get() == 0 )
           return;

         // open file with imported data
         inputIEFile->openFile( dataFile );
         try{
             while( inputIEFile->readBlock( &curRecord) )
                 dbgraph->SaveBson( &curRecord, overwrite, true );
            }
         catch(...)
         {
             changeKeyList();
             throw;
         }

         inputIEFile->closeFile();
         changeKeyList();
       };

       dlg->setExecuteFunction(execF);
       dlg->show();

    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to format file fileName
void VertexWidget::CmExportFormat()
{
   try {

        if( dbgraph.get() == 0 )
          return;

        // Select keys to export
        vector<string> aKeyList;
        vector<vector<string>> aValList;
        vector<int> selNdx;
        dbgraph->GetKeyValueList( aKeyList, aValList );
        if( aKeyList.empty() )
          return;

        SelectDialog selDlg( this, "Please, select a record to export", aValList, selNdx );
        if( !selDlg.exec() )
             return;
        selNdx =  selDlg.allSelected();

         // Select format&export file
        FormatImpexWidget* dlg = new FormatImpexWidget(
                    FormatImpexWidget::runModeExport, curSchemaName, this );


        ExecuteFunction execF = [=]( AbstractIEFile *inputIEFile,
                                    const string& dataFile, bool /*overwrite*/ )
        {
           if( !inputIEFile )
             return; // only for defined types

           // select main schema
           bson_destroy( &curRecord );
           bson_init( &curRecord );
           bson_finish( &curRecord );
           if( !resetBson(inputIEFile->getDataName()) )
           {
             model_schema->saveToBson();
             bsonioErr( inputIEFile->getDataName() , "Try to write another schema format file" );
           }

           // open file to export data
          inputIEFile->openFile( dataFile );  /// open to app

          for( uint ii=0; ii<selNdx.size(); ii++ )
          {
              string key = aKeyList[selNdx[ii]];
              dbgraph->GetRecord( key.c_str() );
              bson obj;
              dbgraph->GetBson(&obj);
              inputIEFile->writeBlock( &obj );
              bson_destroy(&obj);
          }

          inputIEFile->closeFile();
        };

        dlg->setExecuteFunction(execF);
        dlg->show();



    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void VertexWidget::CmDisplaySearchResult()
{
  try
  {
    keysModel->showResult("Vertex Query Result window");
  }
  catch(std::exception& e)
  {
        QMessageBox::critical( this, "CmDisplaySearchResult", e.what() );
  }
}

void VertexWidget::CmSearchQuery()
{
  try
  {
      vector<string> schemalst;
      schemalst.push_back(curSchemaName);
      keysModel->showQuery( "Vertex Query Widget", schemalst );
  }
  catch(std::exception& e)
  {
      QMessageBox::critical( this, "CmSearchQuery", e.what() );
  }
}


void VertexWidget::setQuery( QueryWidget* queryW  )
{
  keysModel->setOneQuery(queryW);
  ui->queryEdit->setText(dbgraph->getQuery().getEJDBQuery().c_str());
}

/// Read multiple records from file fileName
void VertexWidget::CmRestoreMultipleRecordsfromFile()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with json object", "*.json"  ))
       {
            bool overwrite = ioSettings().overwrite();
            FJsonArray file( fileName);
            file.Open( OpenModeTypes::ReadOnly );
            while( file.LoadNext( &curRecord ) )
               dbgraph->SaveBson( &curRecord, overwrite, false );
            file.Close();
        }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
   resetBson( "" );
   contentsChanged = false;
   changeKeyList();
}

/// Write multiple records to file fileName
void VertexWidget::CmBackupQueriedRecordstoFile()
{
    if( dbgraph.get() == 0 )
         return;

    try {
         string fileName = fileShemaExt(curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write (document) records", "*.json", fileName  ))
         {
            FJsonArray file( fileName);

            // Select keys to delete
            vector<string> aKeyList;
            vector<vector<string>> aValList;
            vector<int> selNdx;
            dbgraph->GetKeyValueList( aKeyList, aValList );
            if( aKeyList.empty() )
              return;

            SelectDialog selDlg( this, "Please, select a record to backup", aValList, selNdx,
                                 TMatrixTable::tbNoMenu|TMatrixTable::tbSort );
             if( !selDlg.exec() )
                 return;

            selNdx =  selDlg.allSelected();
            file.Open( OpenModeTypes::WriteOnly );

            for( uint ii=0; ii<selNdx.size(); ii++ )
            {
                string key = aKeyList[selNdx[ii]];
                dbgraph->GetRecord( key.c_str() );
                //string valDB = dbclient->GetJson();
                bson obj;
                //jsonToBson( &obj, valDB );
                dbgraph->GetBson(&obj);
                file.SaveNext(&obj);
                bson_destroy(&obj);
            }
            file.Close();
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Show connect Edjes window
void VertexWidget::CmFollowInEdges()
{
    if( dbgraph.get() == 0 )
         return;
    try
    {
        string _id;
        model_schema->getValue("_id",_id);

        shared_ptr<TDBEdgeDocument> edges( documentAllEdges( uiSettings().database() ));
        string queryJson = edges->inEdgesQuery( _id );
        if( !edges->existKeysByQuery( queryJson ) )
        {
          QMessageBox::StandardButton reply;
          reply = QMessageBox::question( this, "Follow Incoming Edges",
                "No Incoming Edges. Create new?",
                 QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::No)
           return;
        }

        showOtherWindow( false, "", "",  queryJson);
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow edges", e.what() );
    }
}


/// Show connect Edjes window
void VertexWidget::CmFollowOutEdges()
{
    if( dbgraph.get() == 0 )
         return;
    try
    {
        string _id;
        model_schema->getValue("_id",_id);

        shared_ptr<TDBEdgeDocument> edges( documentAllEdges( uiSettings().database() ));
        string queryJson = edges->outEdgesQuery( _id );
        if( !edges->existKeysByQuery( queryJson ) )
        {
          QMessageBox::StandardButton reply;
          reply = QMessageBox::question( this, "Follow Outgoing Edges",
                "No Outgoing Edges. Create new?",
                 QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::No)
           return;
        }
        showOtherWindow( false, "", "",  queryJson);
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow edges", e.what() );
    }
}

/// Read multiple records from file fileName
void VertexWidget::CmRestoreGraphfromFile()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select a file with json object", "*.json"  ))
        {
            GraphTraversal travel( uiSettings().database() );
            travel.RestoreGraphFromFile(fileName, ioSettings().overwrite() );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
   resetBson( "" );
   contentsChanged = false;
   updateDB();
}

/// Write multiple records to file fileName
void VertexWidget::CmBackupGraphtoFile()
{
    if( dbgraph.get() == 0 )
       return;

    FJsonArray* file = 0;

    try {
         string fileName = fileShemaExt(curSchemaName, "json");
         if(  ChooseFileSave( this, fileName,
                     "Please, select a file to write (document) records", "*.json", fileName  ))
         {
            file = new FJsonArray( fileName);

            // Select keys to delete
            vector<string> aKeyList;
            vector<vector<string>> aValList;
            vector<int> selNdx;
            dbgraph->GetKeyValueList( aKeyList, aValList );
            if( aKeyList.empty() )
              return;

            SelectDialog selDlg( this, "Please, select a record to backup", aValList, selNdx );
             if( !selDlg.exec() )
                 return;

            selNdx =  selDlg.allSelected();
            vector<string> idList;
            for( auto const &ent : selNdx)
            {
                string key = aKeyList[ent];
                strip_all( key, ":" );
                idList.push_back(key);
            }

            GraphTraversal travel( uiSettings().database() );

            file->Open( OpenModeTypes::WriteOnly );

            GraphElementFunction afunc =  [file]( bool , bson *data )
                    {
                       file->SaveNext(data);
                    };


            travel.Traversal( true, idList, afunc );
            file->Close();
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
    delete file;
}

} // namespace bsonui

// end of VertexMenu.cpp
