//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file HelpMainWindow.cpp
/// Implementation of classes HelpMainWindow, HtmlGenerator - Help system main dialog
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QLabel>
#include <QMessageBox>
#include <QtWebEngineWidgets>
#include <QDesktopServices>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#include <QFileInfo>

#include "HelpMainWindow.h"
#include "ui_HelpMainWindow.h"
#include "helpgenerator.h"
#include "helpdata.h"

const char *_GEMS_version_stamp = " GEMS-GUI v.3.3 rc.2599 ";
extern const char *_GEMIPM_version_stamp;

void helpWin( const string& name, const string& item )
{
   if( HelpMainWindow::pDia )
   {
     HelpMainWindow::pDia->showHelp( name, item );
     HelpMainWindow::pDia->show();
     HelpMainWindow::pDia->raise();
   }
}


void HtmlGenerator::run()
{
   generator->copyImageRecordsToFileStructure(".");
   forever{

       QString text;

       // end processing?
       if ( stop )
         return;

        {
            // wait for new task
            QMutexLocker locker(&tasksMutex);
            while (tasks.count() == 0)
            {
                // end processing
                if ( stop )
                  return;
                bufferNotEmpty.wait(&tasksMutex);
            }

            // get last task from queue and skip all previous tasks
            while (!tasks.isEmpty())
                text = tasks.dequeue();
        }


        {
            QMutexLocker locker(&genMutex);
            generator->setMarkdownText(text);
            // generate HTML from markdown
            generateHtmlFromMarkdown();
            // generate table of contents
            generateTableOfContents();
        }
    }
}


bool PreviewPage::acceptNavigationRequest(const QUrl &url,
                                          QWebEnginePage::NavigationType /*type*/,
                                          bool /*isMainFrame*/)
{
    if( url.isLocalFile() )
    {
        emit newLink( url );
        return false;
    }
    // Only allow qrc:/index.html.
    if (url.scheme() == QString("qrc"))
        return true;
    QDesktopServices::openUrl(url);
    return false;
}

//-------------------------------------------------------

HelpMainWindow* HelpMainWindow::pDia = 0;


HelpMainWindow::HelpMainWindow(  QWidget* parent):
        QMainWindow( parent ), ui(new Ui::HelpWindowData),
        helpgenerator( new HelpGenerator()),
        htmlgenerator(0)

{
   htmlgenerator = new HtmlGenerator(helpgenerator, this);
   ui->setupUi(this);
   ui->indexWidget->hide();
   PreviewPage *pageHelp = new PreviewPage(this);
   ui->helpView->setPage(pageHelp);
   //PreviewPage *pageToc = new PreviewPage(this);
   //ui->tocView->setPage(pageToc);


   string titl = " Help Viewer ";
   setWindowTitle( trUtf8(titl.c_str()) );
   pDia = this;

 //  ui->splitter->setStretchFactor(0, 1);
   ui->splitter->setStretchFactor(1, 1);
   ui->splitter->setStretchFactor(2, 1);

   htmlgenerator->setCodeHighlightingStyle("default");
//-- ui->helpView->page()->settings()->setUserStyleSheetUrl(QUrl("qrc:/styles/default.css"));

   // inform us when a link in the table of contents or preview view is clicked
//--   ui->helpView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
//--   ui->tocView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
//   connect( ui->tocView->page(), SIGNAL( newLink(QUrl) ), this, SLOT(tocLinkClicked(QUrl)));
   connect( ui->helpView->page(), SIGNAL( newLink(QUrl) ), this, SLOT(helpLinkClicked(QUrl)));
///??   connect( ui->tocView, SIGNAL( anchorClicked(QUrl) ), this, SLOT(tocLinkClicked(QUrl)));


   resetDBClient();
   setActions();
   setupHtmlPreview();
}

void HelpMainWindow::setActions()
{
    connect( ui->actionHome, SIGNAL( triggered()), this, SLOT( actionHome()) );
    connect( ui->actionBack, SIGNAL( triggered()), this, SLOT( actionBack()));
    connect( ui->actionForward, SIGNAL( triggered()), this, SLOT( actionForward()));
    connect( this, SIGNAL( forwardAvailable(bool) ), ui->actionForward, SLOT(setEnabled(bool)));
    connect( this, SIGNAL( backwardAvailable(bool)), ui->actionBack, SLOT(setEnabled(bool)));
    ui->actionForward->setEnabled(false);
    ui->actionBack->setEnabled(false);

    connect( ui->action_Find, SIGNAL( triggered()), this, SLOT(actionFind()));
    connect( ui->actionFind_Next, SIGNAL( triggered()), this, SLOT(actionFindNext()));
    connect( ui->actionFind_Previous, SIGNAL( triggered()), this, SLOT(actionFindPrevious()));

    connect( ui->actionZoom_In, SIGNAL( triggered()), this, SLOT(actionZoomIn()));
    connect( ui->actionZoom_Out, SIGNAL( triggered()), this, SLOT(actionZoomOut()));

    connect( ui->action_About, SIGNAL( triggered()), this, SLOT(helpAbout()));
    connect( ui->actionVersion, SIGNAL( triggered()), this, SLOT(helpVersion()));
    connect( ui->actionHelp_on_Help, SIGNAL( triggered()), this, SLOT(helpOnHelp()));
    connect( ui->action_Print, SIGNAL( triggered()), this, SLOT(helpPrint()));

    QLabel *label_2 = new QLabel(ui->toolFind);
    label_2->setText(trUtf8("Find:"));
    ui->toolFind->addWidget( label_2 );

    findLine = new QLineEdit( ui->toolFind );
    findLine->setEnabled( true );
    findLine->setFocusPolicy( Qt::ClickFocus );
    ui->toolFind->addWidget( findLine );
    ui->toolFind->addAction(ui->actionFind_Previous);
    ui->toolFind->addAction(ui->actionFind_Next);

#ifndef _RELEASE
    QLabel *label = new QLabel(ui->toolAddress);
    label->setText(trUtf8("Address:"));
    ui->toolAddress->addWidget( label );

    adressLine = new QLineEdit( ui->toolAddress );
    adressLine->setEnabled( true );
    adressLine->setFocusPolicy( Qt::ClickFocus );
    adressLine->setReadOnly( true );
    adressLine->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    ui->toolAddress->addWidget( adressLine );
#endif

}

HelpMainWindow::~HelpMainWindow()
{
    // stop background HTML preview generator
    htmlgenerator->Stop();
    htmlgenerator->wait();
    delete htmlgenerator;
    delete ui;
}

void HelpMainWindow::setupHtmlPreview()
{
    // start background HTML preview generator
    connect(htmlgenerator, SIGNAL(htmlResultReady(QString)),
            this, SLOT(htmlResultReady(QString)));
    connect(htmlgenerator, SIGNAL(tocResultReady(QString)),
            this, SLOT(tocResultReady(QString)));
    htmlgenerator->start();
}


void HelpMainWindow::showAddres( const QString& name )
{
#ifndef _RELEASE
   adressLine->setText( name );
#endif
}

void HelpMainWindow::showHelp(const string& name, const string& item )
{
   cout << name << "  " << item << endl;
   string keywd = name+":md:0:";
   anchorItem = item.c_str();
   currentKeyChange( keywd );
}

void HelpMainWindow::showDocument(const char* keywd )
{
   cout << keywd << endl;
   anchorItem = "";
   currentKeyChange( keywd );
}

// Slots ----------------------------------------------

void HelpMainWindow::htmlResultReady(const QString &html)
{
    //cout << "html " << endl << html.toStdString() << endl;

    // show html preview
    QUrl baseUrl = QUrl::fromLocalFile(qApp->applicationDirPath() + "/");
    //    baseUrl = QUrl::fromLocalFile(QFileInfo("docpages/start.md").absolutePath() + "/");
    if( ui->helpView->isVisible() )
    {
        ui->helpView->setHtml( html, baseUrl );
        if( !anchorItem.isEmpty()  )
         ; ///??   ui->helpView->page()->scrollToAnchor(anchorItem);
        // QWebEngineView()->page()->runJavaScript(QString("window.scroll(0,findPos(document.getElementById("%1")));").arg(scrollX).arg(anchor));
    }
    ui->tocView->setHtml(html);
}

void HelpMainWindow::tocResultReady(const QString& toc)
{
   cout << "toc =" << toc.toStdString() << endl;
   ///?? ui->tocView->setHtml(toc);
}

void HelpMainWindow::tocLinkClicked(const QUrl& url)
{
    QString anchor = url.toString();
    anchor = anchor.remove("#");
     //-- ui->helpView->page()->scrollToAnchor(anchor);
    QString javascript = QString("window.scroll(0,findPos(document.getElementById(\"%1\")));").arg(anchor);
    cout << "url = " << url.toString().toStdString() << "script =" << javascript.toStdString() << endl;
    //ui->helpView->page()->runJavaScript( javascript );
}

void HelpMainWindow::helpLinkClicked(const QUrl& url)
{
   currentKeyChange( keyFromUrl(url.url()) );
}

/*void HelpMainWindow::helpLinkClicked(const QUrl& url)
{
    if( url.isLocalFile() )
    {
        currentKeyChange( keyFromUrl(url.url()) );
        return;
    }
    // extern links
    addLink( History::ExternLink, url.toString().toStdString() );
    ui->helpView->load( url);
    ui->tocView->setHtml("");
    //QDesktopServices::openUrl(url);
}*/

void HelpMainWindow::helpVersion()
{
    QMessageBox::information(this,
#ifdef __unix
#ifdef __APPLE__
           trUtf8("Title"), trUtf8("GEMS4.2 (MacOS X >10.6 64 clang)\n\n")+
#else
           trUtf8("GEMS4.2 (Linux 32/64 gcc4.7 Qt5)"),
#endif
#else
           trUtf8("GEMS4.2 (Windows 7 MinGW 32 gcc4.7"),
#endif
           trUtf8("\nThis is GEM-Selektor code package\n\n")+
         //  trUtf8( _GEMS_version_stamp ) + trUtf8(  "\n\nusing " )+
         //  trUtf8( _GEMIPM_version_stamp ) +
           trUtf8( "\n\n\nFor GEMS R&D community\n\n"
                  "(c) 2013, GEMS Development Team\n\n"
                  "          PSI-UH-ETHZ" ) );
}

void HelpMainWindow::helpAbout()
{
    showHelp( "start", "" );
    //showDocument( "GEMS_ABOUT_HTML", 0 );
}

void HelpMainWindow::helpOnHelp()
{
   showHelp( "README", "Build.ThermoHelp" );
   //showDocument( "GEMS_HOWHELP_HTML", 0 );
}

void HelpMainWindow::helpPrint()
{
  QPrinter printer;

  QPrintDialog dlg(  &printer, this );
  dlg.setWindowTitle(tr("Print Help Page"));
  if (ui->helpView->hasSelection() )
      dlg.addEnabledOption(QAbstractPrintDialog::PrintSelection);
  if( dlg.exec() )
   ;/// !!! ui->helpView->page()->print( &printer, [](const QVariant &result){ qDebug() << result; } ) ;
}

void HelpMainWindow::actionZoomIn()
{
   ui->helpView->setZoomFactor(ui->helpView->zoomFactor()*1.1);
//   ui->tocView->setZoomFactor(ui->tocView->zoomFactor()*1.1);
   ui->tocView->zoomIn();
}

void HelpMainWindow::actionZoomOut()
{
  ui->helpView->setZoomFactor(ui->helpView->zoomFactor()*0.9);
  //ui->tocView->setZoomFactor(ui->tocView->zoomFactor()*0.9);
  ui->tocView->zoomOut();
}

void HelpMainWindow::actionFind()
{
  actionFindNext();
}

void HelpMainWindow::actionFindNext()
{
  if( !findLine )
   return;

  QWebEnginePage::FindFlags flg = 0;
  if(ui->action_Case_sensetiv->isChecked() )
       flg |=QWebEnginePage::FindCaseSensitively;

  //--if(ui->action_words_beginnings->isChecked() )
  //--    ; flg |=QWebEnginePage::FindAtWordBeginningsOnly;

  ui->helpView->findText( findLine->text(), flg );
}

void HelpMainWindow::actionFindPrevious()
{
  if( !findLine )
   return;

  QWebEnginePage::FindFlags flg = QWebEnginePage::FindBackward;
  if(ui->action_Case_sensetiv->isChecked() )
       flg |=QWebEnginePage::FindCaseSensitively;

  //-- if(ui->action_words_beginnings->isChecked() )
      //-- flg |=QWebEnginePage::FindAtWordBeginningsOnly;

  ui->helpView->findText( findLine->text(), flg );
}

void HelpMainWindow::actionHome()
{
   showHelp( "start", "" );
}

void HelpMainWindow::actionBack()
{
  if( history.canGoBack() )
  {
    history.back();
    changeLink();
  }
}

void HelpMainWindow::actionForward()
{
    if( history.canGoForward() )
    {
      history.forward();
      changeLink();
    }
}

//------------------------------------------------


void HelpMainWindow::generateHtml( const string& key )
{
    // generate HTML from record
    htmlgenerator->markdownKeyChange(key);
}

void HelpMainWindow::currentKeyChange( const string& key )
{
    generateHtml( key );
    addLink( History::DBRecord, key );
}

void HelpMainWindow::addLink( int atype, const string& alink )
{
    history.addLink( atype, alink );
    showAddres( alink.c_str() );
    emit backwardAvailable( history.canGoBack());
    emit forwardAvailable( history.canGoForward() );
}

void HelpMainWindow::changeLink()
{
    ui->tocView->setHtml("");
    switch( history.type() )
    {
    case History::DBRecord:
        generateHtml( history.link() );
        break;
    case History::ExternLink:
        ui->helpView->load( QUrl(history.link().c_str()) );
        break;
    }
  showAddres( history.link().c_str() );
  emit forwardAvailable( history.canGoForward() );
  emit backwardAvailable( history.canGoBack() );
}

void HelpMainWindow::resetDBClient()
{
   history.clear();
   helpgenerator->resetDBClient();
}

string HelpMainWindow::keyFromUrl( const QString& urllink )
{
    anchorItem = urllink.section('#', 1, 1);
    QString  filePath= urllink.section('#', 0, 0);
    DocPagesData data;
    data.setFileInfo( filePath, false );
    return data.recordKey();
}

//---------------------------------------------

void HtmlGenerator::markdownKeyChange( const string &key )
{
    string mdtext;
    {
        QMutexLocker locker(&genMutex);
        mdtext = generator->readRecord(key);
    }

    {
     QMutexLocker locker(&tasksMutex);
     tasks.enqueue(mdtext.c_str());
     bufferNotEmpty.wakeOne();
    }
}

void HtmlGenerator::setCodeHighlightingStyle(const QString &style)
{
  QMutexLocker locker(&genMutex);
  generator->setCodeHighlightingStyle(style);
}

void HtmlGenerator::Stop()
{
  QMutexLocker locker(&tasksMutex);
  stop = 1;
  tasks.enqueue("");
  bufferNotEmpty.wakeOne();
}

void HtmlGenerator::generateHtmlFromMarkdown()
{
   const QString html = generator->generateHtmlFromMarkdown();
   emit htmlResultReady(html);
}

void HtmlGenerator::generateTableOfContents()
{
  QString toc = generator->generateTableOfContents();
   emit tocResultReady(toc);
}


//-------- End of file HelpMainWindow.cpp ----------------------------


