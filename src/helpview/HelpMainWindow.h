//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file HelpMainWindow.h
/// Declaration of classes HelpMainWindow, HtmlGenerator - Help system main dialog
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef HelpWindow_included
#define HelpWindow_included

#include <vector>
#include <string>
#include <memory>
#include <QMainWindow>
#include <QWebEngineView>
#include <QSettings>
#include <QLineEdit>
#include <QtCore/qthread.h>
#include <QtCore/qqueue.h>
#include <QtCore/qmutex.h>
#include <QtCore/qwaitcondition.h>
using namespace std;

// http://doc.qt.io/qt-5/qtwebenginewidgets-qtwebkitportingguide.html

namespace Ui {
class HelpWindowData;
}

void helpWin( const string& name, const string& item );

class HelpGenerator;
class HtmlGenerator;

class History
{
  vector<string> links;
  vector<int> types;
  int current = -1;

public:

  enum _types { DBRecord, ExternLink, Undefined };

  bool canGoBack() const
  {
    return current>0;
  }

  bool canGoForward() const
  {
    return current < ((int)links.size()-1);
  }

  void back()
  {
    if( canGoBack() )
     current--;
  }

  void forward()
  {
    if( canGoForward() )
     current++;
  }

  int type() const
  {
    if( current >= 0 )
     return types[current];
    return Undefined;
  }

  string link() const
  {
    if( current  >= 0 )
     return links[current];
    return "";
  }

  void addLink( int atype, const string& alink )
  {
    types.resize(current+1);
    links.resize(current+1);
    types.push_back(atype);
    links.push_back(alink);
    current++;
  }

  void clear()
  {
    types.clear();
    links.clear();
    current = -1;
  }

};

class PreviewPage : public QWebEnginePage
{
    Q_OBJECT

signals:
    void newLink(const QUrl& url);

public:
    explicit PreviewPage(QObject *parent = nullptr) : QWebEnginePage(parent) {}

protected:
    bool acceptNavigationRequest(const QUrl &url, NavigationType type, bool isMainFrame);
};

class HelpMainWindow : public QMainWindow
{
    Q_OBJECT

    // internal functions
    void setActions();
    void setupHtmlPreview();

    void currentKeyChange( const string& key );
    void generateHtml( const string& key );
    void addLink( int atype, const string& alink );
    void changeLink();
    string keyFromUrl( const QString& urllink );

signals:
    void forwardAvailable(bool);
    void backwardAvailable(bool);

public slots:

    void resetDBClient();

protected slots:

    void showAddres( const QString& name );
    void htmlResultReady(const QString& html);
    void tocResultReady(const QString& toc);
    void tocLinkClicked(const QUrl& url);
    void helpLinkClicked(const QUrl& url);

    void helpPrint();

    void actionHome();
    void actionBack();
    void actionForward();

    void actionFind();
    void actionFindNext();
    void actionFindPrevious();

    void actionZoomIn();
    void actionZoomOut();

    void helpAbout();
    void helpVersion();
    void helpOnHelp();

public:

    static HelpMainWindow* pDia;

    HelpMainWindow( QWidget* parent = NULL );
    virtual ~HelpMainWindow();

    void showDocument( const char* keywd );
    void showHelp( const string& name, const string& item );

protected:

     Ui::HelpWindowData *ui;
     QLineEdit *adressLine;
     QLineEdit *findLine;

     std::shared_ptr<HelpGenerator> helpgenerator;
     HtmlGenerator* htmlgenerator;
     QString anchorItem = "";
     History history;
};


class HtmlGenerator : public QThread
{
    Q_OBJECT

    std::shared_ptr<HelpGenerator> generator;
    volatile int stop;


public:
    explicit HtmlGenerator( const std::shared_ptr<HelpGenerator>& agenerator, QObject *parent ):
        QThread( parent ),
        generator( agenerator), stop(0)
    {}


public slots:
    void markdownKeyChange( const string &key );
    void setCodeHighlightingStyle(const QString &style);
    void Stop();

signals:
    void htmlResultReady(const QString& html);
    void tocResultReady(const QString& toc);

protected:
    virtual void run();

private:
    void generateHtmlFromMarkdown();
    void generateTableOfContents();

private:

    QQueue<QString> tasks;
    QMutex tasksMutex;
    QMutex genMutex;
    QWaitCondition bufferNotEmpty;

};


#endif // HelpWindow_included

