//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file helpgenerator.h
/// Declarations of HelpGenerator class is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef HELPGENERATOR_H
#define HELPGENERATOR_H

#include <QString>
#include <string>
#ifdef addBSONIO
#include "dbschemadoc.h"
#else
#include "bsonio/dbschemadoc.h"
#endif
class HelpGeneratorPrivate;

// patch -p 1 < cutemarked.patch

/// Allocate Help DB Schema
bsonio::TDBSchemaDocument* newDBHelpClient( const string& queryString  );

/// \class HelpGenerator is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
class HelpGenerator
{
public:

#ifndef ENABLE_HOEDOWN
    enum MarkdownConvert { CDiscountMarkdownConverter, CRevealMarkdownConverter };
#else
    enum MarkdownConvert { CDiscountMarkdownConverter, CHoedownMarkdownConverter, CRevealMarkdownConverter };
#endif

    explicit HelpGenerator( );
    ~HelpGenerator();

    // markdown functions -----------------------
    void setHelpRecord( const std::string& key );
    void setMarkdownText(const QString &text);
    QString generateHtmlFromMarkdown();
    QString generateTableOfContents();
    void setCodeHighlightingStyle(const QString &style);
    void setMarkdownConverter( MarkdownConvert aconverter );
    QString exportHtml(const QString &styleSheet, const QString &highlightingScript, const QString &highlightStyle);

    // work with DB ------------------------------------
    string readRecord( const string& key );
    void saveRecordToFile( const string& key, const QString& helpDir );
    void copyImageRecordsToFileStructure( const QString& dirPath );

    /// Reset Database
    void resetDBClient();

private:

    /// internal help data
    std::shared_ptr<bsonio::TDBSchemaDocument> dbhelp;
    QString docPagesImagePath = "/images";

    MarkdownConvert markdownConverter = CDiscountMarkdownConverter;
    std::unique_ptr<HelpGeneratorPrivate> pdata;
    QString task;

    void clearDir( const QString& dirPath );

};


#endif // HELPGENERATOR_H
