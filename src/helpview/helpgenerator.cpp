//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file helpgenerator.cpp
/// Implementation of HelpGenerator class is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QSettings>
#include <QDir>
#include "helpgenerator.h"
#include "helpdata.h"
#include "preferences.h"
using namespace bsonio;

//#define ENABLE_MARKDOWN

TDBSchemaDocument* newDBHelpClient(  const string& queryString  )
{

    vector<KeyFldsData> _keyFldsInf = { KeyFldsData( "name", "name", "File name" ),
                                        KeyFldsData( "ext", "ext", "File extension" ),
                                        KeyFldsData( "type", "type", "File type" )};

    TDBSchemaDocument* newClient =  TDBSchemaDocument::newDBSchemaDocument(
                bsonui::uiSettings().database(),  "DocPages", "docpages",  _keyFldsInf, queryString );
    return newClient;
}

#ifdef ENABLE_MARKDOWN
#include <converter/markdownconverter.h>
#include <template/template.h>
#include <converter/markdowndocument.h>
#include <converter/discountmarkdownconverter.h>
#include <converter/revealmarkdownconverter.h>
#ifdef ENABLE_HOEDOWN
#include <converter/hoedownmarkdownconverter.h>
#endif

class HelpGeneratorPrivate
{
    MarkdownConverter::ConverterOptions converterOptions() const;
    Template::RenderOptions renderOptions() const;

public:

    explicit HelpGeneratorPrivate( ):
        document(0), converter(0)
    { }

    ~HelpGeneratorPrivate()
    {
        delete document;
        delete converter;
    }

    // main functions
    void setMarkdownConverter( HelpGenerator::MarkdownConvert aconverter );

    QString generateHtmlFromMarkdown()
    {
        if( !document )
            return QString();
        QString html = converter->templateRenderer()->render(converter->renderAsHtml(document), renderOptions());
        return html;
    }

    QString generateTableOfContents()
    {
        if( !document )
            return QString();
        QString toc = converter->renderAsTableOfContents(document);
        QString styledToc = QString("<html><head>\n<style type=\"text/css\">ul { list-style-type: none; padding: 0; margin-left: 1em; } a { text-decoration: none; }</style>\n</head><body>%1</body></html>").arg(toc);
        return styledToc;
    }

    void setMarkdownText(const QString &text)
    {
        //if( !text.isEmpty() )
        {
            delete document;
            // set new markdown document
            document = converter->createDocument(text, converterOptions());
        }
    }

    void setCodeHighlightingStyle(const QString &style)
    {
      converter->templateRenderer()->setCodeHighlightingStyle(style);
    }

    // addition function
    QString exportHtml(const QString &styleSheet, const QString &highlightingScript, const QString &highlightStyle);

private:

    HelpGenerator::MarkdownConvert markdownConverter =  HelpGenerator::CDiscountMarkdownConverter;
    MarkdownDocument *document;
    MarkdownConverter *converter;

};


MarkdownConverter::ConverterOptions HelpGeneratorPrivate::converterOptions() const
{
    MarkdownConverter::ConverterOptions parserOptionFlags(
         MarkdownConverter::TableOfContentsOption | MarkdownConverter::NoStyleOption);

    // autolink
    ///parserOptionFlags |= MarkdownConverter::AutolinkOption;
    // strikethrough
    parserOptionFlags |= MarkdownConverter::NoStrikethroughOption;
    // alphabetic lists
    parserOptionFlags |= MarkdownConverter::NoAlphaListOption;
    // definition lists
    parserOptionFlags |= MarkdownConverter::NoDefinitionListOption;
     // SmartyPants
    parserOptionFlags |= MarkdownConverter::NoSmartypantsOption;
    // Footnotes
    ///parserOptionFlags |= MarkdownConverter::ExtraFootnoteOption;
    // Superscript
    parserOptionFlags |= MarkdownConverter::NoSuperscriptOption;

    return parserOptionFlags;
}

Template::RenderOptions HelpGeneratorPrivate::renderOptions() const
{
    Template::RenderOptions renderOptionFlags;

    // math support
    renderOptionFlags |= Template::MathSupport;

    // inline math support
    ///renderOptionFlags |= Template::MathInlineSupport;

    // diagram support
    renderOptionFlags |= Template::DiagramSupport;

    // code highlighting
    renderOptionFlags |= Template::CodeHighlighting;

    return renderOptionFlags;
}

QString HelpGeneratorPrivate::exportHtml( const QString &styleSheet,
                  const QString &highlightingScript, const QString &highlightStyle )
{
    if (!document) return QString();

    QString header;
    if( !styleSheet.isEmpty() )
        header += QString("\n<style>%1</style>").arg(styleSheet);

    if( !highlightingScript.isEmpty() && !highlightStyle.isEmpty() )
    {
        header += QString("\n<style>%1</style>").arg(highlightStyle);
        header += QString("\n<script>%1</script>").arg(highlightingScript);
        header += "\n<script>hljs.initHighlightingOnLoad();</script>";
    }

    return converter->templateRenderer()->exportAsHtml(header, converter->renderAsHtml(document), renderOptions());
}


void HelpGeneratorPrivate::setMarkdownConverter(  HelpGenerator::MarkdownConvert aconverter )
{
    QString style;

    if( markdownConverter == aconverter  && converter )
        return;

    markdownConverter = aconverter;

    if (converter)
    {
        style = converter->templateRenderer()->codeHighlightingStyle();
        delete converter;
    }

    switch (markdownConverter) {
#ifdef ENABLE_HOEDOWN
    case  HelpGenerator::CHoedownMarkdownConverter:
        converter = new HoedownMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;
#endif

    case  HelpGenerator::CRevealMarkdownConverter:
        converter = new RevealMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;

    case  HelpGenerator::CDiscountMarkdownConverter:
    default:
        converter = new DiscountMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;
    }
}

#else

class HelpGeneratorPrivate
{

public:

    explicit HelpGeneratorPrivate( )  { }

    ~HelpGeneratorPrivate() {  }

    // main functions
    void setMarkdownConverter( HelpGenerator::MarkdownConvert aconverter )
    { markdownConverter = aconverter; }

    QString generateHtmlFromMarkdown() { return QString(); }

    QString generateTableOfContents() { return QString(); }

    void setMarkdownText(const QString &/*text*/)
    { }

    void setCodeHighlightingStyle(const QString &/*style*/)
    {  }

    // addition function
    QString exportHtml(const QString &, const QString &, const QString &)
    {  return QString(); }

private:

    HelpGenerator::MarkdownConvert markdownConverter =  HelpGenerator::CDiscountMarkdownConverter;

};

#endif

//-----------------------------------------------------

HelpGenerator::HelpGenerator(  ):
       pdata( new HelpGeneratorPrivate() )

{
    resetDBClient();
    setMarkdownConverter( markdownConverter );
}

HelpGenerator::~HelpGenerator()
{ }

void HelpGenerator::setHelpRecord( const std::string& key )
{
    task = key.c_str();
    QString code = readRecord( key ).c_str();
    pdata->setMarkdownText( code );
}

void HelpGenerator::setMarkdownText(const QString &text)
{
    pdata->setMarkdownText( text );
}

QString HelpGenerator::generateHtmlFromMarkdown()
{
  return pdata->generateHtmlFromMarkdown();
}

QString HelpGenerator::generateTableOfContents()
{
  return pdata->generateTableOfContents();
}

void HelpGenerator::setCodeHighlightingStyle(const QString &style)
{
  pdata->setCodeHighlightingStyle( style);
}

void HelpGenerator::setMarkdownConverter( MarkdownConvert aconverter )
{
    pdata->setMarkdownConverter( aconverter );
}

QString HelpGenerator::exportHtml( const QString &styleSheet,
                  const QString &highlightingScript, const QString &highlightStyle )
{
    return pdata->exportHtml( styleSheet, highlightingScript, highlightStyle );
}

// ---------------------- db ------------

string HelpGenerator::readRecord( const string& key )
{
  if( key.empty() )
      return "";
  try
   {
      if( dbhelp->Find( key.c_str() ) )
      {
          string markdown;
          dbhelp->GetRecord( key.c_str() );
          dbhelp->getValue("markdown", markdown);
          return markdown;
      }
   }
  catch(...) {}

  return "";
}

void HelpGenerator::resetDBClient()
{
   try{

      bsonio::TDBSchemaDocument* newClient =  newDBHelpClient( "" );
      // no schema
      if( newClient == nullptr )
         dbhelp.reset();
      else
         dbhelp.reset( newClient );

    }
    catch(std::exception& e)
      {
         cout << "Internal comment " << e.what() << endl;
         // throw;
      }
}

void HelpGenerator::saveRecordToFile( const string& key, const QString& helpDir )
{
  DocPagesData data;
  dbhelp->GetRecord( key.c_str() );
  bson obj;
  dbhelp->GetBson(&obj);
  data.fromBson(obj.data);
  data.saveFile(helpDir);
  bson_destroy(&obj);
}

void HelpGenerator::copyImageRecordsToFileStructure( const QString& dirPath )
{
    if( !dbhelp )
      return;

    QDir  dir(dirPath);
    dir.mkpath("images");
    clearDir( dirPath+"/images");

    vector<string> aKeyList;
    vector<vector<string>> aValList;
    dbhelp->GetKeyValueList( "*:*:1:", aKeyList, aValList );
    if( aKeyList.empty() )
      return;

    DocPagesData data;
    for( uint ii=0; ii<aKeyList.size(); ii++ )
    {
      string key = aKeyList[ii];
      dbhelp->GetRecord( key.c_str() );
      bson obj;
      dbhelp->GetBson(&obj);
      data.fromBson(obj.data);
      data.saveFile(dirPath);
      bson_destroy(&obj);
    }
}

void HelpGenerator::clearDir( const QString& dirPath )
{
    QDir dir( dirPath );
    dir.setFilter( QDir::NoDotAndDotDot | QDir::Files );
    foreach( QString dirItem, dir.entryList() )
            dir.remove( dirItem );
}
