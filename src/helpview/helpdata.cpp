//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file helpdata.cpp
/// Implementation of DocPagesData class description of  Help record
/// ( Markdown or image file )
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QFileInfo>
#include "helpdata.h"
#ifdef addBSONIO
#include "v_json.h"
#else
#include "bsonio/v_json.h"
#endif

string DocPagesData::fileName() const
{
  string fname;
  switch( type )
  {
    case Image : fname += "images/";
                 break;
    case Markdown:
    default:     break;
  }
  fname += name+"."+ext;
  return fname;
}

void DocPagesData::setRecordKey( const string& akey )
{
  queue<string> fields = bsonio::split( akey, ":" );
  bsonio::bsonioErrIf(fields.size()<3, "Illegal record key", akey);

  name = fields.front();
  fields.pop();
  ext = fields.front();
  fields.pop();
  type = stoi( fields.front() );

  markdown = "";
  image = "";
  // lastModif =  QDateTime::currentDateTime(););
}

// set data to bson
void DocPagesData::toBson( bson *fbson )
{
     bson_init(fbson);

     bson_append_string( fbson, "name", name.c_str() );
     bson_append_string( fbson, "ext", ext.c_str() );
     bson_append_int( fbson, "type", type );
     bson_append_string( fbson, "markdown", markdown.c_str() );
     bson_append_string( fbson, "image", image.c_str() );

     bson_finish(fbson);
}

void DocPagesData::fromBson( const char *fbson )
{
   if(!bsonio::bson_find_string( fbson, "name", name ) )
             name = "undefined";
   if(!bsonio::bson_find_string( fbson, "ext", ext ) )
             ext = "md";
   if(!bsonio::bson_find_value( fbson, "type", type ) )
             type = Markdown;
   if(!bsonio::bson_find_string( fbson, "markdown", markdown ) )
             markdown = "";
   if(!bsonio::bson_find_string( fbson, "image", image ) )
             image = "";
}

void DocPagesData::setFileInfo( const QString& filePath, bool testModif )
{
    QFileInfo finfo(filePath);

    name = finfo.baseName().toStdString(); // baseName
    ext = finfo.suffix().toStdString();
    if( filePath.contains("images/"))
      type = Image;
    else
      type = Markdown;

    markdown = "";
    image = "";
    if( testModif )
     lastModif =  finfo.lastModified();
}

void DocPagesData::readFile( int atype, const QString& filePath )
{
   QFile infile(filePath);
   type = atype;

   switch (type)
   {
     case Image:     // read binary image
        if (infile.open(QFile::ReadOnly ))
        {
          QByteArray barr = infile.readAll();
          QByteArray barr_64 = barr.toBase64();
          image = barr_64.data();
        }
        break;
     case Markdown:  // read md text file
     default:
         if( infile.open(QFile::ReadOnly | QFile::Text) )
         {
             QString content = infile.readAll();
             markdown = content.toStdString();
         }
       break;
    }
 //  cout << name << "." << ext << endl << markdown << image.size() << endl;
}

void DocPagesData::saveFile( const QString& helpDir )
{
  QString fName = helpDir+"/"+fileName().c_str();
  QFile file(fName);

  switch (type)
  {
    case Image:     // read binary image
       if( !image.empty())
       {
          file.open(QIODevice::WriteOnly );
          QByteArray imgbase64 = image.c_str();
          file.write( QByteArray::fromBase64(imgbase64) );
       }
       break;
    case Markdown:  // read md text file
    default:
      if( !markdown.empty() )
      {
         file.open(QIODevice::WriteOnly | QIODevice::Text);
         file.write( markdown.c_str() );
      }
      break;
   }
}


void DocPagesData::deleteFile( const QString& helpDir )
{
  QString fName = helpDir+"/"+fileName().c_str();
  QFile file(fName);
  file.remove();
}


// http://stackoverflow.com/questions/27854979/how-to-load-base64-image-data-from-json-in-qt
/*

QString strReply = (QString)reply->readAll(); // json data from a servlet (created using gson library)
QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
QJsonObject jsonObj = jsonResponse.object();
QByteArray imgbase64 = jsonObj["photo"]; // image data
QImage img;
img.loadFromData(QByteArray::fromBase64(imgbase64));
ui->outputImage->setPixmap(QPixmap::fromImage(img));
ui->outputImage->setScaledContents( true );
ui->outputImage->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
*/

