//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file model_schema.cpp
/// Implementation of TBsonSchemaModel, TBsonSchemaDelegate
/// implements a tree view of bson with our JSON schemas structure
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#include <iostream>
#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>
#include "model_schema.h"
#include "SelectDialog.h"
#include "ModelLineDialog.h"
#ifdef addBSONIO
#include "ar2base.h"
#else
#include "bsonio/ar2base.h"
#endif
using namespace bsonio;

#include <thrift/protocol/TVirtualProtocol.h>
using namespace ::apache::thrift::protocol;

namespace bsonui {

bool TBsonSchemaModel::showComments = false;
bool TBsonSchemaModel::useEnumNames = false;
bool TBsonSchemaModel::editID = false;

BsonSchemaLine::BsonSchemaLine( ThriftStructDef* astrDef,ThriftFieldDef*  afldDef, int andx,
                        const QString& akey, int atype, const QString& avalue, int alevel ):
  strDef(astrDef), fldDef( afldDef ), ndx( andx),  type( atype ), level(alevel), isMap(false)
{
  fldValue.append(akey);
  fldValue.append(avalue);
  if( TBsonSchemaModel::showComments  )
  {    if( fldDef && level==0 )
         fldValue.append(fldDef->fDoc.c_str());
      else
         fldValue.append("");
  }
  parent = 0;
}

BsonSchemaLine::BsonSchemaLine( const BsonSchemaLine* data ):
  strDef(data->strDef), fldDef(data->fldDef), ndx(data->ndx),
  type(data->type), level(data->level), isMap(data->isMap)
{
  fldValue = data->fldValue;
  parent = data->parent;
  noUsedFields  = data->noUsedFields;

  // copy childrens
  for(uint ii=0; ii<data->children.size(); ii++ )
  {
      BsonSchemaLine *line = new BsonSchemaLine( data->children[ii].get() );
      line->parent = this;
      children.push_back( unique_ptr<BsonSchemaLine>(line) );
  }
}

BsonSchemaLine::~BsonSchemaLine()
{
    children.clear();
   //qDeleteAll(children);
}

void TBsonSchemaModel::struct2model( ThriftStructDef* strDef,
            const char *bson_data, BsonSchemaLine* parent )
{
   uint ii;
   ThriftFieldDef*  fldDef;
   vector<string> noUsedFields_;

   for( ii=0; ii<strDef->fields.size(); ii++ )
   {
       fldDef = &strDef->fields[ii];

       // find from bson
       bson_iterator it;
       bson_type type_;
       type_ =  bson_find_from_buffer(&it, bson_data, fldDef->fName.c_str() );

       if(type_ != BSON_EOO || fldDef->fRequired == fld_required )
       {
         if ( !field2model( 0, strDef, fldDef, &it, type_, parent ) )
             noUsedFields_.push_back(fldDef->fName.c_str());
       }
       else
         noUsedFields_.push_back(fldDef->fName.c_str());
   }

   parent->noUsedFields = noUsedFields_;
}

// Set up struct without bson data
void TBsonSchemaModel::struct2model( ThriftStructDef* strDef,
             BsonSchemaLine* parent )
{
   uint ii;
   ThriftFieldDef*  fldDef;
   vector<string> noUsedFields_;

   for( ii=0; ii<strDef->fields.size(); ii++ )
   {
       fldDef = &strDef->fields[ii];

       if( fldDef->fRequired <= fld_default )
       {
         if ( !field2model( 0, strDef, fldDef, 0, BSON_EOO, parent ) )
           noUsedFields_.push_back(fldDef->fName.c_str());
       }
       else
         noUsedFields_.push_back(fldDef->fName.c_str());
   }
   parent->noUsedFields = noUsedFields_;
}


void TBsonSchemaModel::valueFromBson(int type, bson_iterator *it, QString& value )
{
   switch (type)
   {
      // impotant datatypes
      case BSON_NULL:
            value = "null";
            break;
      case BSON_BOOL:
            value =( bson_iterator_bool(it) ? "true" : "false");
            break;
      case BSON_INT:
            value = QString( "%1").arg( bson_iterator_int(it));
            break;
      case BSON_LONG:
            value = QString( "%1").arg( (uint64_t) bson_iterator_long(it));
            break;
      case BSON_DOUBLE:
            //value = QString( "%1").arg( bson_iterator_double(&i));
            value.setNum( bson_iterator_double(it), 'g', 10 );
            break;
      case BSON_STRING:
            value = QString( "%1").arg( bson_iterator_string(it));
            break;
      case BSON_OID:
      { char oidhex[25];
       bson_oid_to_string(bson_iterator_oid(it), oidhex);
       value = oidhex;
      }
       break;
       // main constructions
      case BSON_OBJECT:
      case BSON_ARRAY:
             value = "";
             break;
      default:
            bsonioErr("bson_to_list" ,"can't print type " );
   }
}


bool TBsonSchemaModel::field2model( int level ,ThriftStructDef* strDef, ThriftFieldDef*  fldDef,
                   bson_iterator *it, int datatype, BsonSchemaLine* parent )
{
  int btype;
  QString bkey;
  QString bdata;
  string defaultVal = "";
  if( !level ) defaultVal= fldDef->fDefault;

  if( !fldDef )
    return false;

  if( datatype != BSON_EOO )
  {
     bkey =  bson_iterator_key(it);
     valueFromBson(datatype, it, bdata );
  }
  else
     {    // field from default !!! must be changed
        bkey =  fldDef->fName.c_str();
        string defaultInsVal = "";
        if( !level )
        {    defaultInsVal= fldDef->insertedDefault;
             if( defaultInsVal.empty() )
              defaultInsVal = fldDef->fDefault;
        }
        if( fldDef->fName == JDBIDKEYNAME )
          bdata = "";
        else
          bdata = getDefValue( fldDef->fTypeId[level], defaultInsVal ).c_str();
     }

  // test types conversion
  switch( fldDef->fTypeId[level] )
  {
    case T_BOOL:
    //case T_BYTE:
    case T_I08:
    case T_I16:
    case T_I32:
    case T_U64:
    case T_I64:
    case T_DOUBLE:
    case T_STRING:
    //case T_UTF7:
    case T_UTF8: if( datatype == BSON_ARRAY || datatype == BSON_OBJECT )
                   return false; // illegal object type
                 btype = getBsonType(fldDef->fTypeId[level]);
                 if( fldDef->fName == JDBIDKEYNAME )
                    btype =  BSON_OID;
                 break;
    case T_MAP:
    case T_STRUCT:
              if( datatype != BSON_EOO && datatype != BSON_OBJECT )
                   return false; // illegal object type
                 btype = BSON_OBJECT;
                 break;
    case T_SET:
    case T_LIST: if( datatype != BSON_EOO && datatype != BSON_ARRAY )
                 return false; // illegal object type
                 btype = BSON_ARRAY;
        break;
    default:
    case T_UTF16:
    case T_STOP:
    case T_VOID: // may be error
                return false;
  }

  // add line to tree view
  BsonSchemaLine *line = new BsonSchemaLine(strDef, fldDef,
                   parent->children.size(), bkey, btype, bdata, level );
  line->parent = parent;
  parent->children.push_back( unique_ptr<BsonSchemaLine>(line) );

  // add levels for objects and arrays
  switch (btype)
  {
     // main constructions
     case BSON_OBJECT:
           {
            if( fldDef->fTypeId[level] == T_STRUCT ) // other type
              {
                // structure
                if( fldDef->className.empty() )
                    bsonioErr("bson_to_list" , "Undefined struct name into schema" );
                ThriftStructDef* strDef2 = schema->getStruct( fldDef->className);
                if( strDef2 == nullptr )
                   bsonioErr( fldDef->className , "Undefined struct definition" );
                if( datatype == BSON_EOO || !it )
                {
                  if(!defaultVal.empty() )
                  {
                      bson bsrec;
                      jsonToBson( &bsrec, defaultVal );
                      struct2model( strDef2, bsrec.data, line );
                  }
                  else
                     struct2model( strDef2,  line );
                }
                else
                    struct2model( strDef2, bson_iterator_value(it),  line );
              }
             else
             if( fldDef->fTypeId[level] == T_MAP )
              { // map
                line->isMap = true;
                if( datatype != BSON_EOO && it )
                 list2model( level+2, strDef, fldDef, bson_iterator_value(it), line );
                else
                 if(!defaultVal.empty() ) // default to map
                 {
                    bson bsrec;
                    jsonToBson( &bsrec, defaultVal );
                    list2model( level+2, strDef, fldDef,  bsrec.data, line );
                 }
              }
          }
          break;
     case BSON_ARRAY:
          // !! next level
          if( datatype != BSON_EOO && it )
           list2model( level+1, strDef, fldDef, bson_iterator_value(it), line );
          else
              if(!defaultVal.empty() )
              {
                bson bsrec;
                jsonToBsonArray( &bsrec, defaultVal );
                list2model( level+1, strDef, fldDef, bsrec.data, line );
               }
     default:     break;
  }
  return true;
}

/// Read array from bson
void TBsonSchemaModel::list2model( int level, ThriftStructDef* strDef, ThriftFieldDef*  fldDef,
                   const char *data, BsonSchemaLine* parent )
{
    bson_iterator i;
    const char* key;
    QString value;

    int ndx=0;
    bson_iterator_from_buffer(&i, data);
    while (bson_iterator_next(&i))
    {
       bson_type t = bson_iterator_type(&i);
       if (t == 0)
          break;
       key = bson_iterator_key(&i);
       valueFromBson(t, &i, value );

       BsonSchemaLine *line = new BsonSchemaLine(strDef, fldDef, ndx++, key, t, value, level );
       line->parent = parent;
       parent->children.push_back( unique_ptr<BsonSchemaLine>(line) );
       if( fldDef->fTypeId[level] == T_MAP ) //if(datatype == BSON_OBJECT )
          line->isMap = true;

       switch (t)
       {
          // main constructions
          case BSON_OBJECT:
                {
                  if( fldDef->fTypeId[level] == T_STRUCT ) // other type
                  {
                     // structure
                     if( fldDef->className.empty() )
                        bsonioErr("bson_to_list" , "Undefined struct name into schema" );
                     ThriftStructDef* strDef2 = schema->getStruct( fldDef->className);
                     if( strDef2 == nullptr )
                        bsonioErr( fldDef->className , "Undefined struct definition" );
                     struct2model( strDef2, bson_iterator_value(&i), line );
                  }
                 else
                 { // map
                   list2model( level+2, strDef, fldDef, bson_iterator_value(&i), line );
                }
               }
               break;
          case BSON_ARRAY:
               list2model( level+1, strDef, fldDef, bson_iterator_value(&i), line );
          default:     break;
       }
    }
}

void TBsonSchemaModel::list_to_bson_full(BsonSchemaLine *headline, bson* bsonobj )
{
    bson_destroy(bsonobj );
    bson_init( bsonobj );
    for(uint ii=0; ii< headline->children.size(); ii++ )
      list_to_bson( headline->children[ii].get(),  bsonobj );
    bson_finish( bsonobj );
}

void TBsonSchemaModel::list_to_bson(BsonSchemaLine *line, bson* bsonobj )
{
    uint ii;
    switch (line->type)
    {
      // impotant datatypes
      case BSON_NULL:
           bson_append_null(bsonobj, line->fldValue[0].toUtf8().data());
           break;
      case BSON_BOOL:
           bson_append_bool(bsonobj, line->fldValue[0].toUtf8().data(),
              ( line->fldValue[1] == "true"? true : false));
           break;
      case BSON_INT:
      case BSON_LONG:
      case BSON_DOUBLE:
           bson_append_double(bsonobj, line->fldValue[0].toUtf8().data(),
                   line->fldValue[1].toDouble() );
           break;
      case BSON_OID:
           // oid
          if( !line->fldValue[1].isEmpty() && line->fldValue[1] != emptiness.c_str() )
           {  // oid
              bson_oid_t oid;
              bson_oid_from_string( &oid, line->fldValue[1].toUtf8().data() );
              bson_append_oid( bsonobj, JDBIDKEYNAME, &oid);
           }
           break;
       case BSON_STRING:
               bson_append_string(bsonobj, line->fldValue[0].toUtf8().data(),
                   line->fldValue[1].toUtf8().data() );
           break;
      // main constructions
      case BSON_OBJECT:
           bson_append_start_object(bsonobj, line->fldValue[0].toUtf8().data());
           for( ii=0; ii< line->children.size(); ii++ )
             list_to_bson( line->children[ii].get(),  bsonobj );
           bson_append_finish_object( bsonobj );
           break;
     case BSON_ARRAY:
         bson_append_start_array(bsonobj, line->fldValue[0].toUtf8().data());
         for( ii=0; ii< line->children.size(); ii++ )
          list_to_bson( line->children[ii].get(),  bsonobj );
         bson_append_finish_array( bsonobj );
         break;
     default:
        break;
      //     bsonioErr("list_to_bson" ,"can't print type " );
    }
}


string TBsonSchemaModel::getFieldPath( const QModelIndex& index )
{
    BsonSchemaLine *line =  lineFromIndex(index);
    string data_ = line->fldValue[0].toUtf8().data();
    line = line->parent;
    while( line->parent )
    {
      data_ =  string(line->fldValue[0].toUtf8().data())+"."+data_;
      line = line->parent;
    }
    return data_;
 }

string TBsonSchemaModel::getFieldData( const QModelIndex& index )
{
    BsonSchemaLine *line =  lineFromIndex(index);

    string data_ = line->fldValue[1].toUtf8().data();
    if(  line->type == BSON_OBJECT || line->type == BSON_ARRAY )
    {
        bson bobj;
        bson_init( &bobj );
        for(uint ii=0; ii< line->children.size(); ii++ )
            list_to_bson( line->children[ii].get(),  &bobj );
        bson_finish( &bobj );
        if( line->type == BSON_OBJECT )
          jsonFromBson( bobj.data, data_ );
        else
            jsonArrayFromBson( bobj.data, data_ );
        bson_destroy( &bobj );
    }
    return data_;
}



//--------------------------------------------------------------------------------------
//  class TBsonSchemaModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//  Reading/writing data from/to bson data
//---------------------------------------------------------------------------------------
TBsonSchemaModel::TBsonSchemaModel( const ThriftSchema *aschema, bson *abson, const string& aschemaName,
                                    const QStringList& aHeaderData,   QObject* parent ):
        TBsonAbstractModel(parent), schema(aschema), hdData( aHeaderData)
{
  rootNode = 0;
  setupModelData(abson, aschemaName);

  connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
  			parent,  SLOT(objectChanged()) );	 
}

TBsonSchemaModel::~TBsonSchemaModel()
{
  if(rootNode )
     delete rootNode;
}

BsonSchemaLine *TBsonSchemaModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<BsonSchemaLine *>(index.internalPointer());
    } else {
        return rootNode;
    }
}


void TBsonSchemaModel::setupModelData( bson *abson, const string& aschemaName )
{
   bsonData = abson;
   schemaName = aschemaName;
   ThriftStructDef* strDef = nullptr;
   if( !schemaName.empty() )
      strDef =  schema->getStruct(schemaName);
   if( strDef == nullptr )
     bsonioErr( schemaName, "Undefined struct definition" );

    beginResetModel();
    if( rootNode )
      delete rootNode;

    rootNode = new BsonSchemaLine( strDef, nullptr, 0, "start", BSON_OBJECT , "", 0 );
    if( (bsonData->cur - bsonData->data) > 5)
       struct2model(strDef, bsonData->data,  rootNode );
    else
        struct2model(strDef,  rootNode );
    endResetModel();

}


QModelIndex TBsonSchemaModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    BsonSchemaLine *parentItem = lineFromIndex( parent );
    return createIndex(row, column, parentItem->children[row].get());
}

QModelIndex TBsonSchemaModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();
 
    BsonSchemaLine *childItem = lineFromIndex(child);
    BsonSchemaLine *parentItem = childItem->parent;
    if (parentItem == rootNode )
        return QModelIndex();
    return createIndex(parentItem->ndx, 0, parentItem);
}


int TBsonSchemaModel::rowCount( const QModelIndex& parent ) const
{
   if (!rootNode)
       return 0;
  if (parent.column() > 0)
      return 0;
  BsonSchemaLine *parentItem = lineFromIndex( parent );
  return parentItem->children.size();
}	

int TBsonSchemaModel::columnCount( const QModelIndex& /*parent*/ ) const
{
   if( showComments )
     return 3;
   return 2;
}	

Qt::ItemFlags TBsonSchemaModel::flags( const QModelIndex& index ) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);
  BsonSchemaLine *item = lineFromIndex( index );
  if( !TBsonSchemaModel::editID && item->fldValue[0][0] == '_' )
  {
    return (flags & ~Qt::ItemIsEditable);
  }

  if( index.column() == 0 && item->parent && item->parent->isMap ) // not array of maps
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }

  if( index.column() == 1 && (item->type != BSON_OBJECT && item->type != BSON_ARRAY ) )
  {
      flags |= Qt::ItemIsEditable;
      return flags;
  }
  else
     return (flags & ~Qt::ItemIsEditable);
}

QVariant TBsonSchemaModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
 if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<hdData.size())
	   return hdData[section];
  return QVariant();
}

QString TBsonSchemaModel::getDescription( const QModelIndex& index ) const
{
    BsonSchemaLine *item = lineFromIndex( index );

    // get doc from thrift schema
    QString titl =  item->fldDef->fDoc.c_str();
    if( !titl.isEmpty())
      return titl;

    // get name with ndx
    while( item->parent->type ==  BSON_ARRAY )
    {  titl = item->fldValue[0] + " " + titl;
       item = item->parent;
    }
    return titl +  item->fldValue[0];
}

QVariant TBsonSchemaModel::data( const QModelIndex& index, int role ) const
{
   if(!index.isValid())
	 return QVariant();	

   switch( role )
   {
     case Qt::DisplayRole:
          {   BsonSchemaLine *item = lineFromIndex( index );
              if( useEnumNames && index.column() == 1 &&
                  item->type == BSON_INT && !item->fldDef->className.empty() )
              {
                  ThriftEnumDef* enumdef = schema->getEnum( item->fldDef->className );
                  if( enumdef )
                   return  QString(enumdef->getNamebyId(item->fldValue[1].toInt()).c_str());

              }
              if( index.column() == 2 &&
                  item->type == BSON_INT && !item->fldDef->className.empty() )
              {
                  ThriftEnumDef* enumdef = schema->getEnum( item->fldDef->className );
                  if( enumdef )
                  { string name = enumdef->getNamebyId(item->fldValue[1].toInt());
                   return  QString(enumdef->getDoc(name).c_str());
                  }

              }
              if( index.column() < item->fldValue.size()  )
                   return item->fldValue[index.column()];
          }
     case Qt::EditRole:
            {  BsonSchemaLine *item = lineFromIndex( index );
               if( index.column() < item->fldValue.size()  )
                   return item->fldValue[index.column()];
            }
      case Qt::ToolTipRole:
      case Qt::StatusTipRole:
              return  getDescription(index );
      case Qt::ForegroundRole:
          {
            BsonSchemaLine *item = lineFromIndex( index );
            if ( index.column() == 0 && item->parent && !item->parent->isMap  )
               {
                 return QVariant( QColor( Qt::darkCyan ) );
               }
            //return QVariant( QColor( Qt::black ) );
          }
      default: break;
   }
   return QVariant();
}

bool TBsonSchemaModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
	if( index.isValid() && ( role == Qt::EditRole ) )
	{
       BsonSchemaLine *line =lineFromIndex(index);
       if(  index.column() <  line->fldValue.size() )
		  {	 
             line->fldValue[index.column()] = QVariant(value).toString();
             list_to_bson_full( rootNode, bsonData );
		  }	 
	 return true;
	} 
	return false;
}

string TBsonSchemaModel::helpName( const QModelIndex& index ) const
{
    BsonSchemaLine *item =  lineFromIndex(index);
    // get name from thrift schema
    return  item->fldDef->fName;
}

bool TBsonSchemaModel::isNumber( const QModelIndex& index ) const
{
    BsonSchemaLine *line =  lineFromIndex(index);
    return (line->type == BSON_INT || line->type == BSON_LONG ||
                line->type == BSON_DOUBLE );
}

bool TBsonSchemaModel::isArray( const QModelIndex& index ) const
{
    BsonSchemaLine *line =  lineFromIndex(index);
    return ( line->level == 0 && ( (line->type == BSON_OBJECT &&  line->isMap )
             || line->type == BSON_ARRAY ) );
}

/*void TBsonSchemaModel::resizeArray( QWidget * aparent, const QModelIndex& index )
{
    BsonSchemaLine *line =  lineFromIndex(index);
    bool ok = 0;
    uint size = QInputDialog::getInt( aparent, "Please, select new array size",
     "Array size ", line->children.size(), 0, 999, 1, &ok );

    if(!ok) // cancel command
      return;

    // resizeline item
    if( size == line->children.size() )
      return;

    // delete if smaler
    if( size < line->children.size() )
    {
       beginRemoveRows( index, size, line->children.size()-size);
       line->children.resize(size);
       endRemoveRows();
       list_to_bson_full( rootNode, bsonData );
       return;
    }

    beginInsertRows( index, line->children.size(), size-1);

    // add new elements
    BsonSchemaLine *linechild;
    for( uint ii=line->children.size(); ii<size; ii++)
    {
        QString key_ = QString( "%1").arg( ii );
        if( line->children.size() > 0) // copy last
        {
          linechild = new BsonSchemaLine( line->children.back().get() );
          linechild->ndx = ii;
          linechild->fldValue[0] = key_;
          linechild->parent = line;
          line->children.push_back( unique_ptr<BsonSchemaLine>(linechild) );
        }
        else     // add null
        {
           int level_ =  line->level+1;
             if( line->fldDef->fTypeId[level_] == T_MAP )
                 level_++;
             int type_ = getBsonType( line->fldDef->fTypeId[level_]);
             QString value_ = getDefValue( line->fldDef->fTypeId[level_], "" ).c_str();
             linechild = new BsonSchemaLine(line->strDef, line->fldDef, ii, key_, type_, value_, level_ );
             if( line->fldDef->fTypeId[level_] == T_MAP )
                linechild->isMap = true;
             linechild->parent = line;
             line->children.push_back( unique_ptr<BsonSchemaLine>(linechild) );

             if( line->fldDef->fTypeId[level_] == T_STRUCT )
             {
                 if( line->fldDef->className.empty() )
                     bsonioErr("addlinestoArray" , "Undefined struct name into schema" );
                 ThriftStructDef* strDef2 = schema->getStruct( line->fldDef->className);
                 if( strDef2 == nullptr )
                    bsonioErr( line->fldDef->className , "Undefined struct definition" );
                 struct2model( strDef2,  linechild );
             }
        }
    }
    endInsertRows();
    list_to_bson_full( rootNode, bsonData );
}
*/

void TBsonSchemaModel::resizeArray( QWidget * parentWidget, const QModelIndex& index )
{
    // get array size and def value
    BsonSchemaLine *line =  lineFromIndex(index);
    ModelLineDialog dlg( index, index, line->fldValue[0], parentWidget );
    if( !dlg.exec() )
       return;
    QString new_value = dlg.getDefValue();
    vector<int> sizes = dlg.getArraySizes();

    // if array alloc it
    if( ( (line->type == BSON_OBJECT &&  line->isMap )||
           line->type == BSON_ARRAY ) && sizes.size() >0 )
        resizeArray( 0, sizes, new_value, index );

    // copy data to internal bson
    list_to_bson_full( rootNode, bsonData );
}


void TBsonSchemaModel::resizeArray(uint level, const vector<int>& sizes,
                     const QString& defvalue, const QModelIndex& cur_index )
{
    if( sizes.size() <= level )
      return;

    BsonSchemaLine *line =  lineFromIndex(cur_index);
    uint size = sizes[level];

    // resizeline item
    if( size == line->children.size() )
       ;
    // delete if smaler
    else if( size < line->children.size() )
    {
       beginRemoveRows( cur_index, size, line->children.size()-size);
       line->children.resize(size);
       endRemoveRows();
    }
    else
     {
        QString value_ = "";
        int level_ =  line->level;
        if( line->fldDef->fTypeId[level_] == T_MAP )
                level_++;
        level_++;

      try {
               beginInsertRows( cur_index, line->children.size(), size-1);

               // add new elements
               BsonSchemaLine *linechild;
               for( uint ii=line->children.size(); ii<size; ii++)
               {
                 QString key_ = QString( "%1").arg( ii );
                 int type_ = getBsonType( line->fldDef->fTypeId[level_]);
                 if( type_ != BSON_ARRAY && type_ != BSON_OBJECT )
                     value_ = defvalue;

                linechild = new BsonSchemaLine(line->strDef, line->fldDef,
                                               ii, key_, type_, value_, level_ );

                if( line->fldDef->fTypeId[level_] == T_MAP )
                    linechild->isMap = true;
                linechild->parent = line;
                line->children.push_back( unique_ptr<BsonSchemaLine>(linechild) );

                if( line->fldDef->fTypeId[level_] == T_STRUCT )
                {
                   if( line->fldDef->className.empty() )
                      bsonioErr("addlinestoArray" , "Undefined struct name into schema" );
                   ThriftStructDef* strDef2 = schema->getStruct( line->fldDef->className);
                   if( strDef2 == nullptr )
                      bsonioErr( line->fldDef->className , "Undefined struct definition" );
                   struct2model( strDef2,  linechild );
               }
            }

            endInsertRows();
       }catch( ... )
         {
            endInsertRows();
            throw;
         }
    }

   // try to resize next level
   for( uint ii=0; ii< line->children.size(); ii++)
   {  const QModelIndex childIndex = index( ii ,0, cur_index );
      resizeArray( level+1, sizes, defvalue, childIndex );
   }
}

bool TBsonSchemaModel::isCanBeRemoved( const QModelIndex& index ) const
{
    BsonSchemaLine *line =  lineFromIndex(index);
    BsonSchemaLine *parentItem = line->parent;
    return (  parentItem->type == BSON_OBJECT );
    // map is object
}

bool TBsonSchemaModel::isStruct( const QModelIndex& index ) const
{
    BsonSchemaLine *line =  lineFromIndex(index);
    BsonSchemaLine *parentItem = line->parent;
    return ( ( parentItem->type == BSON_OBJECT ||
             (line->type== BSON_OBJECT && line->children.size() == 0 ))
             && !parentItem->isMap  );
}

bool TBsonSchemaModel::isUnion( const QModelIndex& index ) const
{
    BsonSchemaLine *line =  lineFromIndex(index);
    BsonSchemaLine *parentItem = line->parent;
    return ( parentItem->type == BSON_OBJECT && !parentItem->isMap
             && line->strDef->testUnion() );
}


void TBsonSchemaModel::delObject( QWidget * parentWidget, const QModelIndex& index )
{
    BsonSchemaLine *line =  lineFromIndex(index);
    if( line->fldDef->fRequired == fld_required )
    {
       QMessageBox::warning(parentWidget, "Data object to delete",
                "Required data object cannot be deleted");
        return;
    }
    QString msg = QString("Confirm deletion of %1 data object?").arg(line->fldValue[0] );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data object to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    beginRemoveRows( parent(index), index.row(), index.row());
    // resizeline item
    BsonSchemaLine *parent_ = line->parent;
    if( !parent_->isMap )
      parent_->noUsedFields.push_back( line->fldDef->fName );
    parent_->children.erase( parent_->children.begin() + index.row() );
    // copy childrens
    for(uint ii=index.row(); ii<parent_->children.size(); ii++ )
    {
      parent_->children[ii].get()->ndx--;
    }
    endRemoveRows();
    list_to_bson_full( rootNode, bsonData );
}

void TBsonSchemaModel::delObjectsUnion( QWidget * parentWidget, const QModelIndex& index )
{
    BsonSchemaLine *line =  lineFromIndex(index);

    QString msg = QString("Confirm deletion of all alternatives to %1 data object union variant?").arg(line->fldValue[0] );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data objects to delete", msg,
             QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
       return;

    // resizeline item
    BsonSchemaLine *parent_ = line->parent;
    uint current_next = index.row()+1;

    if( current_next < parent_->children.size()  )
    { //remove rows after current
       beginRemoveRows( parent(index), current_next, parent_->children.size()-1 );
       for(uint ii=current_next; ii<parent_->children.size(); ii++ )
           parent_->noUsedFields.push_back( parent_->children[ii]->fldDef->fName );
       parent_->children.erase( parent_->children.begin()+current_next, parent_->children.end() );
       endRemoveRows();
    }

    current_next = index.row();
    if( current_next > 0  )
    { //remove rows before current
       beginRemoveRows( parent(index), 0, current_next );
       for(uint ii=0; ii<current_next; ii++ )
           parent_->noUsedFields.push_back( parent_->children[ii]->fldDef->fName );
       parent_->children.erase( parent_->children.begin(), parent_->children.begin()+current_next );
       parent_->children[0].get()->ndx = 0;
       endRemoveRows();
    }

    list_to_bson_full( rootNode, bsonData );
}


const QModelIndex TBsonSchemaModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    uint row;
    QModelIndex parentIndex;
    BsonSchemaLine *line =  lineFromIndex(cindex);
    if(line->type== BSON_OBJECT && line->children.size() == 0 )
    {
        parentIndex = cindex;
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex);
        row =  line->parent->children.size();
    }

    BsonSchemaLine *parent_ = lineFromIndex(parentIndex);//line->parent;

    // select fields to be added
    vector<string> sch_lst = parent_->noUsedFields;
    vector<int> sel;
    if( sch_lst.empty() )
      return cindex;

    SelectDialog selDlg( parentWidget, "Please, select objects to be added", sch_lst, sel );
    if( !selDlg.exec() )
      return cindex;

    sel = selDlg.allSelected();
     // insert from list
    try {
         beginInsertRows( parentIndex, row, row+sel.size() );
         //beginInsertRows( parent(cindex), line->children.size(), line->children.size()+sel.size());
         for(uint ii=0; ii<sel.size(); ii++ )
         {
           string new_object = sch_lst[sel[ii]];
           // add object
           ThriftFieldDef*  fldDef = line->strDef->getFieldDef(new_object);
           if( field2model( 0, line->strDef, fldDef, 0, BSON_EOO , parent_ ))
            parent_->noUsedFields.erase(
                    std::find(parent_->noUsedFields.begin(), parent_->noUsedFields.end(), new_object));
         }
        endInsertRows();
     } catch( ... )
       {
        endInsertRows();
        throw;
       }

   list_to_bson_full( rootNode, bsonData );
   return cindex.sibling( parent_->children.size()-1, 0);
}

const QModelIndex  TBsonSchemaModel::addObject( QWidget* parentWidget, const QModelIndex& cindex )
{
    uint row;
    QModelIndex parentIndex;
    BsonSchemaLine *line =  lineFromIndex(cindex);
    if(line->type== BSON_OBJECT && line->children.size() == 0 )
    {
        parentIndex = cindex;
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex);
        row =  line->parent->children.size();
    }

    // insert data
    ModelLineDialog dlg(cindex, parentIndex, "",parentWidget);
    if( !dlg.exec() )
       return cindex;

    // Get line name
    string new_object = dlg.getName().toUtf8().data();
    ThriftFieldDef*  fldDef = line->strDef->getFieldDef(new_object);
    if(fldDef == nullptr  )
      return cindex;
    //int new_type =      dlg.getType();
    QString new_value = dlg.getDefValue();
    fldDef->insertedDefault = new_value.toUtf8().data();
    BsonSchemaLine *parent_ =  lineFromIndex(parentIndex);

    try{
         // start insert row
        beginInsertRows( parentIndex, row, row );
        // add object
        string oldDef = fldDef->fDefault;
        fldDef->fDefault = ""; // protect default allocation
        if( field2model( 0, line->strDef, fldDef, 0, BSON_EOO , parent_ ))
          parent_->noUsedFields.erase(
          std::find(parent_->noUsedFields.begin(), parent_->noUsedFields.end(), new_object));
        fldDef->fDefault = oldDef;
        endInsertRows();
    } catch( ... )
      {
       endInsertRows();
       throw;
      }

    // if array alloc it
    const QModelIndex new_index = index( row,0, parentIndex );
    vector<int> sizes = dlg.getArraySizes();
    if( /*    ( (new_type == BSON_OBJECT &&   ->isMap )||
         new_type == BSON_ARRAY )*/
        isArray( new_index ) && sizes.size() >0  )
    {
        resizeArray( 0, sizes, new_value, new_index );
    }

    // copy data to internal bson
    list_to_bson_full( rootNode, bsonData );
    return new_index;
}


//-------------------------------------------------------------------------------------
// TBsonDelegate -  individual items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

TBsonSchemaDelegate::TBsonSchemaDelegate( QObject * parent ):
	QItemDelegate( parent )
{ }

// Editing QTreeView for objects in System page
QWidget *TBsonSchemaDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    TBsonSchemaModel *model = (TBsonSchemaModel *)(index.model() );
    BsonSchemaLine *line =  model->lineFromIndex(index);

    // using schema editors
    int type = line->fldDef->fTypeId.back();

     // map key
    if( index.column() == 0 && line->parent->isMap )
        {

           uint ii=0;
           while( line->fldDef->fTypeId[ii] != T_MAP && ii < line->fldDef->fTypeId.size() )
             { ii++;}
           if( ii < line->fldDef->fTypeId.size())
            type = line->fldDef->fTypeId[ii+1];
        }

   switch( type  )
     {
          case T_BOOL:
                {
                  QComboBox *accessComboBox = new QComboBox(parent);
                  accessComboBox->addItem(tr("false"), tr("false"));
                  accessComboBox->addItem(tr("true"), tr("true"));
                  return accessComboBox;
                 }
          case T_I08:
          case T_I16:
          case T_I32:
                {
                  // enums name
                  if( !line->fldDef->className.empty() )
                  {
                     ThriftEnumDef* enumdef = model->getEnum( line->fldDef->className );
                     if(enumdef != nullptr )
                     {
                         vector<string> lst = enumdef->getNamesList();
                         QComboBox *accessComboBox = new QComboBox(parent);
                         for( uint ii=0; ii<lst.size(); ii++ )
                         {
                             string itname =lst[ii];
                             accessComboBox->addItem(tr(itname.c_str()), enumdef->getId(itname) );
                             accessComboBox->setItemData(ii, enumdef->getDoc(itname).c_str(), Qt::ToolTipRole);
                         }
                         return accessComboBox;
                     }
                  }
                }
          case T_I64:
          case T_U64:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                QIntValidator *ivalid = new QIntValidator(lineEdit);
                if( line->fldDef->fTypeId.back() == T_U64 )
                   ivalid->setBottom(0);
                if( line->fldDef->minval != DOUBLE_EMPTY )
                  ivalid->setBottom(line->fldDef->minval);
                if( line->fldDef->maxval != DOUBLE_EMPTY )
                  ivalid->setTop(line->fldDef->maxval);
                lineEdit->setValidator(ivalid);
                return lineEdit;
              }
         case T_DOUBLE:
               { QLineEdit *lineEdit = new QLineEdit(parent);
                 QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
                 if( line->fldDef->minval != DOUBLE_EMPTY )
                   dvalid->setBottom(line->fldDef->minval);
                 if( line->fldDef->maxval != DOUBLE_EMPTY )
                   dvalid->setTop(line->fldDef->maxval);
                 lineEdit->setValidator(dvalid);
                 return lineEdit;
               }
          case T_UTF8:
          case T_STRING:
              { QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
              }
          // main constructions
          default:
             bsonioErr("TBsonDelegate" ,"can't print type " );

  }

   return QAbstractItemDelegate::createEditor( parent, option,  index );
}

void TBsonSchemaDelegate::setEditorData(QWidget *editor,
                                  const QModelIndex &index) const
{
   QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
   if( cellEdit)
   {
       //if( currentData().isValid() )
       {
         int idx = cellEdit->findData(index.data(Qt::EditRole));
         if ( idx != -1 )  // -1 for not found
            cellEdit->setCurrentIndex(idx);
       }
       //else
       //    cellEdit->setCurrentIndex( index.data(Qt::EditRole) );
   }
    else
       QItemDelegate::setEditorData(editor, index);
}

void TBsonSchemaDelegate::setModelData(QWidget *editor,
                                 QAbstractItemModel *model,
                                 const QModelIndex &index) const
{
   QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
   if( cellEdit)
   {
      model->setData(index, cellEdit->currentData(),  Qt::EditRole);
   } else
      QItemDelegate::setModelData(editor, model, index);
 }

} // namespace bsonui

//---------------------- End of file  model_schema.cpp ---------------------------
