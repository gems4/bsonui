//  This is BSONUI library+API (https://bitbucket.org/gems4/bsonui)
//
/// \file preferences.h
/// Declaration of UIPreferences object for monitoring changes
//
// BSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONUI depends on the following open-source software products:
// BSONIO (https://bitbucket.org/gems4/bsonio); Qt5 (https://qt.io).
//

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QObject>
#ifdef addBSONIO
#include "io_settings.h"
#else
#include "bsonio/io_settings.h"
#endif

namespace bsonui {

/// \class Preferences - monitoring preferences into BSONIO
class UIPreferences: public QObject
{
    friend UIPreferences& uiSettings();
    bsonio::BsonioSettings& _iosettings;
    std::shared_ptr<bsonio::TDataBase> _database;

    Q_OBJECT

    /// Read/Init main settings
    void setDefValues();

    /// Constructor
    UIPreferences();

signals:

    /// Changed Data Base
    void dbChanged();
    /// Changed thrift schemas Dir
    void schemaChanged();
    /// Signal to update "View" menu
    void viewMenuChanged();
    /// Signal to update editors model
    void modelChanged();
    /// Signal to update models tableview
    void tableChanged();
    /// Error editing settings
    void errorSettings(string);

public slots:

  /// Run update Preferences Dialog
  void CmSettingth();
  /// On/off extern flag to show schema comments into editor
  void CmShowComments( bool checked );
  /// On/off extern flag to show enum names into editor
  void CmDisplayEnums( bool checked );
  /// On/off extern flag to edit system data
  void CmEditID( bool checked );
  /// On/off extern flag to keep data fields expanded
  void CmEditExpanded( bool checked );
  /// Define new input/output directory
  void CmSetUserDir( QString dirPath);

public:

  void updateUiSettings()
  {
     setDefValues();
  }

  const bsonio::TDataBase* database() const
  {
    return _database.get();
  }


  const std::shared_ptr<bsonio::TDataBase>& dbclient() const
  {
    return _database;
  }

  void updateDatabase()
  {
    _database->updateSettings();
  }

};

/// Function to connect to only one Preferences object
extern UIPreferences& uiSettings();

} // namespace bsonui

#endif // PREFERENCES_H
