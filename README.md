# BSONUI #

This [bsonio](https://bitbucket.org/gems4/bsonio)- and [Qt5](http://www.qt.io/)-based widget C++ library and API is aimed at implementing lightweight GUIs for editing/viewing the structured data kept in a NoSQL database, with rendering documents to/from JSON, YAML, XML files, and with importing/exporting data from/to arbitrary foreign formats. A [Qwt6](http://qwt.sourceforge.net)-based widget for graphical presentation of tabular data (csv format fields) is also available.

* BSONUI is written in C/C++ using open-source Qt5, Qwt6 and BSONIO codes.
* Version: currently 0.1.
* Will be distributed as is (no liability) under the terms of Lesser GPL v.3 license.

### How to clone (download) the BSONUI source code ###

* In your home directory, make a folder named e.g. `bsonui`.
* Cd into `bsonui` and clone this repository from  `https://bitbucket.org/gems4/bsonui.git` using a preinstalled free git client e.g. SourceTree or SmartGit (the best way on Windows). 
* Alternatively on Mac OS X or linux, open a terminal and in the command line (do not forget a period) run:
~~~
cd /home/your_user/bsonui
git clone https://bitbucket.org/gems4/bsonui.git . 
~~~

### How to build the BSONUI library on Linux or Mac OS ###

* Make sure that Qt5, Qwt6 amd CMake are installed in your system. For Ubuntu 16.04, this can be done using the following commands:
~~~
sudo apt-get install cmake
sudo apt-get install qt5-default qttools5-dev libqt5svg5 libqt5svg5-dev libqt5help5
sudo apt-get install libqwt-headers libqwt-qt5-6 libqwt-qt5-dev libqwtmathml-qt5-6 libqwtmathml-qt5-dev
~~~

* Navigate to the directory where this README file is located, and type in the terminal (assuming that Qt5 is installed in the system):
~~~
cd bsonui
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
~~~

* If Qt5 libraries are installed locally (for instance in /home/your_user/Qt5/5.5/gcc64) then use Cmake prefix path, as shown below:
~~~
cd bsonui
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=/home/your_user/Qt5/5.5/gcc_64
make
~~~

* This will create a `build` directory where BSONUI, BSONIO and all third-party libraries are built.

## How to use BSONUI (use cases) ##
TBD