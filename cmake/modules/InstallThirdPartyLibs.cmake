# Include the ExternalProject module
include(ExternalProject)

# Ignore all warning from the third-party libraries
if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions("-w")
endif()

# Set the list of compiler flags for the external projects
if(NOT ${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    set(CXXFLAGS "-fPIC")
endif()

# Download and install the bsonio library
ExternalProject_Add( BSONIO
    PREFIX thirdparty
    GIT_REPOSITORY https://svetadmitrieva@bitbucket.org/gems4/bsonio.git
#    GIT_TAG thrift11
    UPDATE_COMMAND ""
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${THIRDPARTY_DIR}
               -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_INCLUDEDIR=include
               -DCMAKE_INSTALL_LIBDIR=lib
               -DCMAKE_INSTALL_BINDIR=bin
               -DBUILD_SHARED_LIBS=ON
)

#ExternalProject_add( QWT
#  PREFIX thirdparty
##   SVN_REPOSITORY https://qwt.svn.sourceforge.net/svnroot/qwt/branches/qwt-6.1
# SVN_REPOSITORY svn://svn.code.sf.net/p/qwt/code/branches/qwt-6.1
#  UPDATE_COMMAND ""
##  SOURCE_DIR QWT
##  BINARY_DIR QWT-build
##  "${cmakeversion_external_update}"
##  PATCH_COMMAND ${CMAKE_COMMAND} -E
##  copy ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt.qwt
##  <SOURCE_DIR>/CMakeLists.txt
#  PATCH_COMMAND patch -p 1 < ${CMAKE_CURRENT_LIST_DIR}/qwt.patch
#  CMAKE_ARGS ${COMMON_EXTERNAL_PROJECT_ARGS}
#             -DCMAKE_PREFIX_PATH:PATH=${CMAKE_PREFIX_PATH}
#  CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${THIRDPARTY_DIR}
#             -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
#             -DCMAKE_INSTALL_INCLUDEDIR=include
#             -DCMAKE_INSTALL_LIBDIR=lib
#             -DCMAKE_INSTALL_BINDIR=bin
#             -DBUILD_SHARED_LIBS=ON
#             -DBUILD_STATIC_LIBS=ON
#)

# Download and install the CuteMarkEd library
#ExternalProject_Add(CuteMarkEd
#    PREFIX thirdparty
#    GIT_REPOSITORY https://github.com/cloose/CuteMarkEd.git
#    UPDATE_COMMAND ""
#    BUILD_IN_SOURCE 1
#    CONFIGURE_COMMAND qmake CuteMarkEd.pro
#    BUILD_COMMAND make
#    INSTALL_COMMAND  sudo make -j4 install
#)

# Create the install target for the third-party libraries
install(DIRECTORY ${THIRDPARTY_DIR}/lib 
    DESTINATION .)
install(DIRECTORY ${THIRDPARTY_DIR}/include 
    DESTINATION .)
# Create the install target for the third-party libraries
#install(DIRECTORY ${THIRDPARTY_DIR}/
#    DESTINATION .)
